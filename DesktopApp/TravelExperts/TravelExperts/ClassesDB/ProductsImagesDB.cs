﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Author: Dmitry
/// Author: Maru
/// </summary>
namespace TravelExperts.ClassesDB
{
    public static class ProductsImagesDB
    {
        public static bool AddNewProductImage(byte[] b, int ID)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string insertStatement = @"IF EXISTS (SELECT * FROM Products_Images WHERE ProductID=@ProductID) 
                                       UPDATE Products_Images 
                                       SET ImageArray = @Array
                                       WHERE ProductID=@ProductID 
                                   ELSE
                                       INSERT INTO Products_Images (ProductID, ImageArray) 
                                       VALUES(@ProductID, @Array)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, con);
            insertCommand.Parameters.AddWithValue("@ProductID", ID);
            insertCommand.Parameters.AddWithValue("@Array", b);
            try
            {
                con.Open();
                int count = insertCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public static byte[] GetImage(int id)
        {
            byte[] array = null;
            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectQuery = "SELECT ImageArray " +
                                 "FROM Products_Images " +
                                 "WHERE ProductID = @ProductID";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@ProductID", id);
            try
            {
                con.Open(); // open connection
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read()) // read the customer if exists
                {
                    array = (byte [])reader["ImageArray"];
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close(); // close connection
            }
            return array;
        }
    }
}
