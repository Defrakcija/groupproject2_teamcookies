﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using TravelExperts.TableClasses;
using TravelExperts.ClassesDB;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
 * workshop 2
 * Author: Marimel Llamoso
 * Date: January 23, 2018
 * Purpose: to be able to display add edit the product supplier in travel expert data base
 * 
 * */

namespace TravelExperts.UserControls
{
    public partial class ProductsSuppliersPage : UserControl
    {
        List<Products_Suppliers> productsuppliers;
        Products_Suppliers productsupplier;

        public ProductsSuppliersPage()
        {
            InitializeComponent();

        }

        //initializes everything to default display and loads all the comboboxes
        private void ProductsSuppliersPage_Load(object sender, EventArgs e)
        {
            rbtnAdd.Checked = true;
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductsComboBox();
            LoadProductNamesComboBox();
            LoadProductSupplierIDRemove();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();
            gridProductSupplier.Refresh();
            DisplayAddRemoveTabList();
            rbtnPrductSupplier.Checked = true;
            gridProductSupplier.Columns[0].HeaderCell.Value = "Product Supplier ID";
            gridProductSupplier.Columns[1].HeaderCell.Value = "Product ID";
            gridProductSupplier.Columns[2].HeaderCell.Value = "Product Name";
            gridProductSupplier.Columns[3].HeaderCell.Value = "Supplier ID";
            gridProductSupplier.Columns[4].HeaderCell.Value = "Supplier Name";
            gridProductSuppliersAddEdit.Columns[0].HeaderCell.Value = "Product Supplier ID";
            gridProductSuppliersAddEdit.Columns[1].HeaderCell.Value = "Product ID";
            gridProductSuppliersAddEdit.Columns[2].HeaderCell.Value = "Product Name";
            gridProductSuppliersAddEdit.Columns[3].HeaderCell.Value = "Supplier ID";
            gridProductSuppliersAddEdit.Columns[4].HeaderCell.Value = "Supplier Name";
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
            gridProductSupplier.Columns[0].Width = 125;
            gridProductSupplier.Columns[4].Width = 200;
            gridProductSuppliersAddEdit.Columns[0].Width = 125;
            gridProductSuppliersAddEdit.Columns[4].Width = 200;
            lblEmpty.Text = "";
        }
/// <summary>
/// ALL Load combo boxes below
/// </summary>

            //loads products name combobox in add remove tab
    private void LoadProductsComboBox()
        {
            List<Products> products = new List<Products>();
            try
            {
                products = ProductsDB.GetProductsList();
                cboProducts.DataSource = products;
                cboProducts.DisplayMember = "ProductName";
                cboProducts.ValueMember = "ProductID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        //load product id
        private void LoadProductIDsComboBox()
        {
            List<Products> productIDs = new List<Products>();
            try
            {                
                cboProductIDs.DisplayMember = "ProductID";
                cboProductIDs.ValueMember = "ProductName";
                productIDs = ProductsDB.GetProductsList();
                cboProductIDs.DataSource = productIDs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        //load product name 
        private void LoadProductNamesComboBox()
        {
            List<Products> products = new List<Products>();
            try
            {
                cboProductNames.ValueMember = "ProductID";
                cboProductNames.DisplayMember = "ProductName";
                products = ProductsDB.GetProductsList();
                cboProductNames.DataSource = products;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        //load supplier name in tab remove combo box
        private void LoadSuppliersComboBox()
        {
            List<Suppliers> suppliers = new List<Suppliers>();
            try
            {                                
                suppliers = SuppliersDB.GetAllSuppliers();
                cboSuppliers.DataSource = suppliers;
                cboSuppliers.DisplayMember = "SupplierName";
                cboSuppliers.ValueMember = "SupplierID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        //load supplier name
        private void LoadSupplierNamesComboBox()
        {
            List<Suppliers> supplierNames = new List<Suppliers>();
            try
            {                
                cboSupplierNames.DisplayMember = "SupplierName";
                cboSupplierNames.ValueMember = "SupplierID";
                supplierNames = SuppliersDB.GetAllSuppliers();
                cboSupplierNames.DataSource = supplierNames;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        //load supplier id
        private void LoadSupplierIDsComboBox()
        {
            List<Suppliers> supplierIDs = new List<Suppliers>();
            try
            {
                cboSupplierIDs.DisplayMember = "SupplierID";
                cboSupplierIDs.ValueMember = "SupName";
                supplierIDs = SuppliersDB.GetAllSuppliers();
                cboSupplierIDs.DataSource = supplierIDs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        //load product supplier combo box
        private void LoadProductSupplierIDsComboBox()
        {
            List<Products_Suppliers> productsupplierIDs = new List<Products_Suppliers>();
            try
            {
                cboProductSupplierID.DisplayMember = "productsupplierId";
                cboProductSupplierID.ValueMember = "productsupplierId";
                productsupplierIDs = Products_SuppliersDB.GetProductSuppliers();
                cboProductSupplierID.DataSource = productsupplierIDs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        //load product supplier combo box in remove
        private void LoadProductSupplierIDRemove()
        {
            List<Products_Suppliers> productsupplierIDs = new List<Products_Suppliers>();
            try
            {
                cboProductSupplierDelete.DisplayMember = "productsupplierId";
                cboProductSupplierDelete.ValueMember = "productsupplierId";
                productsupplierIDs = Products_SuppliersDB.GetProductSuppliers();
                cboProductSupplierDelete.DataSource = productsupplierIDs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
/// <summary>
/// ALL display methods below
/// </summary>

            // display product supplier list by product id
        private void DisplayProductSupplierListbyProdID(int selectedprodID)
        {
            lblEmpty.Text = "";
            gridProductSupplier.Refresh();
            productsuppliers = Products_SuppliersDB.GetProdsupplierIDandSuppliersbyProdID(selectedprodID);
            if (productsuppliers.Count == 0)
            {
                lblEmpty.Text = "Please assign a Supplier for this Product";
                var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
                var source = new BindingSource(bindingList, null);
                gridProductSupplier.DataSource = source;
            }
            else
            {
                var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
                var source = new BindingSource(bindingList, null);
                gridProductSupplier.DataSource = source;

            }


        }
        // display product supplier list by supplier id
        private void DisplayProductSupplierListbySupID(int selectedsupID)
        {
            lblEmpty.Text = "";
            gridProductSupplier.Refresh();
            productsuppliers = Products_SuppliersDB.GetProdsupplierIDandSuppliersbySupID(selectedsupID);
            if (productsuppliers.Count == 0)
            {
                lblEmpty.Text = "Please assign a product for this supplier";
                gridProductSupplier.Refresh();
                var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
                var source = new BindingSource(bindingList, null);
                gridProductSupplier.DataSource = source;
            }
            else
            {
                gridProductSupplier.Refresh();
                var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
                var source = new BindingSource(bindingList, null);
                gridProductSupplier.DataSource = source;

            }
        }
        // display product supplier list by product supplier id
        private void DisplayProductSupplierListbyProdsupID(int selectedprodsupID)
        {
            gridProductSupplier.Refresh();
            productsuppliers = Products_SuppliersDB.GetProductSuppliersbyProdSupID(selectedprodsupID);
            var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
            var source = new BindingSource(bindingList, null);
            gridProductSupplier.DataSource = source;

        }
        // display from the add remove tab grid view product supplier list
        private void DisplayAddRemoveTabList()
        {

            productsuppliers = Products_SuppliersDB.GetProductSuppliers();
            var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
            var source = new BindingSource(bindingList, null);
            gridProductSuppliersAddEdit.DataSource = source;
        }

        // display from the add remove tab grid view product supplier list by product supplier id
        private void DisplayRemoveupdate(int selectedprodsupID)
        {
            gridProductSuppliersAddEdit.Refresh();
            productsuppliers = Products_SuppliersDB.GetProductSuppliersbyProdSupID(selectedprodsupID);
            var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
            var source = new BindingSource(bindingList, null);
            gridProductSuppliersAddEdit.DataSource = source;
        }
/// <summary>
/// Combo box select change below
/// </summary>
        private void cboProductID_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Products selectedProd = (Products)cboProductIDs.SelectedItem;
            int selectedprodID = Convert.ToInt32(selectedProd.ProductID);
            lblSelected.Text = "Product ID " + selectedprodID.ToString() + " (Product Name = " + selectedProd.ProductName + ")";
            DisplayProductSupplierListbyProdID(selectedprodID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = false;
            gridProductSupplier.Columns[2].Visible = false;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }

        private void cboProductNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Products selectedProd = (Products)cboProductNames.SelectedItem;
            int selectedprodID = Convert.ToInt32(selectedProd.ProductID);
            lblSelected.Text = "Product Name: " + cboProductNames.Text + " (ID = " + selectedprodID.ToString() + ")";
            DisplayProductSupplierListbyProdID(selectedprodID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = false;
            gridProductSupplier.Columns[2].Visible = false;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }

        private void cboSupplierIDs_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Suppliers selectedSup = (Suppliers)cboSupplierIDs.SelectedItem;
            int selectedsupID = Convert.ToInt32(selectedSup.SupplierID);
            lblSelected.Text = "Supplier ID " + selectedsupID.ToString() + " (Supplier Name= " + selectedSup.SupplierName + ")";
            DisplayProductSupplierListbySupID(selectedsupID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = false;
            gridProductSupplier.Columns[4].Visible = false;
        }

        private void cboSupplierNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Suppliers selectedSup = (Suppliers)cboSupplierNames.SelectedItem;
            int selectedsupID = Convert.ToInt32(selectedSup.SupplierID);
            lblSelected.Text = "Supplier Name: " + cboSupplierNames.Text + " (ID = " + selectedsupID.ToString() + ")";
            DisplayProductSupplierListbySupID(selectedsupID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = false;
            gridProductSupplier.Columns[4].Visible = false;
        }

        private void cboProductSupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Products_Suppliers selectedProdSup = (Products_Suppliers)cboProductSupplierID.SelectedItem;
            int selectedprodsupID = Convert.ToInt32(selectedProdSup.ProductSupplierId);
            lblSelected.Text = "Product Supplier ID " + selectedprodsupID.ToString();
            DisplayProductSupplierListbyProdsupID(selectedprodsupID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }
/// <summary>
/// all Buttons below
/// </summary>
    //diplayall the product suplier list
        private void btnDisplayAll_Click(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();
            productsuppliers = Products_SuppliersDB.GetProductSuppliers();
            var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
            var source = new BindingSource(bindingList, null);
            gridProductSupplier.DataSource = source;
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }
        //add product supplier
        private void addProductSupplier_Click(object sender, EventArgs e)
        {
            gridProductSuppliersAddEdit.Refresh();
            productsupplier = new Products_Suppliers();
            PutProductSupplierData(productsupplier);
            try
            {
                productsupplier.ProductSupplierId = Products_SuppliersDB.AddProductSupplier(productsupplier);
                MessageBox.Show("New Product Supplier has been added");
                DisplayAddRemoveTabList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductNamesComboBox();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();
            DisplayAddRemoveTabList();

        }
        //clears the display
        private void btnClear_Click(object sender, EventArgs e)
        {
            gridProductSupplier.Rows.Clear();
            gridProductSupplier.Refresh();
            lblSelected.Text = "";
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }

        //removes the product supplier when clicked
        private void btnRemove_Click(object sender, EventArgs e)
        {
            Products_Suppliers selectedProdsup = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodsupID = Convert.ToInt32(selectedProdsup.ProductSupplierId);
            productsupplier = Products_SuppliersDB.GetProducsuppliersObject(selectedprodsupID);
            DialogResult result = MessageBox.Show("Delete Product Supplier ID " + selectedprodsupID.ToString() + "?",
                            "Confirm Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                try
                {
                    if (!Products_SuppliersDB.RemoveProductSupplier(productsupplier))
                    {
                        MessageBox.Show("Another User may have Updated or deleted that product supplier or " +
                                    "It is being used in another Table, CANNOT REMOVE", "Database Error");
                    }
                    else
                    {
                        MessageBox.Show("Product Supplier ID: " + selectedprodsupID + " has been deleted");
                        LoadProductSupplierIDRemove();
                        DisplayAddRemoveTabList();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductsComboBox();
            LoadProductNamesComboBox();
            LoadProductSupplierIDRemove();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();
            DisplayAddRemoveTabList();

        }
        //when add radio button is selected it changes the display
        private void rbtnAdd_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            btnRemove.Visible = false;
            cboProductSupplierDelete.Visible = false;
            lblProductSupplier.Visible = false;
            btnUpdate.Visible = false;
            btnaddProductSupplier.Visible = true;
            lblProduct.Visible = true;
            lblSupplier.Visible = true;
            cboProducts.Visible = true;
            cboSuppliers.Visible = true;
            DisplayAddRemoveTabList();
            lblInstruction.Text = "Instruction: Select a product and a supplier " +
                "to Add new Product supplier.\nNew Supplier will be added at the end of the list.";
        }
        //when remove radio button is selected it changes the display
        private void rbtnRemove_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            btnRemove.Visible = true;
            cboProductSupplierDelete.Visible = true;
            lblProductSupplier.Visible = true;
            btnUpdate.Visible = false;
            btnaddProductSupplier.Visible = false;
            lblProduct.Visible = false;
            lblSupplier.Visible = false;
            cboProducts.Visible = false;
            cboSuppliers.Visible = false;
            Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
            DisplayRemoveupdate(selectedprodSupID);
            lblInstruction.Text = "Instruction: Select a the Product Supplier ID you want to remove.";
        }
        //when update radio button is selected it changes the display
        private void rbtnUpdate_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            btnRemove.Visible = false;
            cboProductSupplierDelete.Visible = true;
            lblProductSupplier.Visible = true;
            btnUpdate.Visible = true;
            btnaddProductSupplier.Visible = false;
            lblProduct.Visible = true;
            lblSupplier.Visible = true;
            cboProducts.Visible = true;
            cboSuppliers.Visible = true;
            Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
            DisplayRemoveupdate(selectedprodSupID);
            lblInstruction.Text = "Instruction: Select a the Product Supplier ID you want to Update then choose a product and a supplier for this ID.";
        }
        //update the product suplier lis
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            gridProductSuppliersAddEdit.Refresh();
            Products_Suppliers newproductsupplier = new Products_Suppliers();
            Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
            Products_Suppliers productsupplier = Products_SuppliersDB.GetProducsuppliersObject(selectedprodSupID);
            PutProductSupplierData(newproductsupplier);
            DialogResult result = MessageBox.Show("Update Product Supplier?\nWarning It will Update all product suppliers in other table",
                            "Confirm Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                try
                {
                    if (!Products_SuppliersDB.UpdateProductSupplier(productsupplier, newproductsupplier))
                    {
                        MessageBox.Show("Another user has updated or " +
                                    "deleted that Product Supplier.", "Database Error");
                    }
                    else
                    {
                        MessageBox.Show("Product supplier ID: " + selectedprodSupID + " has been edited");
                        DisplayRemoveupdate(selectedprodSupID);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            ////LoadProductSupplierIDRemove();
            //LoadProductsComboBox();
            //LoadSuppliersComboBox();
            //LoadSuppliersComboBox();
            //LoadProductIDsComboBox();
            //LoadProductsComboBox();
            //LoadProductNamesComboBox();
            //LoadProductSupplierIDRemove();
            //LoadSupplierIDsComboBox();
            //LoadSupplierNamesComboBox();
            //LoadProductSupplierIDsComboBox();
        }
        private void cboProductSupplierDelete_SelectedIndexChanged(object sender, EventArgs e)
        {
            Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
            DisplayRemoveupdate(selectedprodSupID);
        }

        //changes the display when product ID is checked
        private void rbtnProductID_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductIDsComboBox();
            cboProductSupplierID.Visible = false;
            cboProductIDs.Visible = true;
            cboProductNames.Visible = false;
            cboSupplierIDs.Visible = false;
            cboSupplierNames.Visible = false;
            lblProductSupplierID.Visible = false;
            lblProductID.Visible = true;
            lblProductName.Visible = false;
            lblSupplierID.Visible = false;
            lblSupplierName.Visible = false;

        }
        //changes the display when product Name is checked
        private void rbtnProdName_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductNamesComboBox();
            cboProductSupplierID.Visible = false;
            cboProductIDs.Visible = false;
            cboProductNames.Visible = true;
            cboSupplierIDs.Visible = false;
            cboSupplierNames.Visible = false;
            lblProductSupplierID.Visible = false;
            lblProductID.Visible = false;
            lblProductName.Visible = true;
            lblSupplierID.Visible = false;
            lblSupplierName.Visible = false;
        }
        //changes the display when supplier ID is checked
        private void rbtnSupplierID_CheckedChanged(object sender, EventArgs e)
        {
            LoadSupplierIDsComboBox();
            cboProductSupplierID.Visible = false;
            cboProductIDs.Visible = false;
            cboProductNames.Visible = false;
            cboSupplierIDs.Visible = true;
            cboSupplierNames.Visible = false;
            lblProductSupplierID.Visible = false;
            lblProductID.Visible = false;
            lblProductName.Visible = false;
            lblSupplierID.Visible = true;
            lblSupplierName.Visible = false;
        }
        //changes the display whensuplier name is checked
        private void rbtnSuName_CheckedChanged(object sender, EventArgs e)
        {
            LoadSupplierNamesComboBox();
            cboProductSupplierID.Visible = false;
            cboProductIDs.Visible = false;
            cboProductNames.Visible = false;
            cboSupplierIDs.Visible = false;
            cboSupplierNames.Visible = true;
            lblProductSupplierID.Visible = false;
            lblProductID.Visible = false;
            lblProductName.Visible = false;
            lblSupplierID.Visible = false;
            lblSupplierName.Visible = true;
        }
        //changes the display when product supplier is checked
        private void rbtnPrductSupplier_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductSupplierIDsComboBox();
            cboProductSupplierID.Visible = true;
            cboProductIDs.Visible = false;
            cboProductNames.Visible = false;
            cboSupplierIDs.Visible = false;
            cboSupplierNames.Visible = false;
            lblProductSupplierID.Visible = true;
            lblProductID.Visible = false;
            lblProductName.Visible = false;
            lblSupplierID.Visible = false;
            lblSupplierName.Visible = false;
        }
        //load all combo box when tab changes to displa
        private void tabDisplay_Click(object sender, EventArgs e)
        {
            LoadProductNamesComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductNamesComboBox();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();
            DisplayAddRemoveTabList();
        }
        //load combo box when tab changes
        private void tabAddRemove_Click(object sender, EventArgs e)
        {
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductNamesComboBox();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();
            LoadProductNamesComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDRemove();
            DisplayAddRemoveTabList();
        }
//creates product supplier object 
        private void PutProductSupplierData(Products_Suppliers productsupplier)
        {

            if (btnUpdate.Capture == true)
            {
                Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
                int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
                productsupplier.ProductSupplierId = selectedprodSupID;
                Products selectedProd = (Products)cboProducts.SelectedItem;
                int selectedprodID = Convert.ToInt32(selectedProd.ProductID);
                productsupplier.ProductId = selectedprodID;
                Suppliers selectedSup = (Suppliers)cboSuppliers.SelectedItem;
                int selectedSupId = Convert.ToInt32(selectedSup.SupplierID);
                productsupplier.SupplierId = selectedSupId;
            }
            else
            {
                Products selectedProd = (Products)cboProducts.SelectedItem;
                int selectedprodID = Convert.ToInt32(selectedProd.ProductID);
                productsupplier.ProductId = selectedprodID;
                Suppliers selectedSup = (Suppliers)cboSuppliers.SelectedItem;
                int selectedSupId = Convert.ToInt32(selectedSup.SupplierID);
                productsupplier.SupplierId = selectedSupId;
            }


        }
    }
}
