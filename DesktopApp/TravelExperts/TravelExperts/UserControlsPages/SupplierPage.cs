﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExperts.ClassesDB;
/// <summary>
/// Author: Andrew
/// </summary>
namespace TravelExperts.UserControlsPages
{
    public partial class SupplierPage : UserControl
    {
        List<Suppliers> supList;
        Suppliers sup;
        Suppliers newsup;
        bool editMode = false;
        public SupplierPage()
        {
            InitializeComponent();
        }

        public void DisplaySuppliersForProduct()
        {
            flpSuppliers.Controls.Clear();
            supList = SuppliersDB.GetAllSuppliers();
            //TableLayoutPanelCellPosition pos = tableLayoutPanel1.GetCellPosition(flpSuppliers);
            //int width = tableLayoutPanel1.GetColumnWidths()[pos.Column];
            foreach (Suppliers s in supList)
            {
                Button btn = DisplayGui.CreateResponsiveTextButton(s);
                flpSuppliers.Controls.Add(btn);
                btn.Width = flpSuppliers.Width - 25;
                sup = (Suppliers)btn.Tag;
                btn.Click += (sender, e) =>
                {
                    if (editMode == false)
                    {
                        txtSupID.Text = s.SupplierID.ToString();
                        txtSupName.Text = s.SupplierName;
                    }
                };
            }
        }

        public void FirstLoad()
        {
            flpSuppliers.Controls.Clear();
            supList = SuppliersDB.GetAllSuppliers();
            TableLayoutPanelCellPosition pos = tableLayoutPanel1.GetCellPosition(flpSuppliers);
            int width = tableLayoutPanel1.GetColumnWidths()[pos.Column];
            foreach (Suppliers s in supList)
            {
                Button btn = DisplayGui.CreateResponsiveTextButton(s);
                flpSuppliers.Controls.Add(btn);
                btn.Width = width - 13;
                sup = (Suppliers)btn.Tag;
                btn.Click += (sender, e) =>
                {
                    if (editMode == false)
                    {
                        txtSupID.Text = s.SupplierID.ToString();
                        txtSupName.Text = s.SupplierName;
                    }
                };
            }
        }



        private void SupplierPage_Load(object sender, EventArgs e)
        {
            FirstLoad();
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            AutoCompleteStringCollection col = new AutoCompleteStringCollection();
            foreach (Suppliers s in supList)
            {
                col.Add(s.SupplierName);
            }
            txtSearch.AutoCompleteCustomSource = col;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            if (Validator.IsPresent(txtSearch, "Search box is empty, Enter value"))
            {
                flpSuppliers.Controls.Clear();
                Suppliers s = supList.Find(x => x.SupplierName == txtSearch.Text);
                if (s != null)
                {
                    Button btn = DisplayGui.CreateResponsiveTextButton(s);
                    btn.Width = flpSuppliers.Width;
                    flpSuppliers.Controls.Add(btn);
                    AddAction(btn, s);                }
            }
        }

        private void AddAction(Button btn, Suppliers s)
        {
            btn.Click += (sender, e) =>
            {
                if (editMode == false)
                {
                    txtSupID.Text = s.SupplierID.ToString();
                    txtSupName.Text = s.SupplierName;
                }
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (RbAdd.Checked)
            {
                if (txtSupName != null && editMode == true)
                {
                    newsup = new Suppliers();
                    this.PutSupplierData(newsup);
                    try
                    {
                        SuppliersDB.AddSupplier(newsup);
                        //newsup.SupplierID = SuppliersDB.AddSupplier(newsup);
                        MessageBox.Show("Supplier " + txtSupName.Text + " was added successfully !!");
                        txtSupName.Text = "";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                    DisplaySuppliersForProduct();                    
                    ClearControls();
                    RbAdd.Checked = false;
                    txtSupName.ReadOnly = true;
                    editMode = false;
                }
                else
                    MessageBox.Show("Please enter a valid Name");              
                
            }
            else if (RbDelete.Checked)
            {
                DialogResult result = MessageBox.Show("Delete " + txtSupName.Text + "?",
                "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        if (!SuppliersDB.DeleteSupplier(sup))
                        {
                            MessageBox.Show("Another user has updated or deleted " +
                                "that supplier.", "Database Error");

                            this.GetSupplier(sup.SupplierName);
                            if (sup != null)
                                this.DisplaySupplier();
                            else
                                this.ClearControls();
                        }
                        else
                        {
                            this.ClearControls();
                            this.DisplaySupplier();
                        }
                    }
                    catch (Exception)
                    {
                        //MessageBox.Show(ex.Message, ex.GetType().ToString());
                        MessageBox.Show("It's not possible to delete supplier " + txtSupName.Text + ". It's being used by a package ");
                    }
                    RbDelete.Checked = false;
                }
            }
            else if (RbUpdate.Checked)
            {
                Suppliers newSupplier = new Suppliers();
                //this.PutProductData(newProduct);
                newSupplier.SupplierID = Convert.ToInt32(txtSupID.Text);
                newSupplier.SupplierName = txtSupName.Text.ToString();
                try
                {
                    if (!SuppliersDB.UpdateSupplier(newSupplier))
                    {
                        MessageBox.Show("Another user has updated or " +
                            "deleted that supplier.", "Database Error");
                    }
                    else
                    {
                        DisplaySuppliersForProduct();
                        RbUpdate.Checked = false;
                        txtSupName.ReadOnly = true;
                        editMode = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        private void ClearControls()
        {
            txtSupID.Text = "";
            txtSupName.Text = "";
        }

        private void PutSupplierData(Suppliers sups)
        {
            sups.SupplierName = txtSupName.Text;
        }


        private void RbAdd_CheckedChanged(object sender, EventArgs e)
        {
            editMode = true;
            ClearControls();
            txtSupName.ReadOnly = false;
        }

        private void GetSupplier(string suppliername)
        {
            try
            {
                sup = SuppliersDB.GetSupplier(suppliername.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void DisplaySupplier()
        {
            txtSupID.Text = sup.SupplierID.ToString();
            txtSupName.Text = sup.SupplierName;
        }

        private void RbUpdate_CheckedChanged(object sender, EventArgs e)
        {
            editMode = true;
            txtSupName.ReadOnly = false;
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            DisplaySuppliersForProduct();
        }
    }
}

