﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using TravelExperts.TableClasses;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExperts.UserControlsPages;
using TravelExperts.UserControls;
using TravelExperts.ClassesDB;
/// <summary>
/// Author: Dmitry
/// </summary>
namespace TravelExperts
{
    public partial class frmMain : Form
    {
        private Packages package; 
        private Packages currentPackage; // Selected Package
        private List<Button> buttonList; // Current button List
        private Products currentProduct; // Selected product
        int currentRow;
        private int currentPackageID;
        bool editMode = false;
        bool PSAdded = false;
        enum Pages { Packages, Products, Suppliers, ProductSuppliers, WelcomePage, Settings }

        public frmMain()
        {
            InitializeComponent();
            tabMain.ItemSize = new Size(0, 1);
            tabMain.SizeMode = TabSizeMode.Fixed;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.WelcomePage;
            lblWelcome.BackColor = Color.FromArgb(1, 255, 255, 255);
        }



        //         --- NAVIGATION MENU ---


        private void btnNavProducts_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.Products;
            ProductsPage pp = new ProductsPage();
            pp.Dock = DockStyle.Fill;
            tabProducts.Controls.Add(pp);
        }
        
        // Close button on navigation bar, closes all program not just current window
        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private void btnNavPackages_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.Packages;
            DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet());
            cmbSort.SelectedIndex = 0;
        }
        private void btnPPS_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.Suppliers;
            SupplierPage sp = new SupplierPage();
            sp.Dock = DockStyle.Fill;
            tabSuppliers.Controls.Add(sp);
        }

        private void btnNavProductSuppliers_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.ProductSuppliers;
            if (PSAdded == false)
            {
                tblPSCont.Controls.Add(new ProductsSuppliersPage { Dock = DockStyle.Fill }, 1, 1);
                PSAdded = true;
            }
        }


        //         --- PACKAGES PAGE ---
      

        private void btnPkgDisplayAll_Click(object sender, EventArgs e)
        {
            if (editMode != true)
            {
                ClearDisplayArea();
                DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet());
            }
        }

        // Adding Product to selected Package
        private void btnPkgAddProduct_Click(object sender, EventArgs e)
        {
            if (currentPackageID > 0 && editMode == false) // Check if Package selected and not in edit mode
            {
                frmAddPackageProduct form = new frmAddPackageProduct(currentPackageID); // creates form for adding new Product and passing Id of selected Package
                DialogResult result = form.ShowDialog();
                if (result == DialogResult.OK) // If dialog window closed with Ok button 
                {
                    DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet()); // Update grid view by loading data from database
                    gridPackages.ClearSelection();
                    SelectRowForPackage(currentPackageID.ToString());
                }
            }
            DisplayCurrentPackage();

        }

        // Displays selected Package in display area from a row in DataGridView
        private void gridPackages_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridPackages.SelectedCells.Count > 0)
            {
                currentRow = gridPackages.SelectedCells[0].RowIndex;
                if (gridPackages.Rows[currentRow] != null)
                {
                    DataGridViewRow selectedRow = gridPackages.Rows[currentRow];
                    if (Int32.TryParse(selectedRow.Cells["PackageID"].Value.ToString(), out currentPackageID))
                    {
                        DisplayCurrentPackage();
                    }
                }
            }
        }

        private void DisplayCurrentPackage()
        {
            if (currentPackageID > 0)
            {
                package = PackagesDB.GetPackage(currentPackageID);
                txtPkgID.Text = package.PackageID.ToString();
                txtPkgName.Text = package.PackageName.ToString();
                txtPkgDesc.Text = package.PackageDescription;
                dtpPkgStart.Value = package.PackageStartDate;
                dtpPkgEnd.Value = package.PackageEndDate;
                txtPkgPrice.Text = package.PackageBasePrice.ToString("C");
                txtPkgCommision.Text = package.PackageAgencyCommission.ToString("C");
                buttonList = DisplayGui.DisplayPackageProducts(currentPackageID, flpProducts);
                ProductButtonClickEvent(buttonList);
            }
        }

        private void ProductButtonClickEvent(List<Button> buttonList)
        {
            Suppliers sup;
            foreach (Button btn in buttonList)
            {
                btn.Click += (sender, e) =>
                    {                        
                        currentProduct = (Products)btn.Tag;
                        lblPkgProductID.Text = currentProduct.ProductID.ToString();
                        lblPkgProductName.Text = currentProduct.ProductName;
                        sup = SuppliersDB.GetSupplierForTheProduct(currentProduct.ProductID, currentPackageID);
                        lblPkgProductSupplier.Text = sup.SupplierName;
                    };
                btn.LostFocus += (sender, e) =>
                {
                    lblPkgProductID.Text = "";
                    lblPkgProductName.Text = "";
                    lblPkgProductSupplier.Text = "";
                    //currentProduct = null;
                };
            }
        }

        // Enables blank display area
        private void btnPkgAdd_Click(object sender, EventArgs e)
        {
            if (editMode == false)
            {
                editMode = true;
                ClearDisplayArea();
                EnableControls();
            }
        }

        // Clear all controls in display area
        private void ClearDisplayArea()
        {
            txtPkgID.Clear();
            txtPkgName.Clear();
            txtPkgDesc.Clear();
            dtpPkgStart.Value = DateTime.Today;
            dtpPkgEnd.Value = DateTime.Today;
            txtPkgPrice.Clear();
            txtPkgCommision.Clear();
            flpProducts.Controls.Clear();
        }

        // Enables all controls in display area
        private void EnableControls()
        {
            txtPkgName.ReadOnly = false;
            txtPkgDesc.ReadOnly = false;
            dtpPkgStart.Enabled = true;
            dtpPkgEnd.Enabled = true;
            txtPkgPrice.ReadOnly = false;
            txtPkgCommision.ReadOnly = false;
            btnPkgSave.Enabled = true;
        }

        // Makes all constrols read only to prevent editing data 
        private void DisableControls()
        {
            txtPkgName.ReadOnly = true;
            txtPkgDesc.ReadOnly = true;
            dtpPkgStart.Enabled = false;
            dtpPkgEnd.Enabled = false;
            txtPkgPrice.ReadOnly = true;
            txtPkgCommision.ReadOnly = true;
            btnPkgSave.Enabled = false;
        }

        // Grabs data from display area, creates Packages object and saves it to database
        private void BtnPkgSave_Click(object sender, EventArgs e)
        {
            package = new Packages();
            if (Validator.IsPresent(txtPkgName, "Enter Package Name") && Validator.IsPresent(txtPkgDesc, "Enter Description") &&
                Validator.IsDecimal(txtPkgPrice, "Base price") && Validator.IsDecimal(txtPkgCommision, "Commision") &&
                Validator.IsLessThan(txtPkgPrice, txtPkgCommision, "Base price" , "Commision"))
            {
                package.PackageName = txtPkgName.Text;
                package.PackageDescription = txtPkgDesc.Text;
                package.PackageBasePrice = decimal.Parse(txtPkgPrice.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
                package.PackageAgencyCommission = decimal.Parse(txtPkgCommision.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
                package.PackageStartDate = dtpPkgStart.Value;
                package.PackageEndDate = dtpPkgEnd.Value;
                if (txtPkgID.Text == "")
                {
                    currentPackageID = PackagesDB.AddNewPackage(package);
                }
                else
                {

                    package.PackageID = Convert.ToInt32(txtPkgID.Text);
                    if (!PackagesDB.UpdatePackage(currentPackage, package))
                    {
                        MessageBox.Show("Another user has updated or " +
                            "deleted this package. List updated!.", "Database Error");
                        this.DialogResult = DialogResult.Retry;
                    }
                    DisplayCurrentPackage();
                }
            DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet());
            gridPackages.ClearSelection();
            SelectRowForPackage(currentPackageID.ToString());
            //ClearDisplayArea();
            DisableControls();
            editMode = false;
            }       
        }

        // Enables Display area and populates it with selected Package data
        private void btnPkgEdit_Click(object sender, EventArgs e)
        {
            if (editMode == false)
            {
                if (txtPkgID.Text != "")
                {
                    currentPackage = new Packages()
                    {
                        PackageID = Convert.ToInt32(txtPkgID.Text),
                        PackageName = txtPkgName.Text.ToString(),
                        PackageDescription = txtPkgDesc.Text,
                        PackageStartDate = dtpPkgStart.Value,
                        PackageEndDate = dtpPkgEnd.Value,
                        PackageBasePrice = decimal.Parse(txtPkgPrice.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.Number),
                        PackageAgencyCommission = decimal.Parse(txtPkgCommision.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.Number)
                    };
                    EnableControls();
                    editMode = true;
                }
                else
                {
                    MessageBox.Show("Choose Package from list");
                }
            }
        }

        //Clears and disables display area
        private void btnPkgCencel_Click(object sender, EventArgs e)
        {
            ClearDisplayArea();
            DisableControls();
            editMode = false;
        }

        //
        public void DisplayProductInfo(Products p)
        {
            lblPkgProductID.Text = p.ProductID.ToString();
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.D1)
            {
                Form f3 = new Form();
                Label l = new Label();
                l.Text = "Secret Form";
                f3.Controls.Add(l);
                f3.ShowDialog();
            }
        }

        private void lblMain_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.WelcomePage;
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.Settings;
        }

        private void btnSelectControlColor_Click(object sender, EventArgs e)
        {
            ChangeColor<Button>(GetColor());
        }      
        
        private Color GetColor()
        {
            ColorDialog MyDialog = new ColorDialog();
            // Keeps the user from selecting a custom color.
            MyDialog.AllowFullOpen = false;
            // Allows the user to get help. (The default is false.)
            MyDialog.ShowHelp = true;
            // Sets the initial color select to the current text color.
            MyDialog.Color = btnExit.BackColor;

            // Update the text box color if the user clicks OK 
            if (MyDialog.ShowDialog() == DialogResult.OK)
                return MyDialog.Color;
            return btnExit.BackColor;
        }

        private void ChangeColor<T>(Color color)
        {
            foreach (Control c in tlpNav.Controls)
            {
                if (c.GetType() == typeof(T))
                    c.BackColor = color;
            }
        }

        private void ChangeTextColor<T>(Color color)
        {
            foreach (Control c in tlpNav.Controls)
            {
                if (c.GetType() == typeof(T))
                    c.ForeColor = color;
            }
        }

        private void btnBGColor_Click(object sender, EventArgs e)
        {
            tlpNav.BackColor = GetColor();
        }

        private void btnLableColor_Click(object sender, EventArgs e)
        {
            Color color = GetColor();
            ChangeColor<Label>(color);
        }

        private void btnTextColor_Click(object sender, EventArgs e)
        {
            Color color = GetColor();
            ChangeTextColor<Label>(color);
        }

        private void btnButtonTextColor_Click(object sender, EventArgs e)
        {
            ChangeTextColor<Button>(GetColor());
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            ChangeTextColor<Button>(Color.Black);
            ChangeTextColor<Label>(Color.Black);
            ChangeColor<Button>(Color.Aqua);
            ChangeColor<Label>(Color.Aqua);
            tlpNav.BackColor = Color.Teal;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ClearDisplayArea();
            if (Validator.IsPresent(txtInput, "Enter " + cmbSort.SelectedItem ))
            {
                List<Packages> pkg = PackagesDB.GetPackagesList();
                if (cmbSort.SelectedIndex == 0)
                {
                    string name = txtInput.Text;
                    List<Packages> newPkg = pkg.FindAll(x => x.PackageName.ToLower().Contains(name.ToLower()));
                    gridPackages.DataSource = newPkg;
                }
                else
                {
                    if (Validator.IsInt32(txtInput, "ID" ))
                    {
                        int ID = Convert.ToInt32(txtInput.Text);
                        List<Packages> newPkg = pkg.FindAll(x => x.PackageID.Equals(ID));
                        gridPackages.DataSource = newPkg;
                    }
                }

            }
        }

        private void btnPkgDeleteProduct_Click(object sender, EventArgs e)
        {
            if (currentPackageID > 0 && currentProduct != null)
            {
                if (MessageBox.Show("Delete " + currentProduct.ProductName + "?", "Delete product", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                {
                    PackagesProductsSupplierDB.DeletePPS(currentPackageID, currentProduct.ProductID);
                }
                DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet()); // Update grid view by loading data from database
                DisplayCurrentPackage();
                gridPackages.ClearSelection();
                SelectRowForPackage(currentPackageID.ToString());
            }
        }

        private void dtpPkgEnd_ValueChanged(object sender, EventArgs e)
        {
            if (dtpPkgEnd.Value < dtpPkgStart.Value && editMode == true)
            {
                MessageBox.Show("End Date can't be earlier than Start Date");
                dtpPkgEnd.Value = dtpPkgStart.Value.AddDays(1);
            }
        }

        private void btnPkgDelete_Click(object sender, EventArgs e)
        {
            if (editMode != true || MessageBox.Show("Delete " + currentProduct.ProductName + "?", "Delete product", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    PackagesDB.DeletePackage(currentPackageID);
                    DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet()); // Update grid view by loading data from databas
                    ClearDisplayArea();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("You can't delete this package, it contains Products or was Booked", "Message");
                    ErrorLog.SaveException(ex);
                }
            }
        }

        private void SelectRowForPackage(string value)
        {
            int rowIndex = -1;
            DataGridViewRow row = gridPackages.Rows
                .Cast<DataGridViewRow>()
                .Where(r => r.Cells["PackageId"].Value.ToString().Equals(value))
                .First();

            rowIndex = row.Index;
            gridPackages.Rows[rowIndex].Selected = true;
        }

        private void dtpPkgStart_ValueChanged(object sender, EventArgs e)
        {
            if (dtpPkgStart.Value < DateTime.Today && editMode == true)
            {
                MessageBox.Show("Start Date can't be in past");
                dtpPkgStart.Value = DateTime.Today;
            }
        }
    }
}

