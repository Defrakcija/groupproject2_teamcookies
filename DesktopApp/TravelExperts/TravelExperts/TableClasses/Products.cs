﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Author: Nour
/// </summary>
namespace TravelExperts
{
    public class Products
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
    }
}
