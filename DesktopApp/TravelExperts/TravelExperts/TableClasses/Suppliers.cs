﻿// Author - Andrew Murphy
// Project Workshop 2


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Author: Andrew
/// </summary>
namespace TravelExperts
{
    public class Suppliers
    {
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
    }
}
