﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Author: David
/// </summary>
namespace TravelExperts
{
    public class PackagesProductsSupplier
    {
        public int PackageID { get; set; }
        public int ProductSupplierID { get; set; }
    }
}
