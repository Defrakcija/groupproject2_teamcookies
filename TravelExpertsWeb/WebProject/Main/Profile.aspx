﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Main.Profile" %>
 <%--Author :Nour --%><%--Customer Profile Page --%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <%--  Links for styling the page --%>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="ProfileStyles.css" rel="stylesheet" />

</head>

<body class="w3-light-grey w3-content" style="max-width:1600px">
    <form id="form1" runat="server">
    <div>
    <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br/>
          <div class="w3-container">
            <a href="#" onclick="w3_close()" class="w3-hide-large w3-right w3-jumbo w3-padding w3-hover-grey" title="close menu">
              <i class="fa fa-remove"></i>
            </a>
            <h1>Travel Experts</h1>
              <img src="Images/users.jpg" style="width:45%;" class="w3-round"/><br /><br />
            <h4><b>Welcome, <asp:Label ID="lblUser" runat="server" Text="UserName"></asp:Label>!</b></h4>
          </div>
          <div class="w3-bar-block">
            <a href="#portfolio" onclick="w3_close()" class="w3-bar-item w3-button w3-padding w3-text-teal">
               <i class="fa fa-user fa-fw w3-margin-right"></i>Personal Information</a> 

            <a href="#orders" onclick="w3_close()" class="w3-bar-item w3-button w3-padding ">
                <i class="fa fa-th-large fa-fw w3-margin-right"></i>History</a> 

  
            <a href="#contact" onclick="w3_close()" class="w3-bar-item w3-button w3-padding">
               <i class="fa fa-envelope fa-fw w3-margin-right"></i>CONTACT US</a>
            <asp:Button ID="btnSignout" runat="server" Text="Sign Out"  class="w3-bar-item w3-button w3-padding" CausesValidation="False" OnClick="btnSignout_Click" />
          </div>
 
        </nav>

<!-- Overlay effect when opening sidebar on small screens -->
    <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <!-- !PAGE CONTENT! -->
        <div class="w3-main" style="margin-left:300px">

          <!-- Header -->
          <header >
            <a href="#"><img src="Images/users.jpg" style="width:80px;" class="w3-circle w3-right w3-margin w3-hide-large w3-hover-opacity"/></a>
            <span class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onclick="w3_open()"><i class="fa fa-bars"></i></span>
 
          </header>

    <div class="w3-row-padding">
        <div class="w3-container">
            <h1 id="portfolio"><b>Information</b></h1>
         </div>
        <%-- Displaying customer info where they can see their information and update it --%>
                <div class="row">
                    <div class="column" >
                    <asp:Label ID="LblFname" runat="server" Text="First Name"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFname" ErrorMessage="First name is required" ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
                    <asp:TextBox ID="txtFname" runat="server" Width="249px"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblLname" runat="server" Text="Last Name"></asp:Label> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLname" ErrorMessage="Last name is required" ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            
                    <asp:TextBox ID="txtLname" runat="server" Width="252px"></asp:TextBox>
                    <br />
                               <asp:Label ID="lblUserName" runat="server" Text="User Name"></asp:Label><br />
           
                    <asp:TextBox ID="txtUserName" runat="server" Width="252px"></asp:TextBox>
                    <br />
           
                    <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
      
                    <asp:TextBox ID="txtCity" runat="server" Width="252px"></asp:TextBox>
                    <br />
               <asp:Label ID="lblCustEmail" runat="server" Text="Email"></asp:Label>
                    <br />
           
                    <asp:TextBox ID="txtEmail" runat="server" Width="252px"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblHome" runat="server" Text="Home Phone"></asp:Label>
                    <asp:RegularExpressionValidator ID="HomePhoneValidation" runat="server" ControlToValidate="txtHomePhone" ErrorMessage="Home phone pattern should be  like 5555555555" ValidationExpression="\(?([0-9]{3})\)?[-.?]?([0-9]{3})[-.?]?([0-9]{4})" ForeColor="Red"></asp:RegularExpressionValidator>
            
                    <asp:TextBox ID="txtHomePhone" runat="server" Width="252px"></asp:TextBox>
            
              </div>
             <div class="column" >
                  <asp:Label ID="lblAddress" runat="server" Text="Address "></asp:Label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address is required" ForeColor="Red"></asp:RequiredFieldValidator>
                  <br />
                    <asp:TextBox ID="txtAddress" runat="server" Width="252px"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblProv" runat="server" Text="Province"></asp:Label>
                          <br />
                  <asp:DropDownList ID="ddlProvUpdate" runat="server" AutoPostBack="True">
                      <asp:ListItem Text="Select a Province" value="-1" ></asp:ListItem>
                        <asp:ListItem Text="Alberta" Value="AB"></asp:ListItem>
                        <asp:ListItem Text="British Columbia" Value="BC"></asp:ListItem>
                        <asp:ListItem Text="Manitoba" Value="MB"></asp:ListItem>
                        <asp:ListItem Text="New Brunswick" Value="NB"></asp:ListItem>
                        <asp:ListItem Text="Newfoundland and Labrador" Value="NL"></asp:ListItem>
                        <asp:ListItem Text="Northwest Territories" Value="NT"></asp:ListItem>
                        <asp:ListItem Text="Nova Scotia" Value="NS"></asp:ListItem>
                        <asp:ListItem Text="Nunavut" Value="NU"></asp:ListItem>
                        <asp:ListItem Text="Ontario" Value="ON"></asp:ListItem>
                        <asp:ListItem Text="Prince Edward Island" Value="PE"></asp:ListItem>
                        <asp:ListItem Text="Quebec" Value="QC"></asp:ListItem>
                        <asp:ListItem Text="Saskatchewan" Value="SK"></asp:ListItem>
                        <asp:ListItem Text="Yukon" Value="YT"></asp:ListItem>
                  </asp:DropDownList>
            <br />
            <asp:Label ID="lblPostal" runat="server" Text="Postal Code"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPostalCode" ErrorMessage="Postal Code is required" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="PostalValidation" runat="server" ControlToValidate="txtPostalCode" ErrorMessage="Postal Code should be X1X 1X1" ValidationExpression="([ABCEGHJKLMNPRSTVXY]|[abcdefghijklmnopqrstuvwxyz])[0-9]([ABCEGHJKLMNPRSTVWXYZ]|[abcdefghijklmnopqrstuvwxyz]) ?[0-9]([ABCEGHJKLMNPRSTVWXYZ]|[abcdefghijklmnopqrstuvwxyz])[0-9]" ForeColor="Red"></asp:RegularExpressionValidator>
        <br />
           
            <asp:TextBox ID="txtPostalCode" runat="server" Width="252px" Style="text-transform : uppercase"></asp:TextBox>
            <br />
            <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtCountry" ErrorMessage="Country is required" ForeColor="Red"></asp:RequiredFieldValidator>
        <br />
           
            <asp:TextBox ID="txtCountry" runat="server" Width="252px"></asp:TextBox>
            <br />
          
           
            <asp:Label ID="lblBus" runat="server" Text="Buisness Phone"></asp:Label><br />
        
           <asp:TextBox ID="txtBusPhone" runat="server" Width="252px" ValidateRequestMode="Enabled"></asp:TextBox>
            <br />
                  <asp:CompareValidator ID="CVPASS" runat="server" ControlToCompare="txtPass" ControlToValidate="txtConfirm" ErrorMessage="Password doesn't match" ForeColor="Red"></asp:CompareValidator>
                  <br />
           
            <asp:TextBox ID="txtPass" runat="server" Width="252px" TextMode="Password" Placeholder="Type New Password"></asp:TextBox>
                  <br />
                  <asp:TextBox ID="txtConfirm" runat="server" Width="252px" TextMode="Password" Placeholder="Re-Type Password"></asp:TextBox>
            <br />
  </div>
            </div>

            <asp:Button link="#portfolio" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" class ="button button2" CausesValidation="False"/>

            <asp:Button href="#portfolio" ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="button button1" />

            <asp:Button href="#portfolio" ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" class="button button3" CausesValidation="False"/>
            <br />
        
 
  </div>
  
    <!--Displaying customer order history  -->
        
    <div class="w3-third w3-container w3-margin-bottom" style=" width: 100% " >
            <div class="w3-container w3-padding-large" style="margin-bottom:32px">
    <h4 id="orders"><b>Orders History</b></h4>
    <!-- GridView to display the customers order history -->
    <div class="w3-row-padding" style="margin:0 -16px">
      <asp:Label ID="lblOrders" runat="server" BorderColor="#6699FF" BorderStyle="Dashed" Font-Italic="True" Font-Size="Large" ForeColor="#0000CC" ></asp:Label>
        <asp:GridView ID="gvBookingDetails" runat="server" ShowFooter="true" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" EmptyDataText="No orders yet">
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
           <columns>
          <asp:boundfield datafield="BookingNo" headertext="BookingNo"/>
          <asp:boundfield datafield="BookingDate" headertext="BookingDate" DataFormatString="{0:MM-dd-yyyy}"/>
          <asp:boundfield datafield="TravelerCount" headertext="TravelerCount"/>
          <asp:boundfield datafield="Destination" headertext="Destination"/>
          <asp:boundfield datafield="FeeName" headertext="FeeName" />
          <asp:boundfield datafield="FeeAmt" headertext="FeeAmt" DataFormatString="{0:C}"/>
          <asp:boundfield datafield="BasePrice" headertext="BasePrice" DataFormatString="{0:C}"/>
          <asp:boundfield datafield="Total" headertext="Total" DataFormatString="{0:C}"/>
        </columns>
        </asp:GridView>

   </div>
 
    </div>
       
    </div>

  <!-- Contact Section -->
  <div class="w3-container w3-padding-large w3-grey">
    <h4 id="contact"><b>Contact US</b></h4>
    <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
      <div class="w3-third w3-dark-grey">
        <p><i class="fa fa-envelope w3-xxlarge w3-text-light-grey"></i></p>
        <p>agents@TravelExperts.com</p>
      </div>
      <div class="w3-third w3-teal">
        <p><i class="fa fa-map-marker w3-xxlarge w3-text-light-grey"></i></p>
        <p>Calgary, CA</p>
      </div>
      <div class="w3-third w3-dark-grey">
        <p><i class="fa fa-phone w3-xxlarge w3-text-light-grey"></i></p>
        <p>555-555-5555</p>
      </div>
    </div>
    <hr class="w3-opacity"/>
    
      <div class="w3-section">
        <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
        <asp:TextBox ID="Name" runat="server" class="w3-input w3-border"></asp:TextBox>
      </div>
      <div class="w3-section">
        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
        <asp:TextBox ID="txtEmailUs" runat="server" class="w3-input w3-border"></asp:TextBox>
      </div>
      <div class="w3-section">
        <asp:Label ID="lblMessage" runat="server" Text="Message"></asp:Label>
        <asp:TextBox ID="txtMessage" runat="server" class="w3-input w3-border"></asp:TextBox>
      </div>
      <button type="submit" class="w3-button w3-black w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Send Message</button>
    
  </div>

  <!-- Footer -->
  <footer class="w3-container w3-padding-32 w3-dark-grey">
  <div class="w3-row-padding">
    <div class="w3-third">
      <h3>About US</h3>
      <p>We are travel experts.</p>
      <p>Some Text</p>
    </div>
  
    <div class="w3-third">
      <h3>Location</h3>
      <ul class="w3-ul w3-hoverable">
        <li class="w3-padding-16">
          <span class="w3-large">123 street NE</span><br>
          <span>Calgar,AB</span>
        </li>
        <li class="w3-padding-16">
          <span class="w3-large"> X1X 1X1</span><br>
            <span class="w3-large">CANADA </span><br>
          <span>Andrew</span>
        </li> 
      </ul>
    </div>

    <div class="w3-third">
      <h3>Social Media</h3>
      <p>
         <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i></p>
    </div>

  </div>
  </footer>
  
  <div class="w3-black w3-center w3-padding-24">Powered by team-Cookies OOSD 2018</div>

<!-- End page content -->
</div>

<script>
// Script to open and close sidebar
function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
</script>
    </div>
    </form>
</body>
</html>
