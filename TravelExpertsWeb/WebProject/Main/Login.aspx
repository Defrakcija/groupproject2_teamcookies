﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Main.Login" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="mainPlaceholder">
    <link href="Style/LoginStyle.css" rel="stylesheet" />
<div class="login-card">
    <h5 style =" text-align :center ; border: 2px solid DodgerBlue;"><asp:Label ID="lblRegStatus" runat="server" ForeColor="#000099"></asp:Label></h5>
    <h1>Log-in</h1><br>
    <asp:TextBox runat="server" placeholder="Username" id="txtUsername"/>    
    <asp:RequiredFieldValidator ID="usernameValidator" runat="server" ErrorMessage="Enter username" ControlToValidate="txtUsername"></asp:RequiredFieldValidator>
    <asp:TextBox runat="server" name="pass" placeholder="Password" id="txtPassword" TextMode="Password"/>    
    <asp:RequiredFieldValidator ID="passwordValidator" runat="server" ErrorMessage="Enter Password" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
    <asp:Button runat="server" ID="btnLogin" class="login login-submit" Text="Login" OnClick="btnLogin_Click" />
    <div class="login-help">
        <a href="~/Register.aspx">Register</a> • <a href="#">Forgot Password</a><br/>
        <asp:Label ID="lblStatus" runat="server" ForeColor="#CC0000"></asp:Label><br/>
    </div>
</div>
</asp:Content>
