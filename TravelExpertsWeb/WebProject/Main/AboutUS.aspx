﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="AboutUS.aspx.cs" Inherits="Main.AboutUS" %>
<%--Author : Nour Srouji - Jan,2018--%>
<%--This page gets the styles from the master page --%>
<asp:Content ID="Content1" runat="server" contentplaceholderid="mainPlaceholder">
    
<div style=" height : 500px ">
    <h2 style="text-align:center">COMING SOON</h2>
   <%-- Counter displayed on the screen when it loads --%>
    <p id="demo" style="color:darkred ; text-align:center ; font-size : xx-large"></p>
</div>

    <%--JavaScript code to play the counter --%>
<script>
// Set the date we're counting down to
var countDownDate = new Date("june 5, 2018 15:37:25").getTime();

// Update the count down every 1 second
var countdownfunction = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(countdownfunction);
        document.getElementById("demo").innerHTML = "EXPIRED";
    }
}, 1000);
</script>

</asp:Content>
