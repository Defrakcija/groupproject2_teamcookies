﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//Author : Nour Srouji
//Date : Jan -2018
namespace Main
{
    public partial class Profile : System.Web.UI.Page
    {
       //list to save order history of a customer 
        List<BookingDetails> details;
        // current customer 
        Customers cust;
        //when the page loads
        protected void Page_Load(object sender, EventArgs e)
        {
           //check if the customer is Not Logged In in if not redirect them to notloggedin page 
            if (Application["IsLoggedIn"] == null)
            {
                Response.Redirect("NotLoggedIn.aspx");
            }
            else
            {//they are logged in 
                if (!IsPostBack)
                {//the user now can't change any info until they press update 
                    ReadOnlyTrue();
                    ButtonsNotVisible();
                    // get the logged in customer ID 
                    int custId = (int)Application["currentUser"];
                    //get his information 
                    cust = CustomersDB.GetCustomerDetails(custId);
                    //small message to welcome the user 
                    lblUser.Text = "" + cust.CustFirstName;
                    //display values in textboxes
                    SetValues();
                    // get the order history of this customer 
                    details = BookingDetailsDB.GetBookingDetails(cust.CustomerId).ToList();
                    //if the customer have details display in gridview
                    if (details.Count > 0)
                    {
                        lblOrders.Text = "";
                        gvBookingDetails.DataSource = details;
                        gvBookingDetails.DataBind();
                        // calculate total for fees , base price and total owing 
                        decimal Ptotal = 0;
                        foreach (BookingDetails detail in details)
                            Ptotal += Convert.ToDecimal(detail.BasePrice);
                        gvBookingDetails.FooterRow.Cells[4].Text = "Total";
                        gvBookingDetails.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                        gvBookingDetails.FooterRow.Cells[5].Text = Ptotal.ToString("C");
                        gvBookingDetails.FooterRow.Cells[5].BorderStyle = BorderStyle.Dotted;

                        decimal feetotal = 0;
                        foreach (BookingDetails detail in details)
                            feetotal += Convert.ToDecimal(detail.FeeAmt);
                        gvBookingDetails.FooterRow.Cells[6].Text = feetotal.ToString("C");
                        gvBookingDetails.FooterRow.Cells[6].BorderStyle = BorderStyle.Dotted;

                        decimal total = 0;
                        foreach (BookingDetails detail in details)
                            total += Convert.ToDecimal(detail.Total);
                        gvBookingDetails.FooterRow.Cells[7].Text = total.ToString("C");
                        gvBookingDetails.FooterRow.Cells[7].BorderStyle = BorderStyle.Double;
                        gvBookingDetails.FooterRow.Cells[7].BorderColor = System.Drawing.Color.Red;
                    }
                    else
                    {//if the customer doesn't have any display a message 
                        lblOrders.Text = "No Orders Yet. Hurry Up and Book Our Amazing Deals !!!! ";
                    }
                    // hide some information of the customer just for styling reasons 
                    HideFields();

                }
            }
            
        }
          // when the customer wants to update 
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            // display save and cancel button
            ButtonsVisible();
            // allow the user to edit 
            ReadOnlyFalse();
            // display all the customer information
            DisplayFields();
            //hide update button
            btnUpdate.Visible = false;

        }
       //cancel button go to the state before update 
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ReadOnlyTrue();
            ButtonsNotVisible();
            int custId = (int)Application["currentUser"];
            cust = CustomersDB.GetCustomerDetails(custId);
            SetValues();
            HideFields();
            btnUpdate.Visible = true;
        }
        // when they want to save new changes 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //create new customer 
            Customers newcustomer = new Customers();
          
            int custId = (int)Application["currentUser"];
            cust = CustomersDB.GetCustomerDetails(custId);
            newcustomer.CustomerId = cust.CustomerId;
            // set the values of new customer 
            SaveValues(newcustomer);
            // save the new customer in database
            if (CustomersDB.UpdateCustomer(cust, newcustomer))
                cust = newcustomer;
            //display new values 
            SetValues();
         //hide some fields for style reasons 
            ReadOnlyTrue();
            ButtonsNotVisible();
            HideFields();
            btnUpdate.Visible = true;
        }
        //when the user signs out 
        protected void btnSignout_Click(object sender, EventArgs e)
        {
            //set the application to false and clear it 
            Application["IsLoggedIn"] = false;
            Application["currentUser"] = null;
            HttpContext.Current.Application.Clear();
            Application.Clear();
            Response.Cache.SetNoStore();
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //redirect the user to welcome page 
            Server.Transfer("Welcome.aspx", true);
        }

        // save value of the new changes in database
        private void SaveValues(Customers newcustomer)
        {
            newcustomer.CustFirstName = txtFname.Text;
            newcustomer.CustLastName = txtLname.Text;
            newcustomer.CustAddress = txtAddress.Text;
            newcustomer.CustCity = txtCity.Text;
            
            newcustomer.CustProv =  ddlProvUpdate.SelectedValue.ToString();
            newcustomer.CustPostal = txtPostalCode.Text.ToUpper();
            newcustomer.CustCountry = txtCountry.Text;
            newcustomer.CustHomePhone = txtHomePhone.Text;
            newcustomer.CustBusPhone = txtBusPhone.Text;
            newcustomer.CustEmail = txtEmail.Text;
            if (txtPass.Text != "")
            {
                newcustomer.Password = BCrypt.Net.BCrypt.HashPassword(txtPass.Text);
            }
            else
            {
                newcustomer.Password = cust.Password; 
            }
          
            newcustomer.Username = txtUserName.Text; 
        }

     //display values in fields
        private void SetValues()
        {
            txtFname.Text = cust.CustFirstName;
            txtLname.Text = cust.CustLastName;
            txtAddress.Text = cust.CustAddress;
            txtCity.Text = cust.CustCity;
            ddlProvUpdate.SelectedValue = cust.CustProv;
          
            txtPostalCode.Text = cust.CustPostal;
            txtCountry.Text = cust.CustCountry;
            txtHomePhone.Text = cust.CustHomePhone;
            txtBusPhone.Text = cust.CustBusPhone;
            txtEmail.Text = cust.CustEmail;
            txtUserName.Text = cust.Username;
          
        }
        //don't allow user to make changes 
        private void ReadOnlyTrue()
        {
            txtFname.ReadOnly = true;
            txtLname.ReadOnly = true;
            txtAddress.ReadOnly = true;
            txtCity.ReadOnly = true;
            txtCountry.ReadOnly = true;
            txtPostalCode.ReadOnly = true;
            txtBusPhone.ReadOnly = true;
            txtHomePhone.ReadOnly = true;
            txtEmail.ReadOnly = true;
            txtPass.ReadOnly = true;
            txtConfirm.ReadOnly = true;
            txtUserName.ReadOnly = true;
        }

        //allow user to make changes 
        private void ReadOnlyFalse()
        {
            txtFname.ReadOnly = false;
            txtLname.ReadOnly = false;
            txtAddress.ReadOnly = false;
            txtCity.ReadOnly = false;
            txtCountry.ReadOnly = false;
            txtPostalCode.ReadOnly = false;
            txtBusPhone.ReadOnly = false;
            txtHomePhone.ReadOnly = false;
            txtEmail.ReadOnly = false;
            txtPass.ReadOnly = false;
            txtConfirm.ReadOnly = false;
            txtUserName.ReadOnly = false;
        }
        //make buttons visible when needed
        private void ButtonsNotVisible()
        {
            btnSave.Visible = false;
            btnCancel.Visible = false;
        }
        private void ButtonsVisible()
        {
            btnSave.Visible = true;
            btnCancel.Visible = true;
        }
        //hide fields when not needed
        private void HideFields()
        {
            lblAddress.Visible = false;
            lblProv.Visible = false;
            lblPostal.Visible = false;
            lblCountry.Visible = false;
            lblHome.Visible = false;
            lblBus.Visible = false;
            txtAddress.Visible = false;
            ddlProvUpdate.Visible = false;
            txtPostalCode.Visible = false;
            txtCountry.Visible = false;
            txtHomePhone.Visible = false;
            txtBusPhone.Visible = false;
           // lblPassword.Visible = false;
            txtPass.Visible = false;
            txtConfirm.Visible = false;
        }

        //display fiels when the user wants to update 
        private void DisplayFields()
        {
            lblAddress.Visible = true;
            lblProv.Visible = true;
            lblPostal.Visible = true;
            lblCountry.Visible = true;
            lblHome.Visible = true;
            lblBus.Visible = true;
            txtAddress.Visible = true;
            ddlProvUpdate.Visible = true;
            txtPostalCode.Visible = true;
            txtCountry.Visible = true;
            txtHomePhone.Visible = true;
            txtBusPhone.Visible = true;
            //lblPassword.Visible = true;
            txtPass.Visible = true;
            txtConfirm.Visible = true;
        }
    }
}