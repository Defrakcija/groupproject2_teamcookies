﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Author: Maru
/// Author: David
/// Content: customer registers, then checks for valid input, if input valid it is added to the data base. password is encdypted 
/// </summary>
namespace Main
{
    public partial class Register : System.Web.UI.Page
    {
        //re direct to profile page when not logged in
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Application["IsLoggedIn"] != null)
            {
                Response.Redirect("Profile.aspx");
            }
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }

        //stores all input into customer object
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Customers customers = new Customers();
            if (Page.IsValid)
            {
                bool value = false;

            customers.CustFirstName = txtFirstName.Text;
            customers.CustLastName = txtLastName.Text;
            customers.CustAddress = txtAddress.Text;
            customers.CustCity = txtCity.Text;
            customers.CustProv = ddlProvince.SelectedValue.ToString();
            customers.CustPostal = txtPostal.Text.ToUpper();
            customers.CustHomePhone = txtHome.Text;
            customers.CustBusPhone = txtPhone.Text;
            customers.CustEmail = txtEmail.Text;
            customers.Password = BCrypt.Net.BCrypt.HashPassword(txtPassword.Text);
            customers.Username = txtUser.Text;
            customers.CustCountry = "Canada";

            value = CustomersDB.AddCustomer(customers);
                //add the customer to data base
            if (value == true)
            {
                Session["FirstName"] = customers.CustFirstName.ToUpper() + ", Welcome to our team !!!";
                //Session["FirstName"] = "Welcome to our team";
                //Response.Write("<script>alert('You are now registered')</script>");
                Response.Redirect("Login.aspx");

            }
            else
                lblusertaken.Text = "Username already taken";
            // Response.Write("<script>alert('Username or email address is already taken, Please try again')</script>");

        }
    }
        //clears all input
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            ddlProvince.SelectedValue = "-1";
            txtPostal.Text = "";
            txtHome.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
            txtPassword.Text = "";
            txtUser.Text = "";
            lblusertaken.Text = "";
        }
    }
}