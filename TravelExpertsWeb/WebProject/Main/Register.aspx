﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Main.Register" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="mainPlaceholder">
    <link href="Style/registerStyle.css" rel="stylesheet" />
    <div class="login-card" >
        <h1><em>Customer Registration</em></h1>
        <fieldset>
        <legend>Customer Details</legend>
            <ul class="reglist">
                <li>
                    <asp:RequiredFieldValidator ID="rqdFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First Name is required" ForeColor="Red"></asp:RequiredFieldValidator>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <asp:RequiredFieldValidator ID="rqdLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Last name is required" ForeColor="Red"></asp:RequiredFieldValidator><br />
                    <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label>
                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="25"></asp:TextBox><asp:Label ID="Label2" runat="server" Text="Last Name"></asp:Label>
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="25"></asp:TextBox> 
                    
                    
                          
                </li>
                <li>
                    <asp:RequiredFieldValidator ID="rqdAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address is required" ForeColor="Red"></asp:RequiredFieldValidator>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <asp:RequiredFieldValidator ID="rqdCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" ForeColor="Red"></asp:RequiredFieldValidator><br />
                    <asp:Label ID="Label3" runat="server" Text="Address"></asp:Label>
                    <asp:TextBox ID="txtAddress" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:Label ID="Label4" runat="server" Text="City"></asp:Label>
                    <asp:TextBox ID="txtCity" runat="server" MaxLength="15" ></asp:TextBox>
                </li>
                <li>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorProvince" InitialValue="-1" runat="server" ControlToValidate="ddlProvince" ErrorMessage="Province is required" ForeColor="Red"></asp:RequiredFieldValidator><br/>
                    <asp:Label ID="Label11" runat="server" Text="Province/Territory"></asp:Label>     
                    <asp:DropDownList ID="ddlProvince" runat="server" Height="30px" Width="200px">
                        <asp:ListItem Text="Select a Province" value="-1" ></asp:ListItem>
                        <asp:ListItem Text="Alberta" Value="AB"></asp:ListItem>
                        <asp:ListItem Text="British Columbia" Value="BC"></asp:ListItem>
                        <asp:ListItem Text="Manitoba" Value="MB"></asp:ListItem>
                        <asp:ListItem Text="New Brunswick" Value="NB"></asp:ListItem>
                        <asp:ListItem Text="Newfoundland and Labrador" Value="NL"></asp:ListItem>
                        <asp:ListItem Text="Northwest Territories" Value="NT"></asp:ListItem>
                        <asp:ListItem Text="Nova Scotia" Value="NS"></asp:ListItem>
                        <asp:ListItem Text="Nunavut" Value="NU"></asp:ListItem>
                        <asp:ListItem Text="Ontario" Value="ON"></asp:ListItem>
                        <asp:ListItem Text="Prince Edward Island" Value="PE"></asp:ListItem>
                        <asp:ListItem Text="Quebec" Value="QC"></asp:ListItem>
                        <asp:ListItem Text="Saskatchewan" Value="SK"></asp:ListItem>
                        <asp:ListItem Text="Yukon" Value="YT"></asp:ListItem>
                    </asp:DropDownList>    
                </li>
                <li>
                    <asp:RequiredFieldValidator ID="rqdPostal" runat="server"  ControlToValidate="txtPostal" ErrorMessage="Postal code is required" ForeColor="Red"></asp:RequiredFieldValidator><br/>
                    <asp:Label ID="Label5" runat="server" Text="Postal"></asp:Label>
                    <asp:TextBox ID="txtPostal" runat="server" style="text-transform:uppercase"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPostal" runat="server" ControlToValidate="txtPostal" ErrorMessage="Please enter a valid postal code" ValidationExpression="([ABCEGHJKLMNPRSTVXY]|[abcdefghijklmnopqrstuvwxyz])[0-9]([ABCEGHJKLMNPRSTVWXYZ]|[abcdefghijklmnopqrstuvwxyz]) ?[0-9]([ABCEGHJKLMNPRSTVWXYZ]|[abcdefghijklmnopqrstuvwxyz])[0-9]" ForeColor="Red"></asp:RegularExpressionValidator>
                        
                </li>
                <li>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorhome" runat="server"  ControlToValidate="txtHome" ErrorMessage="Home phone number is required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorhome" runat="server" ControlToValidate="txtHome" ErrorMessage="Please enter a valid phone number" ValidationExpression="\(?([0-9]{3})\)?[-. ?]?([0-9]{3})[-. ?]?([0-9]{4})" ForeColor="Red"></asp:RegularExpressionValidator><br />
                    <asp:Label ID="Label9" runat="server" Text="Home Phone Number"></asp:Label>                  
                    <asp:TextBox ID="txtHome" runat="server"></asp:TextBox>                  
                    
                </li>
                <li>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorphone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Please enter a valid phone number" ValidationExpression="\(?([0-9]{3})\)?[-.?]?([0-9]{3})[-.?]?([0-9]{4})" ForeColor="Red"></asp:RegularExpressionValidator><br />
                    <asp:Label ID="Label6" runat="server" Text="Bussiness Phone Number" ></asp:Label>                    
                    <asp:TextBox ID="txtPhone" runat="server" Placeholder="Optional"></asp:TextBox>             
                </li>
                <li>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatoremail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter a valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator><br />
                    <asp:Label ID="Label7" runat="server" Text="Email Address"></asp:Label>                  
                    <asp:TextBox ID="txtEmail" runat="server" Placeholder="Optional" MaxLength="50"></asp:TextBox>                  
                </li>
                <li>
                    <asp:RequiredFieldValidator ID="rqdUser" runat="server" ControlToValidate="txtUser" ErrorMessage="Username is required" ForeColor="Red"></asp:RequiredFieldValidator><br />
                    <asp:Label ID="Label8" runat="server" Text="Username"></asp:Label>                   
                    <asp:TextBox ID="txtUser" runat="server" MaxLength="50"></asp:TextBox>                  
                </li>
                <asp:Label ID="lblusertaken" runat="server" TextColor="Red" ForeColor="Red"></asp:Label>
                <li>
                    <asp:RequiredFieldValidator ID="rqdPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password is required" ForeColor="Red"></asp:RequiredFieldValidator>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp    
                    <asp:RequiredFieldValidator ID="rqdPassword2" runat="server" ControlToValidate="txtPassword2" ErrorMessage="must retype password" ForeColor="Red"></asp:RequiredFieldValidator><br />
                    <asp:Label ID="Label10" runat="server" Text="Password"></asp:Label>       
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Placeholder="Password" MaxLength="50"></asp:TextBox>              
                    <asp:TextBox ID="txtPassword2" runat="server" ForeColor="Black" TextMode="Password" Placeholder="Re-type Password"></asp:TextBox><br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtPassword2" ErrorMessage="Password does not match" ForeColor="Red"></asp:CompareValidator>  <br /> 
                </li>
                <li>
                     <asp:Button ID="btnRegister" runat="server" OnClick="btnRegister_Click" Text="Register" class ="btn btn2" /><asp:Button ID="BtnClear" runat="server" Text="Clear" OnClick="BtnClear_Click" class ="btn btn3" CausesValidation="False"  />
                </li>            
            </ul>
        </fieldset>
    </div>
</asp:Content>
