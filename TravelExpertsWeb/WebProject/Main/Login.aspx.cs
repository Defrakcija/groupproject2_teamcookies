﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Author: Dmitry
/// </summary>
namespace Main
{
    public partial class Login : System.Web.UI.Page
    {
        int? currentCustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["FirstName"] != null)
            {
                lblRegStatus.Text = (string)Session["FirstName"];
                Session["FirstName"] = "";
            }
            if (Application["IsLoggedIn"] != null)
            {
                Response.Redirect("Profile.aspx");
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Customers cust = new Customers()
            {
                Username = txtUsername.Text,
                Password = txtPassword.Text,
            };
            currentCustomerID = CustomersDB.CheckCustomer(cust);
            if (currentCustomerID != null)
            {
                Application["IsLoggedIn"] = true;
                Application["currentUser"] = currentCustomerID;
                Server.Transfer("Profile.aspx", true);
                Button btn = (Button)Master.FindControl("btnSignUP");
                if(btn != null)
                {
                    btn.Visible = false;
                }
            }
            else
            {
                lblStatus.Text = "Wrong Username/Password";
            }
        }
    }
}