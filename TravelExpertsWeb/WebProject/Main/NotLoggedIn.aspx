﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="NotLoggedIn.aspx.cs" Inherits="Main.NotLoggedIn" %>
<%--Author : Nour--%>
<asp:Content ID="Content1" runat="server" contentplaceholderid="mainPlaceholder">
   <%-- MapPath image where the user can press on the image to redirect them to the login page --%>
    <asp:ImageMap ID="ImageMap1" runat="server" ImageUrl="~/Images/LOGIN.png" Height="263px" Width="955px">
        <asp:CircleHotSpot AlternateText="log in" HotSpotMode="Navigate" NavigateUrl="~/Login.aspx" Radius="90" X="280" Y="80" />
    </asp:ImageMap>
</asp:Content>
