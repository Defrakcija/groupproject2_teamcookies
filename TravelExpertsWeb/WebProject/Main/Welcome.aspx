﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="Main.Welcome" %>

<%--Author :Nour 
    Welcome page the first page the customer see gets styles from master page  --%>
<asp:Content ID="Content1" runat="server" contentplaceholderid="mainPlaceholder">
    <%--Multiple pictures and texts displayed just for styling--%> 
    <div class="w3-row w3-container" style="margin:50px 0">
    <div class="w3-half w3-container">
      <div class="w3-topbar w3-border-darkblue">
        <img src="Images/offerPackages.jpg" style="width:100%"/>
        <h2>Packages</h2>
        <p>We have variety of packages for you to choose from.</p>
      </div>
    </div>

    <div class="w3-half w3-container">
      <div class="w3-topbar w3-border-darkblue">
          <img src="Images/scenes.jpg" style="width:100% ;height = 50%"/>
        <h2>Super Offers</h2>
        <p>Up to 50% offers. Always 25% student offers.</p>
      </div>
    </div>
    </div>

    <div class="w3-row w3-container" style="margin:50px 0">
    <div class="w3-half w3-container">
      <div class="w3-topbar w3-border-darkblue">
        <img src="Images/CarRentals.jpg" style="width:100%"/>
        <h2>No Restrictions</h2>
        <p>Car rentals available with many of our packages</p>
      </div>
    </div>

    <div class="w3-half w3-container">
      <div class="w3-topbar w3-border-darkblue">
          <img src="Images/hotel.jpg" style="width:100%"/>
        <h2>Tours , Cruise , Travel Insurance</h2>
        <p>And so much more .</p>
        </div>
    </div>
    </div>
    <div class="w3-container w3-black w3-center w3-opacity w3-padding-64">
        <h1 class="w3-margin w3-xlarge">Quote of the day: live life</h1>
    </div
</asp:Content>
