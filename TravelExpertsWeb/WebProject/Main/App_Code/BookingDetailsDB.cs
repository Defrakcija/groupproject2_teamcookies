﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
//Author :Nour 

[DataObject(true)]
public class BookingDetailsDB
{
    [DataObjectMethod(DataObjectMethodType.Select)]

    //get the customer details 
    public static List<BookingDetails> GetBookingDetails(int custId)
    {

        List<BookingDetails> bookingDetails = new List<BookingDetails>(); //list to saverecords
        BookingDetails details;
        SqlConnection connection = TravelExpertsDB.GetConnection();//connect to database

        // define the select query command
        string selectQuery = "select Bookings.BookingNo , Bookings.BookingDate , Bookings.TravelerCount , " +
                             " BookingDetails.Destination , Fees.FeeName , Fees.FeeAmt , " +
                             " BookingDetails.BasePrice , " +
                            " Fees.FeeAmt +  BookingDetails.BasePrice * Bookings.TravelerCount as Total " +
                            " from Customers iNNer join Bookings " +
                            " on Bookings.CustomerId = Customers.CustomerId " +
                            " inner join BookingDetails on Bookings.BookingId = BookingDetails.BookingId " +
                            " inner join Fees on Fees.FeeId = BookingDetails.FeeId " +
                            " where Customers.CustomerId = @CustomerId " +
							" order by Bookings.BookingDate desc";
        SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
        selectCommand.Parameters.AddWithValue("@CustomerId", custId);//set parameter
        try
        {
            // open the connection
            connection.Open();

            // execute the query
            SqlDataReader reader = selectCommand.ExecuteReader();

            // process the results if any
            while (reader.Read()) // while there are customer
            {
                details = new BookingDetails();
                details.BookingNo = reader["BookingNo"].ToString();
                if (reader["BookingDate"] != null && reader["BookingDate"] != DBNull.Value)
                {
                    details.BookingDate = (DateTime?)reader["BookingDate"];
                }
                else
                {
                    details.BookingDate = (DateTime?)null;
                }

                if (reader["TravelerCount"] != null && reader["TravelerCount"] != DBNull.Value)
                {
                    details.TravelerCount = (double)reader["TravelerCount"];
                }
                else
                {
                    details.TravelerCount = 0;
                }


                if (reader["Destination"] != null && reader["Destination"] != DBNull.Value)
                {
                    details.Destination = reader["Destination"].ToString();
                }
                else
                {
                    details.Destination = "";
                }
                details.FeeName = reader["FeeName"].ToString();
                details.FeeAmt = (decimal) reader["FeeAmt"];
                if (reader["BasePrice"] != null && reader["BasePrice"] != DBNull.Value)
                {
                    details.BasePrice = (decimal)reader["BasePrice"];
                }
                else
                {
                    details.BasePrice = 0;
                }
                details.Total = Convert.ToDecimal(reader["Total"]);

                bookingDetails.Add(details);
            }
        }
        catch (Exception ex)
        {
            throw ex; // let the form handle it
        }
        finally
        {
            connection.Close(); // close connecto no matter what
        }

        return bookingDetails;//return the list 


    }
}

