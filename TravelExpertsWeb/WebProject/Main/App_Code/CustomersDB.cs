﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Author: Nour
/// Author: Maru
/// </summary>
[DataObject(true)]
public class CustomersDB
    {
        [DataObjectMethod(DataObjectMethodType.Select)]
        //get customer detail using id 
    public static Customers GetCustomerDetails(int customerId)
    {
        //Author: NOur
        
        // define connection
        Customers cust = new Customers();
        SqlConnection connection = TravelExpertsDB.GetConnection();

        // define the select query command
        string selectQuery = "select CustomerId , CustFirstName , CustLastName , CustAddress , " +
                             " CustCity , CustProv , CustPostal , CustCountry , " +
                             " CustHomePhone , CustBusPhone , CustEmail , AgentId , Username , Password " +
                             "  from Customers where @CustomerId = CustomerId";

        SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
        selectCommand.Parameters.AddWithValue("@CustomerId", customerId);
        try
        {
            // open the connection
            connection.Open();

            // execute the query
            SqlDataReader reader = selectCommand.ExecuteReader();

            // process the results if any
            if (reader.Read()) // while there are customer
            {
               
                cust.CustomerId = (int)reader["CustomerId"];
                cust.CustFirstName = reader["CustFirstName"].ToString();
                cust.CustLastName = reader["CustLastName"].ToString();
                cust.CustAddress = reader["CustAddress"].ToString();
                cust.CustCity = reader["CustCity"].ToString();
                cust.CustProv = reader["CustProv"].ToString();
                cust.CustPostal = reader["CustPostal"].ToString();

                if (reader["CustCountry"] != null && reader["CustCountry"] != DBNull.Value)
                {
                    cust.CustCountry = reader["CustCountry"].ToString();
                }
                else
                {
                    cust.CustCountry = "";
                }

                if (reader["CustHomePhone"] != null && reader["CustHomePhone"] != DBNull.Value)
                {
                    cust.CustHomePhone = reader["CustHomePhone"].ToString();
                }
                else
                {
                    cust.CustHomePhone = "";
                }
                cust.CustBusPhone = reader["CustBusPhone"].ToString();
                cust.CustEmail = reader["CustEmail"].ToString();

                if (reader["AgentId"] != null && reader["AgentId"] != DBNull.Value)
                {
                    cust.AgentId = (int)reader["AgentId"];
                }
                else
                {
                    cust.AgentId = 0;
                }

                
                if (reader["Username"] != null && reader["Username"] != DBNull.Value)
                {
                    cust.Username = reader["Username"].ToString();
                }
                else
                {
                    cust.Username = "";
                }
                

                if (reader["Password"] != null && reader["Password"] != DBNull.Value)
                {
                    cust.Password = reader["Password"].ToString();
                }
                else
                {
                    cust.Password = "";
                }
                //customers.Add(cust);
            }
        }
        catch (Exception ex)
        {
            throw ex; // let the form handle it
        }
        finally
        {
            connection.Close(); // close connecto no matter what
        }

        return cust;


    }

    [DataObjectMethod(DataObjectMethodType.Update)]
    public static bool UpdateCustomer(Customers oldCustomer, Customers newCustomer)
    {
        //Author : Nour 
        //create connection 
        SqlConnection con = TravelExpertsDB.GetConnection();
        // update query 
        string updateStatement = "UPDATE Customers SET " +
                                " CustFirstName = @NewCustFirstName , " +
                                " CustLastName = @NewCustLastName , " +
                                " CustAddress = @NewCustAddress , " +
                                " CustCity = @NewCustCity , " +
                                " CustProv = @NewCustProv , " +
                                " CustPostal = @NewCustPostal , " +
                                " CustCountry = @NewCustCountry , " +
                                " CustHomePhone = @NewCustHomePhone , " +
                                " CustBusPhone = @NewCustBusPhone , " +
                                " CustEmail = @NewCustEmail , " +
                                " Username = @NewUsername , " + 
                                " Password = @NewPassword " +
                                " WHERE CustomerId = @OldCustomerId ";

        SqlCommand updateCommand = new SqlCommand(updateStatement, con);
        // set parameters
        updateCommand.Parameters.AddWithValue("@NewCustFirstName", newCustomer.CustFirstName);
        updateCommand.Parameters.AddWithValue("@NewCustLastName", newCustomer.CustLastName);
        updateCommand.Parameters.AddWithValue("@NewCustAddress", newCustomer.CustAddress);
        updateCommand.Parameters.AddWithValue("@NewCustCity", newCustomer.CustCity);
        updateCommand.Parameters.AddWithValue("@NewCustProv", newCustomer.CustProv);
        updateCommand.Parameters.AddWithValue("@NewCustPostal", newCustomer.CustPostal);
        updateCommand.Parameters.AddWithValue("@NewCustCountry", newCustomer.CustCountry);
        updateCommand.Parameters.AddWithValue("@NewCustHomePhone", newCustomer.CustHomePhone);
        updateCommand.Parameters.AddWithValue("@NewCustBusPhone", newCustomer.CustBusPhone);
        if (newCustomer.CustEmail == null)
        {
            updateCommand.Parameters.AddWithValue("@NewCustEmail", DBNull.Value);
        }
        else
            updateCommand.Parameters.AddWithValue("@NewCustEmail", newCustomer.CustEmail);

        if (newCustomer.Username == null)
        {
            updateCommand.Parameters.AddWithValue("@NewUsername", DBNull.Value);
        }
        else
            updateCommand.Parameters.AddWithValue("@NewUsername", newCustomer.Username);


        if (newCustomer.Password == null)
        {
            updateCommand.Parameters.AddWithValue("@NewPassword", DBNull.Value);
        }
        else
            updateCommand.Parameters.AddWithValue("@NewPassword", newCustomer.Password);


        updateCommand.Parameters.AddWithValue("@OldCustomerId", oldCustomer.CustomerId);
        try
        {
            //start connection
            con.Open();
            //if the query works 
            int count = updateCommand.ExecuteNonQuery();
            if (count > 0)
                return true;
            else
                return false;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    //Author : MARU 
    public static bool AddCustomer(Customers customer)
    {
        
        SqlConnection connection = TravelExpertsDB.GetConnection();

        string insertStatement = "Insert into [Customers] (CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustHomePhone, CustBusPhone, CustEmail, Password, Username, CustCountry) " +
        "SELECT @CustFirstName, @CustLastName, @CustAddress, @CustCity, @CustProv, @CustPostal, @CustHomePhone, @CustBusPhone, @CustEmail, @Password, @Username ,@CustCountry " +
        "WHERE not exists(select CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustHomePhone, CustBusPhone, CustEmail, Password, Username ,CustCountry " +
        "from Customers where Username = @Username)";
        SqlCommand insertCommand =
            new SqlCommand(insertStatement, connection);
        insertCommand.Parameters.AddWithValue("@CustFirstName", customer.CustFirstName);
        insertCommand.Parameters.AddWithValue("@CustLastName", customer.CustLastName);
        insertCommand.Parameters.AddWithValue("@CustAddress", customer.CustAddress);
        insertCommand.Parameters.AddWithValue("@CustCity", customer.CustCity);
        insertCommand.Parameters.AddWithValue("@CustProv", customer.CustProv);
        insertCommand.Parameters.AddWithValue("@CustPostal", customer.CustPostal);
        insertCommand.Parameters.AddWithValue("@CustHomePhone", customer.CustHomePhone);
        if (customer.CustBusPhone == null)
        {
            insertCommand.Parameters.AddWithValue("@CustBusPhone", DBNull.Value);
        }
        else
            insertCommand.Parameters.AddWithValue("@CustBusPhone", customer.CustBusPhone);




        if (customer.CustEmail == null)
        {
            insertCommand.Parameters.AddWithValue("@CustEmail", DBNull.Value);
        }
        else
            insertCommand.Parameters.AddWithValue("@CustEmail", customer.CustEmail);
        insertCommand.Parameters.AddWithValue("@Password", customer.Password);
        insertCommand.Parameters.AddWithValue("@Username", customer.Username);
        insertCommand.Parameters.AddWithValue("@CustCountry", customer.CustCountry);


        try
        {
            connection.Open();
            int COUNT = insertCommand.ExecuteNonQuery();


            if (COUNT > 0)
                return true;
            else
                return false;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }


    [DataObjectMethod(DataObjectMethodType.Select)]

    public static int? CheckCustomer(Customers cust)
    {
        //Author: Dmitry
        int? cusID = null;
        const string query = "SELECT CustomerId, Username, Password FROM Customers WHERE Username = @username";

        using (SqlConnection conn = TravelExpertsDB.GetConnection())
        using (SqlCommand cmd = new SqlCommand(query, conn))
        {
            cmd.Parameters.AddWithValue("@username", cust.Username);
            conn.Open();

            //string hashedPassword = BCrypt.Net.BCrypt.HashPassword(passwordtextbox.Text);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    string storedPassword = reader["Password"].ToString();
                    if (BCrypt.Net.BCrypt.Verify(cust.Password, storedPassword))
                    {
                        cusID = Convert.ToInt32(reader["CustomerId"]);
                    }
                }

            }
            return cusID;
        }
    }
}

