﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
/// <summary>
/// Author: Team4
/// </summary>
public class TravelExpertsDB
    {
        public static SqlConnection GetConnection()
        {

            string connString = ConfigurationManager.ConnectionStrings["TravelExpertsConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            return conn;
        }
    }

