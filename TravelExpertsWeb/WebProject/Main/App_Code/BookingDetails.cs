﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//Author :Nour 

    public class BookingDetails
    {
    // public properties for this class
        public string BookingNo { get; set; }
        public DateTime? BookingDate { get; set; }
        public double TravelerCount { get; set; }
        public string Destination { get; set; }
        public string FeeName { get; set; }
        public decimal FeeAmt { get; set; }
        public decimal BasePrice { get; set; }

        public decimal Total { get; set; }

    }
