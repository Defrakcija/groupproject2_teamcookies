﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using BCrypt.Net;

namespace CustomerRegistration
{
    public partial class CustomerRegistration : System.Web.UI.Page
    {

        
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }
        private string EncryptPassword(string pass)
        {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(pass);
            string EncryptedPassword = Convert.ToBase64String(bytes);
            return EncryptedPassword;
        }
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            //string h = BCrypt.Net.BCrypt.HashPassword(txtPassword.Text);
            Customers customers = new Customers();
            if (Page.IsValid)
            {
                bool value = false;

                customers.CustFirstName = txtFirstName.Text;
                customers.CustLastName = txtLastName.Text;
                customers.CustAddress = txtAddress.Text;
                customers.CustCity = txtCity.Text;
                customers.CustProv = txtProv.Text;
                customers.CustPostal = txtPostal.Text.ToUpper();
                customers.CustHomePhone = txtHome.Text;
                customers.CustBusPhone = txtPhone.Text;
                customers.CustEmail = txtEmail.Text;
                customers.Password = txtPassword.Text;
                customers.Username = txtUser.Text;


                value = CustomersDB.AddCustomer(customers);
                if (value == true)
                {
                    lblDataStatus.Text = "Saved";

                }
                else
                    lblDataStatus.Text = "notsaved";

            }
        }
   }

}