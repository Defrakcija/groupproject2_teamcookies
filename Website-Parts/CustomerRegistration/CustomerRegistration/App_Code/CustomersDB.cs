﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

[DataObject(true)]
public class CustomersDB
    {
    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static bool AddCustomer(Customers customer)
    {
        bool value = true;
        SqlConnection connection = TravelExpertsDB.GetConnection();
        //string insertStatement =
        //    "Insert into Customers With (ROWLOCK) (CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustHomePhone, CustBusPhone, CustEmail, Password, Username) " +
        //    "SELECT @CustFirstName, @CustLastName, @CustAddress, @CustCity, @CustProv, @CustPostal, @CustHomePhone, @CustBusPhone, @CustEmail, @Password, @Username "+
        //    "WHERE not exists(select CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustHomePhone, CustBusPhone, CustEmail, Password, Username " +
        //    "from Customers where CustEmail = @CustEmail and Username = @Username)";
        string insertStatement = "Insert into Customers With (ROWLOCK)(CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustHomePhone, CustBusPhone, CustEmail, Password, Username) " +
        "SELECT @CustFirstName, @CustLastName, @CustAddress, @CustCity, @CustProv, @CustPostal, @CustHomePhone, @CustBusPhone, @CustEmail, @Password, @Username " +
        "WHERE not exists(select CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustHomePhone, CustBusPhone, CustEmail, Password, Username " +
        "from Customers where CustEmail = @CustEmail and Username = @Username)";
        SqlCommand insertCommand =
            new SqlCommand(insertStatement, connection);
        insertCommand.Parameters.AddWithValue("@CustFirstName", customer.CustFirstName);
        insertCommand.Parameters.AddWithValue("@CustLastName", customer.CustLastName);
        insertCommand.Parameters.AddWithValue("@CustAddress", customer.CustAddress);
        insertCommand.Parameters.AddWithValue("@CustCity", customer.CustCity);
        insertCommand.Parameters.AddWithValue("@CustProv", customer.CustProv);
        insertCommand.Parameters.AddWithValue("@CustPostal", customer.CustPostal);
        insertCommand.Parameters.AddWithValue("@CustHomePhone", customer.CustHomePhone);
        insertCommand.Parameters.AddWithValue("@CustBusPhone", customer.CustBusPhone);
        insertCommand.Parameters.AddWithValue("@CustEmail", customer.CustEmail);
        insertCommand.Parameters.AddWithValue("@Password", customer.Password);
        insertCommand.Parameters.AddWithValue("@Username", customer.Username);
        try
        {   
            connection.Open();
            insertCommand.ExecuteNonQuery();
            string selectStatement =
                "SELECT IDENT_CURRENT('Customers') FROM Customers";
            SqlCommand selectCommand =
                new SqlCommand(selectStatement, connection);
            int customerID = Convert.ToInt32(selectCommand.ExecuteScalar());
            int count = insertCommand.ExecuteNonQuery();
            if (count > 0)
                return value;
            else
                return false;
        }
        catch (SqlException ex)
        {

            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }
}
