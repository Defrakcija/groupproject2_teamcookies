﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

    public class Customers
    {
        public Customers() { }
        public string CustFirstName { get; set; }
        public string CustLastName { get; set; }
        public string CustAddress { get; set; }
        public string CustCity { get; set; }
        public string CustProv { get; set; }
        public string CustPostal { get; set; }
        public string CustHomePhone { get; set; }
        public string CustBusPhone { get; set; }
        public string CustEmail { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
}
