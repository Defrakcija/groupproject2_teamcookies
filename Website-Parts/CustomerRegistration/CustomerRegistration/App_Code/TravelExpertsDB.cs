﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class TravelExpertsDB
    {
        public static SqlConnection GetConnection()
        {
            //string connString = "@Data Source=(LocalDB)\\MSSQLLocalDB;" +
            //    "AttachDbFilename=|DataDirectory|\\TravelExperts.mdf;" +
            //    "Integrated Security=True;Connect Timeout=30";
        string connString =
          ConfigurationManager.ConnectionStrings["TravelExpertConnection"].ConnectionString;

        SqlConnection conn = new SqlConnection(connString);
            return conn;
        }
    }
