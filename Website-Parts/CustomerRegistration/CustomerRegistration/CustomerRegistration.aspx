﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerRegistration.aspx.cs" Inherits="CustomerRegistration.CustomerRegistration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 165px;
        }
        .auto-style3 {
            width: 100px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <h1>
            <em>Customer Registration</em></h1>
        <p>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style3"><asp:Label ID="Label1" runat="server" Text="First Name:"></asp:Label></td>
                    <td class="auto-style2">

        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                    </td>
                    <td>
        <asp:RequiredFieldValidator ID="rqdFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First Name is required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3"><asp:Label ID="Label2" runat="server" Text="Last Name:"></asp:Label></td>
                    <td class="auto-style2">


        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                    </td>
                    <td>
        <asp:RequiredFieldValidator ID="rqdLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Last name is required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label3" runat="server" Text="Address:"></asp:Label>
                    </td>
                    <td class="auto-style2">
    

            <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                    </td>
                    <td>
            <asp:RequiredFieldValidator ID="rqdAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address is required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label4" runat="server" Text="City"></asp:Label>
                    </td>
                    <td class="auto-style2">
            <asp:TextBox ID="txtCity" runat="server" ></asp:TextBox>
                    </td>
                    <td>
            <asp:RequiredFieldValidator ID="rqdCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Province</td>
                    <td class="auto-style2">
            <asp:TextBox ID="txtProv" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProv" ErrorMessage="Province is required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label5" runat="server" Text="Postal"></asp:Label>
                    </td>
                    <td class="auto-style2">
            <asp:TextBox ID="txtPostal" runat="server" style="text-transform:uppercase"></asp:TextBox>
                    </td>
                    <td>
            <asp:RequiredFieldValidator ID="rqdPostal" runat="server"  ControlToValidate="txtPostal" ErrorMessage="Postal code is required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorPostal" runat="server" ControlToValidate="txtPostal" ErrorMessage="Please enter a valid postal code" ValidationExpression="([ABCEGHJKLMNPRSTVXY]|[abcdefghijklmnopqrstuvwxyz])[0-9]([ABCEGHJKLMNPRSTVWXYZ]|[abcdefghijklmnopqrstuvwxyz]) ?[0-9]([ABCEGHJKLMNPRSTVWXYZ]|[abcdefghijklmnopqrstuvwxyz])[0-9]" ForeColor="Red"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label9" runat="server" Text="Home Phone Number:"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtHome" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorHome" runat="server" ControlToValidate="txtPhone" ErrorMessage="Home phone number is required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorHome" runat="server" ControlToValidate="txtHome" ErrorMessage="Please enter a valid phone number" ForeColor="Red" ValidationExpression="\(?([0-9]{3})\)?[-.?]?([0-9]{3})[-.?]?([0-9]{4})"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label6" runat="server" Text="Bussiness Phone Number:"></asp:Label>
                    </td>
                    <td class="auto-style2">
            <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorphone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Please enter a valid phone number" ValidationExpression="\(?([0-9]{3})\)?[-.?]?([0-9]{3})[-.?]?([0-9]{4})" ForeColor="Red"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label7" runat="server" Text="Email Adress"></asp:Label>
                    </td>
                    <td class="auto-style2">
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatoremail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter a valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label8" runat="server" Text="Username"></asp:Label>
                    </td>
                    <td class="auto-style2">
            <asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
                    </td>
                    <td>
            <asp:RequiredFieldValidator ID="rqdUser" runat="server" ControlToValidate="txtUser" ErrorMessage="Username is required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Password:</td>
                    <td class="auto-style2">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
            <asp:TextBox ID="txtPassword2" runat="server" ForeColor="Red" TextMode="Password"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtPassword2" ErrorMessage="Password does not match" ForeColor="Red"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style2">
            <asp:RequiredFieldValidator ID="rqdPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password is requied" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td>
            <asp:RequiredFieldValidator ID="rqdPassword2" runat="server" ControlToValidate="txtPassword2" ErrorMessage="must retype password" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style2">
            <asp:Button ID="btnRegister" runat="server" OnClick="btnRegister_Click" Text="Button" />
                    </td>
                    <td>
            <asp:Label ID="lblDataStatus" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
            </table>
        </p>
        <p>
            &nbsp;</p>
        <p>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"></asp:ObjectDataSource>
        </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
    </form>
</body>
</html>
