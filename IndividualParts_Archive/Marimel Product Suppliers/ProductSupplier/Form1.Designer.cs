﻿namespace ProductSupplier
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboSuppliers = new System.Windows.Forms.ComboBox();
            this.btnaddProductSupplier = new System.Windows.Forms.Button();
            this.tabProductSupplier = new System.Windows.Forms.TabControl();
            this.tabDisplay = new System.Windows.Forms.TabPage();
            this.rbtnSuName = new System.Windows.Forms.RadioButton();
            this.rbtnProdName = new System.Windows.Forms.RadioButton();
            this.rbtnSupplierID = new System.Windows.Forms.RadioButton();
            this.rbtnProductID = new System.Windows.Forms.RadioButton();
            this.rbtnPrductSupplier = new System.Windows.Forms.RadioButton();
            this.lblEmpty = new System.Windows.Forms.Label();
            this.gridProductSupplier = new System.Windows.Forms.DataGridView();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDisplayAll = new System.Windows.Forms.Button();
            this.cboProductNames = new System.Windows.Forms.ComboBox();
            this.lblSelected = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.lblSupplierID = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblProductID = new System.Windows.Forms.Label();
            this.lblProductSupplierID = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboSupplierNames = new System.Windows.Forms.ComboBox();
            this.cboSupplierIDs = new System.Windows.Forms.ComboBox();
            this.cboProductIDs = new System.Windows.Forms.ComboBox();
            this.cboProductSupplierID = new System.Windows.Forms.ComboBox();
            this.tabAddRemove = new System.Windows.Forms.TabPage();
            this.gridProductSuppliersAddEdit = new System.Windows.Forms.DataGridView();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.rbtnUpdate = new System.Windows.Forms.RadioButton();
            this.rbtnRemove = new System.Windows.Forms.RadioButton();
            this.rbtnAdd = new System.Windows.Forms.RadioButton();
            this.lblProductSupplier = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.cboProductSupplierDelete = new System.Windows.Forms.ComboBox();
            this.lblSupplier = new System.Windows.Forms.Label();
            this.lblProduct = new System.Windows.Forms.Label();
            this.lblInstruction = new System.Windows.Forms.Label();
            this.cboProducts = new System.Windows.Forms.ComboBox();
            this.tabProductSupplier.SuspendLayout();
            this.tabDisplay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductSupplier)).BeginInit();
            this.tabAddRemove.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductSuppliersAddEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // cboSuppliers
            // 
            this.cboSuppliers.FormattingEnabled = true;
            this.cboSuppliers.Location = new System.Drawing.Point(462, 65);
            this.cboSuppliers.Name = "cboSuppliers";
            this.cboSuppliers.Size = new System.Drawing.Size(149, 21);
            this.cboSuppliers.TabIndex = 2;
            // 
            // btnaddProductSupplier
            // 
            this.btnaddProductSupplier.Location = new System.Drawing.Point(627, 63);
            this.btnaddProductSupplier.Name = "btnaddProductSupplier";
            this.btnaddProductSupplier.Size = new System.Drawing.Size(75, 23);
            this.btnaddProductSupplier.TabIndex = 3;
            this.btnaddProductSupplier.Text = "Add";
            this.btnaddProductSupplier.UseVisualStyleBackColor = true;
            this.btnaddProductSupplier.Click += new System.EventHandler(this.addProductSupplier_Click);
            // 
            // tabProductSupplier
            // 
            this.tabProductSupplier.Controls.Add(this.tabDisplay);
            this.tabProductSupplier.Controls.Add(this.tabAddRemove);
            this.tabProductSupplier.Location = new System.Drawing.Point(36, 54);
            this.tabProductSupplier.Name = "tabProductSupplier";
            this.tabProductSupplier.SelectedIndex = 0;
            this.tabProductSupplier.Size = new System.Drawing.Size(743, 339);
            this.tabProductSupplier.TabIndex = 4;
            // 
            // tabDisplay
            // 
            this.tabDisplay.Controls.Add(this.rbtnSuName);
            this.tabDisplay.Controls.Add(this.rbtnProdName);
            this.tabDisplay.Controls.Add(this.rbtnSupplierID);
            this.tabDisplay.Controls.Add(this.rbtnProductID);
            this.tabDisplay.Controls.Add(this.rbtnPrductSupplier);
            this.tabDisplay.Controls.Add(this.lblEmpty);
            this.tabDisplay.Controls.Add(this.gridProductSupplier);
            this.tabDisplay.Controls.Add(this.btnClear);
            this.tabDisplay.Controls.Add(this.btnDisplayAll);
            this.tabDisplay.Controls.Add(this.cboProductNames);
            this.tabDisplay.Controls.Add(this.lblSelected);
            this.tabDisplay.Controls.Add(this.lbl);
            this.tabDisplay.Controls.Add(this.lblSupplierName);
            this.tabDisplay.Controls.Add(this.lblSupplierID);
            this.tabDisplay.Controls.Add(this.lblProductName);
            this.tabDisplay.Controls.Add(this.lblProductID);
            this.tabDisplay.Controls.Add(this.lblProductSupplierID);
            this.tabDisplay.Controls.Add(this.label2);
            this.tabDisplay.Controls.Add(this.cboSupplierNames);
            this.tabDisplay.Controls.Add(this.cboSupplierIDs);
            this.tabDisplay.Controls.Add(this.cboProductIDs);
            this.tabDisplay.Controls.Add(this.cboProductSupplierID);
            this.tabDisplay.Location = new System.Drawing.Point(4, 22);
            this.tabDisplay.Name = "tabDisplay";
            this.tabDisplay.Padding = new System.Windows.Forms.Padding(3);
            this.tabDisplay.Size = new System.Drawing.Size(735, 313);
            this.tabDisplay.TabIndex = 0;
            this.tabDisplay.Text = "Display";
            this.tabDisplay.UseVisualStyleBackColor = true;
            // 
            // rbtnSuName
            // 
            this.rbtnSuName.AutoSize = true;
            this.rbtnSuName.Location = new System.Drawing.Point(480, 12);
            this.rbtnSuName.Name = "rbtnSuName";
            this.rbtnSuName.Size = new System.Drawing.Size(94, 17);
            this.rbtnSuName.TabIndex = 21;
            this.rbtnSuName.TabStop = true;
            this.rbtnSuName.Text = "Supplier Name";
            this.rbtnSuName.UseVisualStyleBackColor = true;
            this.rbtnSuName.CheckedChanged += new System.EventHandler(this.rbtnSuName_CheckedChanged);
            // 
            // rbtnProdName
            // 
            this.rbtnProdName.AutoSize = true;
            this.rbtnProdName.Location = new System.Drawing.Point(298, 12);
            this.rbtnProdName.Name = "rbtnProdName";
            this.rbtnProdName.Size = new System.Drawing.Size(93, 17);
            this.rbtnProdName.TabIndex = 20;
            this.rbtnProdName.TabStop = true;
            this.rbtnProdName.Text = "Product Name";
            this.rbtnProdName.UseVisualStyleBackColor = true;
            this.rbtnProdName.CheckedChanged += new System.EventHandler(this.rbtnProdName_CheckedChanged);
            // 
            // rbtnSupplierID
            // 
            this.rbtnSupplierID.AutoSize = true;
            this.rbtnSupplierID.Location = new System.Drawing.Point(397, 12);
            this.rbtnSupplierID.Name = "rbtnSupplierID";
            this.rbtnSupplierID.Size = new System.Drawing.Size(77, 17);
            this.rbtnSupplierID.TabIndex = 19;
            this.rbtnSupplierID.TabStop = true;
            this.rbtnSupplierID.Text = "Supplier ID";
            this.rbtnSupplierID.UseVisualStyleBackColor = true;
            this.rbtnSupplierID.CheckedChanged += new System.EventHandler(this.rbtnSupplierID_CheckedChanged);
            // 
            // rbtnProductID
            // 
            this.rbtnProductID.AutoSize = true;
            this.rbtnProductID.Location = new System.Drawing.Point(214, 12);
            this.rbtnProductID.Name = "rbtnProductID";
            this.rbtnProductID.Size = new System.Drawing.Size(76, 17);
            this.rbtnProductID.TabIndex = 18;
            this.rbtnProductID.TabStop = true;
            this.rbtnProductID.Text = "Product ID";
            this.rbtnProductID.UseVisualStyleBackColor = true;
            this.rbtnProductID.CheckedChanged += new System.EventHandler(this.rbtnProductID_CheckedChanged);
            // 
            // rbtnPrductSupplier
            // 
            this.rbtnPrductSupplier.AutoSize = true;
            this.rbtnPrductSupplier.Location = new System.Drawing.Point(91, 12);
            this.rbtnPrductSupplier.Name = "rbtnPrductSupplier";
            this.rbtnPrductSupplier.Size = new System.Drawing.Size(117, 17);
            this.rbtnPrductSupplier.TabIndex = 17;
            this.rbtnPrductSupplier.TabStop = true;
            this.rbtnPrductSupplier.Text = "Product Supplier ID";
            this.rbtnPrductSupplier.UseVisualStyleBackColor = true;
            this.rbtnPrductSupplier.CheckedChanged += new System.EventHandler(this.rbtnPrductSupplier_CheckedChanged);
            // 
            // lblEmpty
            // 
            this.lblEmpty.AutoSize = true;
            this.lblEmpty.Location = new System.Drawing.Point(64, 149);
            this.lblEmpty.Name = "lblEmpty";
            this.lblEmpty.Size = new System.Drawing.Size(35, 13);
            this.lblEmpty.TabIndex = 16;
            this.lblEmpty.Text = "label7";
            // 
            // gridProductSupplier
            // 
            this.gridProductSupplier.AllowUserToOrderColumns = true;
            this.gridProductSupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridProductSupplier.Location = new System.Drawing.Point(22, 104);
            this.gridProductSupplier.MultiSelect = false;
            this.gridProductSupplier.Name = "gridProductSupplier";
            this.gridProductSupplier.ReadOnly = true;
            this.gridProductSupplier.Size = new System.Drawing.Size(687, 174);
            this.gridProductSupplier.TabIndex = 15;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(634, 284);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 14;
            this.btnClear.Text = "Clear List";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDisplayAll
            // 
            this.btnDisplayAll.Location = new System.Drawing.Point(553, 284);
            this.btnDisplayAll.Name = "btnDisplayAll";
            this.btnDisplayAll.Size = new System.Drawing.Size(75, 23);
            this.btnDisplayAll.TabIndex = 13;
            this.btnDisplayAll.Text = "Display All";
            this.btnDisplayAll.UseVisualStyleBackColor = true;
            this.btnDisplayAll.Click += new System.EventHandler(this.btnDisplayAll_Click);
            // 
            // cboProductNames
            // 
            this.cboProductNames.FormattingEnabled = true;
            this.cboProductNames.Location = new System.Drawing.Point(218, 64);
            this.cboProductNames.Name = "cboProductNames";
            this.cboProductNames.Size = new System.Drawing.Size(114, 21);
            this.cboProductNames.TabIndex = 5;
            this.cboProductNames.SelectedIndexChanged += new System.EventHandler(this.cboProductNames_SelectedIndexChanged);
            // 
            // lblSelected
            // 
            this.lblSelected.AutoSize = true;
            this.lblSelected.Location = new System.Drawing.Point(64, 72);
            this.lblSelected.Name = "lblSelected";
            this.lblSelected.Size = new System.Drawing.Size(0, 13);
            this.lblSelected.TabIndex = 12;
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(19, 88);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(52, 13);
            this.lbl.TabIndex = 11;
            this.lbl.Text = "Selected:";
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.AutoSize = true;
            this.lblSupplierName.Location = new System.Drawing.Point(538, 48);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(76, 13);
            this.lblSupplierName.TabIndex = 10;
            this.lblSupplierName.Text = "Supplier Name";
            // 
            // lblSupplierID
            // 
            this.lblSupplierID.AutoSize = true;
            this.lblSupplierID.Location = new System.Drawing.Point(394, 48);
            this.lblSupplierID.Name = "lblSupplierID";
            this.lblSupplierID.Size = new System.Drawing.Size(59, 13);
            this.lblSupplierID.TabIndex = 9;
            this.lblSupplierID.Text = "Supplier ID";
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Location = new System.Drawing.Point(215, 48);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(75, 13);
            this.lblProductName.TabIndex = 5;
            this.lblProductName.Text = "Product Name";
            // 
            // lblProductID
            // 
            this.lblProductID.AutoSize = true;
            this.lblProductID.Location = new System.Drawing.Point(120, 48);
            this.lblProductID.Name = "lblProductID";
            this.lblProductID.Size = new System.Drawing.Size(58, 13);
            this.lblProductID.TabIndex = 8;
            this.lblProductID.Text = "Product ID";
            // 
            // lblProductSupplierID
            // 
            this.lblProductSupplierID.AutoSize = true;
            this.lblProductSupplierID.Location = new System.Drawing.Point(20, 48);
            this.lblProductSupplierID.Name = "lblProductSupplierID";
            this.lblProductSupplierID.Size = new System.Drawing.Size(99, 13);
            this.lblProductSupplierID.TabIndex = 7;
            this.lblProductSupplierID.Text = "Product Supplier ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Sort by:";
            // 
            // cboSupplierNames
            // 
            this.cboSupplierNames.FormattingEnabled = true;
            this.cboSupplierNames.Location = new System.Drawing.Point(541, 64);
            this.cboSupplierNames.Name = "cboSupplierNames";
            this.cboSupplierNames.Size = new System.Drawing.Size(168, 21);
            this.cboSupplierNames.TabIndex = 5;
            this.cboSupplierNames.SelectedIndexChanged += new System.EventHandler(this.cboSupplierNames_SelectedIndexChanged);
            // 
            // cboSupplierIDs
            // 
            this.cboSupplierIDs.FormattingEnabled = true;
            this.cboSupplierIDs.Location = new System.Drawing.Point(397, 64);
            this.cboSupplierIDs.Name = "cboSupplierIDs";
            this.cboSupplierIDs.Size = new System.Drawing.Size(69, 21);
            this.cboSupplierIDs.TabIndex = 4;
            this.cboSupplierIDs.SelectedIndexChanged += new System.EventHandler(this.cboSupplierIDs_SelectedIndexChanged);
            // 
            // cboProductIDs
            // 
            this.cboProductIDs.FormattingEnabled = true;
            this.cboProductIDs.Location = new System.Drawing.Point(123, 64);
            this.cboProductIDs.Name = "cboProductIDs";
            this.cboProductIDs.Size = new System.Drawing.Size(66, 21);
            this.cboProductIDs.TabIndex = 3;
            this.cboProductIDs.SelectedIndexChanged += new System.EventHandler(this.cboProductID_SelectedIndexChanged);
            // 
            // cboProductSupplierID
            // 
            this.cboProductSupplierID.FormattingEnabled = true;
            this.cboProductSupplierID.Location = new System.Drawing.Point(23, 64);
            this.cboProductSupplierID.Name = "cboProductSupplierID";
            this.cboProductSupplierID.Size = new System.Drawing.Size(41, 21);
            this.cboProductSupplierID.TabIndex = 2;
            this.cboProductSupplierID.SelectedIndexChanged += new System.EventHandler(this.cboProductSupplierID_SelectedIndexChanged);
            // 
            // tabAddRemove
            // 
            this.tabAddRemove.Controls.Add(this.gridProductSuppliersAddEdit);
            this.tabAddRemove.Controls.Add(this.btnUpdate);
            this.tabAddRemove.Controls.Add(this.rbtnUpdate);
            this.tabAddRemove.Controls.Add(this.rbtnRemove);
            this.tabAddRemove.Controls.Add(this.rbtnAdd);
            this.tabAddRemove.Controls.Add(this.lblProductSupplier);
            this.tabAddRemove.Controls.Add(this.btnRemove);
            this.tabAddRemove.Controls.Add(this.cboProductSupplierDelete);
            this.tabAddRemove.Controls.Add(this.lblSupplier);
            this.tabAddRemove.Controls.Add(this.lblProduct);
            this.tabAddRemove.Controls.Add(this.lblInstruction);
            this.tabAddRemove.Controls.Add(this.cboProducts);
            this.tabAddRemove.Controls.Add(this.cboSuppliers);
            this.tabAddRemove.Controls.Add(this.btnaddProductSupplier);
            this.tabAddRemove.Location = new System.Drawing.Point(4, 22);
            this.tabAddRemove.Name = "tabAddRemove";
            this.tabAddRemove.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddRemove.Size = new System.Drawing.Size(735, 313);
            this.tabAddRemove.TabIndex = 1;
            this.tabAddRemove.Text = "Add/Remove/Update";
            this.tabAddRemove.UseVisualStyleBackColor = true;
            // 
            // gridProductSuppliersAddEdit
            // 
            this.gridProductSuppliersAddEdit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridProductSuppliersAddEdit.Location = new System.Drawing.Point(22, 92);
            this.gridProductSuppliersAddEdit.MultiSelect = false;
            this.gridProductSuppliersAddEdit.Name = "gridProductSuppliersAddEdit";
            this.gridProductSuppliersAddEdit.ReadOnly = true;
            this.gridProductSuppliersAddEdit.Size = new System.Drawing.Size(690, 200);
            this.gridProductSuppliersAddEdit.TabIndex = 17;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(627, 63);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 16;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // rbtnUpdate
            // 
            this.rbtnUpdate.AutoSize = true;
            this.rbtnUpdate.Location = new System.Drawing.Point(443, 6);
            this.rbtnUpdate.Name = "rbtnUpdate";
            this.rbtnUpdate.Size = new System.Drawing.Size(60, 17);
            this.rbtnUpdate.TabIndex = 15;
            this.rbtnUpdate.TabStop = true;
            this.rbtnUpdate.Text = "Update";
            this.rbtnUpdate.UseVisualStyleBackColor = true;
            this.rbtnUpdate.CheckedChanged += new System.EventHandler(this.rbtnUpdate_CheckedChanged);
            // 
            // rbtnRemove
            // 
            this.rbtnRemove.AutoSize = true;
            this.rbtnRemove.Location = new System.Drawing.Point(319, 6);
            this.rbtnRemove.Name = "rbtnRemove";
            this.rbtnRemove.Size = new System.Drawing.Size(65, 17);
            this.rbtnRemove.TabIndex = 14;
            this.rbtnRemove.TabStop = true;
            this.rbtnRemove.Text = "Remove";
            this.rbtnRemove.UseVisualStyleBackColor = true;
            this.rbtnRemove.CheckedChanged += new System.EventHandler(this.rbtnRemove_CheckedChanged);
            // 
            // rbtnAdd
            // 
            this.rbtnAdd.AutoSize = true;
            this.rbtnAdd.Location = new System.Drawing.Point(170, 6);
            this.rbtnAdd.Name = "rbtnAdd";
            this.rbtnAdd.Size = new System.Drawing.Size(44, 17);
            this.rbtnAdd.TabIndex = 13;
            this.rbtnAdd.TabStop = true;
            this.rbtnAdd.Text = "Add";
            this.rbtnAdd.UseVisualStyleBackColor = true;
            this.rbtnAdd.CheckedChanged += new System.EventHandler(this.rbtnAdd_CheckedChanged);
            // 
            // lblProductSupplier
            // 
            this.lblProductSupplier.AutoSize = true;
            this.lblProductSupplier.Location = new System.Drawing.Point(30, 70);
            this.lblProductSupplier.Name = "lblProductSupplier";
            this.lblProductSupplier.Size = new System.Drawing.Size(102, 13);
            this.lblProductSupplier.TabIndex = 12;
            this.lblProductSupplier.Text = "Product Supplier ID:";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(627, 63);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 10;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // cboProductSupplierDelete
            // 
            this.cboProductSupplierDelete.FormattingEnabled = true;
            this.cboProductSupplierDelete.Location = new System.Drawing.Point(138, 65);
            this.cboProductSupplierDelete.Name = "cboProductSupplierDelete";
            this.cboProductSupplierDelete.Size = new System.Drawing.Size(75, 21);
            this.cboProductSupplierDelete.TabIndex = 9;
            this.cboProductSupplierDelete.SelectedIndexChanged += new System.EventHandler(this.cboProductSupplierDelete_SelectedIndexChanged);
            // 
            // lblSupplier
            // 
            this.lblSupplier.AutoSize = true;
            this.lblSupplier.Location = new System.Drawing.Point(408, 70);
            this.lblSupplier.Name = "lblSupplier";
            this.lblSupplier.Size = new System.Drawing.Size(48, 13);
            this.lblSupplier.TabIndex = 8;
            this.lblSupplier.Text = "Supplier:";
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.Location = new System.Drawing.Point(234, 70);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(47, 13);
            this.lblProduct.TabIndex = 7;
            this.lblProduct.Text = "Product:";
            // 
            // lblInstruction
            // 
            this.lblInstruction.AutoSize = true;
            this.lblInstruction.Location = new System.Drawing.Point(30, 27);
            this.lblInstruction.Name = "lblInstruction";
            this.lblInstruction.Size = new System.Drawing.Size(296, 13);
            this.lblInstruction.TabIndex = 6;
            this.lblInstruction.Text = "Select a Product and Supplier to add on Product Supplier List";
            // 
            // cboProducts
            // 
            this.cboProducts.FormattingEnabled = true;
            this.cboProducts.Location = new System.Drawing.Point(287, 65);
            this.cboProducts.Name = "cboProducts";
            this.cboProducts.Size = new System.Drawing.Size(101, 21);
            this.cboProducts.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 405);
            this.Controls.Add(this.tabProductSupplier);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabProductSupplier.ResumeLayout(false);
            this.tabDisplay.ResumeLayout(false);
            this.tabDisplay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductSupplier)).EndInit();
            this.tabAddRemove.ResumeLayout(false);
            this.tabAddRemove.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductSuppliersAddEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox cboSuppliers;
        private System.Windows.Forms.Button btnaddProductSupplier;
        private System.Windows.Forms.TabControl tabProductSupplier;
        private System.Windows.Forms.TabPage tabDisplay;
        private System.Windows.Forms.TabPage tabAddRemove;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.ComboBox cboSupplierNames;
        private System.Windows.Forms.ComboBox cboSupplierIDs;
        private System.Windows.Forms.ComboBox cboProductIDs;
        private System.Windows.Forms.ComboBox cboProductSupplierID;
        private System.Windows.Forms.Label lblSupplierName;
        private System.Windows.Forms.Label lblSupplierID;
        private System.Windows.Forms.Label lblProductID;
        private System.Windows.Forms.Label lblProductSupplierID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboProductNames;
        private System.Windows.Forms.Label lblSelected;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.ComboBox cboProducts;
        private System.Windows.Forms.Button btnDisplayAll;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.ComboBox cboProductSupplierDelete;
        private System.Windows.Forms.Label lblSupplier;
        private System.Windows.Forms.Label lblProduct;
        private System.Windows.Forms.Label lblInstruction;
        private System.Windows.Forms.RadioButton rbtnUpdate;
        private System.Windows.Forms.RadioButton rbtnRemove;
        private System.Windows.Forms.RadioButton rbtnAdd;
        private System.Windows.Forms.Label lblProductSupplier;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGridView gridProductSuppliersAddEdit;
        private System.Windows.Forms.DataGridView gridProductSupplier;
        private System.Windows.Forms.Label lblEmpty;
        private System.Windows.Forms.RadioButton rbtnSuName;
        private System.Windows.Forms.RadioButton rbtnProdName;
        private System.Windows.Forms.RadioButton rbtnSupplierID;
        private System.Windows.Forms.RadioButton rbtnProductID;
        private System.Windows.Forms.RadioButton rbtnPrductSupplier;
    }
}

