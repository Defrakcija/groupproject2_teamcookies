﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductSupplier
{
    public partial class Form1 : Form
    {
        List<Products_Suppliers> productsuppliers = null;

        private Products_Suppliers productsupplier;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rbtnAdd.Checked = true;
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductsComboBox();
            LoadProductNamesComboBox();
            LoadProductSupplierIDRemove();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();
            gridProductSupplier.Refresh();
            lblSelected.Location= new Point(80, 88);
            DisplayAddRemoveTabList();
            rbtnPrductSupplier.Checked = true;
            gridProductSupplier.Columns[0].HeaderCell.Value = "Product Supplier ID";
            gridProductSupplier.Columns[1].HeaderCell.Value = "Product ID";
            gridProductSupplier.Columns[2].HeaderCell.Value = "Product Name";
            gridProductSupplier.Columns[3].HeaderCell.Value = "Supplier ID";
            gridProductSupplier.Columns[4].HeaderCell.Value = "Supplier Name";
            gridProductSuppliersAddEdit.Columns[0].HeaderCell.Value = "Product Supplier ID";
            gridProductSuppliersAddEdit.Columns[1].HeaderCell.Value = "Product ID";
            gridProductSuppliersAddEdit.Columns[2].HeaderCell.Value = "Product Name";
            gridProductSuppliersAddEdit.Columns[3].HeaderCell.Value = "Supplier ID";
            gridProductSuppliersAddEdit.Columns[4].HeaderCell.Value = "Supplier Name";
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
            gridProductSupplier.Columns[0].Width = 125;
            gridProductSupplier.Columns[4].Width = 200;
            gridProductSuppliersAddEdit.Columns[0].Width = 125;
            gridProductSuppliersAddEdit.Columns[4].Width = 200;
            lblEmpty.Text = "";

        }
        private void LoadProductsComboBox()
        {
            List<Products> products = new List<Products>();
            try
            {
                products = ProductsDB.GetProducts();
                cboProducts.DataSource = products;
                cboProducts.DisplayMember = "ProdName";
                cboProducts.ValueMember = "ProductID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        private void LoadProductIDsComboBox()
        {
            List<Products> productIDs = new List<Products>();
            try
            {
                productIDs = ProductsDB.GetProducts();
                cboProductIDs.DataSource = productIDs;
                cboProductIDs.DisplayMember = "ProductID";
                cboProductIDs.ValueMember = "ProdName";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        private void LoadProductNamesComboBox()
        {
            List<Products> products = new List<Products>();
            try
            {
                products = ProductsDB.GetProducts();
                cboProductNames.DataSource = products;
                cboProductNames.DisplayMember = "ProdName";
                cboProductNames.ValueMember = "ProductID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        private void LoadSuppliersComboBox()
        {
            List<Suppliers> suppliers = new List<Suppliers>();
            try
            {
                suppliers = SuppliersDB.GetSuppliers();
                cboSuppliers.DataSource = suppliers;
                cboSuppliers.DisplayMember = "SupName";
                cboSuppliers.ValueMember = "SupplierID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        private void LoadSupplierNamesComboBox()
        {
            List<Suppliers> supplierNames = new List<Suppliers>();
            try
            {
                supplierNames = SuppliersDB.GetSuppliers();
                cboSupplierNames.DataSource = supplierNames;
                cboSupplierNames.DisplayMember = "SupName";
                cboSupplierNames.ValueMember = "SupplierID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        private void LoadSupplierIDsComboBox()
        {
            List<Suppliers> supplierIDs = new List<Suppliers>();
            try
            {
                supplierIDs = SuppliersDB.GetSuppliers();
                cboSupplierIDs.DataSource = supplierIDs;
                cboSupplierIDs.DisplayMember = "SupplierID";
                cboSupplierIDs.ValueMember = "SupName";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        private void LoadProductSupplierIDsComboBox()
        {
            List<Products_Suppliers> productsupplierIDs = new List<Products_Suppliers>();
            try
            {
                productsupplierIDs = Products_SuppliersDB.GetProductSuppliers();
                cboProductSupplierID.DataSource = productsupplierIDs;
                cboProductSupplierID.DisplayMember = "productsupplierId";
                cboProductSupplierID.ValueMember = "productsupplierId";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
        private void LoadProductSupplierIDRemove()
        {
            List<Products_Suppliers> productsupplierIDs = new List<Products_Suppliers>();
            try
            {
                productsupplierIDs = Products_SuppliersDB.GetProductSuppliers();
                cboProductSupplierDelete.DataSource = productsupplierIDs;
                cboProductSupplierDelete.DisplayMember = "productsupplierId";
                cboProductSupplierDelete.ValueMember = "productsupplierId";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }


        private void DisplayProductSupplierListbyProdID(int selectedprodID)
        {
            lblEmpty.Text = "";
            gridProductSupplier.Refresh();
            productsuppliers = Products_SuppliersDB.GetProdsupplierIDandSuppliersbyProdID(selectedprodID);
            if (productsuppliers.Count == 0)
            {
                lblEmpty.Text = "Please assign a Supplier for this Product";
                var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
                var source = new BindingSource(bindingList, null);
                gridProductSupplier.DataSource = source;
            }
            else
            {
                var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
                var source = new BindingSource(bindingList, null);
                gridProductSupplier.DataSource = source;
                
            }


        }
        private void DisplayProductSupplierListbySupID(int selectedsupID)
        {
            lblEmpty.Text = "";
            gridProductSupplier.Refresh();
            productsuppliers = Products_SuppliersDB.GetProdsupplierIDandSuppliersbySupID(selectedsupID);
            if (productsuppliers.Count ==0)
            {
                lblEmpty.Text = "Please assign a product for this supplier";
                gridProductSupplier.Refresh();
                var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
                var source = new BindingSource(bindingList, null);
                gridProductSupplier.DataSource = source;
            }
            else
            {
                gridProductSupplier.Refresh();
                var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
                var source = new BindingSource(bindingList, null);
                gridProductSupplier.DataSource = source;
                
            }
        }
        private void DisplayProductSupplierListbyProdsupID(int selectedprodsupID)
        {
            gridProductSupplier.Refresh();
            productsuppliers = Products_SuppliersDB.GetProductSuppliersbyProdSupID(selectedprodsupID);
            var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
            var source = new BindingSource(bindingList, null);
            gridProductSupplier.DataSource = source;

        }
        private void DisplayAddRemoveTabList()
        {

            productsuppliers = Products_SuppliersDB.GetProductSuppliers();
            var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
            var source = new BindingSource(bindingList, null);
            gridProductSuppliersAddEdit.DataSource = source;
        }

        private void DisplayRemoveupdate(int selectedprodsupID)
        {
            gridProductSuppliersAddEdit.Refresh();
            productsuppliers = Products_SuppliersDB.GetProductSuppliersbyProdSupID(selectedprodsupID);
            var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
            var source = new BindingSource(bindingList, null);
            gridProductSuppliersAddEdit.DataSource = source;
        }
        private void cboProductID_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Products selectedProd = (Products)cboProductIDs.SelectedItem;
            int selectedprodID = Convert.ToInt32(selectedProd.ProductId);
            lblSelected.Text = "Product ID " + selectedprodID.ToString() + " (Product Name = " + selectedProd.ProdName + ")";
            DisplayProductSupplierListbyProdID(selectedprodID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = false;
            gridProductSupplier.Columns[2].Visible = false;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }

        private void cboProductNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Products selectedProd = (Products)cboProductNames.SelectedItem;
            int selectedprodID = Convert.ToInt32(selectedProd.ProductId);
            lblSelected.Text = "Product Name: " + cboProductNames.Text + " (ID = " + selectedprodID.ToString() + ")";
            DisplayProductSupplierListbyProdID(selectedprodID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = false;
            gridProductSupplier.Columns[2].Visible = false;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }

        private void cboSupplierIDs_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Suppliers selectedSup = (Suppliers)cboSupplierIDs.SelectedItem;
            int selectedsupID = Convert.ToInt32(selectedSup.SupplierId);
            lblSelected.Text = "Supplier ID " + selectedsupID.ToString() + " (Supplier Name= " + selectedSup.SupName + ")";
            DisplayProductSupplierListbySupID(selectedsupID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = false;
            gridProductSupplier.Columns[4].Visible = false;
        }

        private void cboSupplierNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Suppliers selectedSup = (Suppliers)cboSupplierNames.SelectedItem;
            int selectedsupID = Convert.ToInt32(selectedSup.SupplierId);
            lblSelected.Text = "Supplier Name: " + cboSupplierNames.Text + " (ID = " + selectedsupID.ToString() + ")";
            DisplayProductSupplierListbySupID(selectedsupID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = false;
            gridProductSupplier.Columns[4].Visible = false;
        }

        private void cboProductSupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();

            Products_Suppliers selectedProdSup = (Products_Suppliers)cboProductSupplierID.SelectedItem;
            int selectedprodsupID = Convert.ToInt32(selectedProdSup.ProductSupplierId);
            lblSelected.Text = "Product Supplier ID " + selectedprodsupID.ToString();
            DisplayProductSupplierListbyProdsupID(selectedprodsupID);
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }

        private void btnDisplayAll_Click(object sender, EventArgs e)
        {
            gridProductSupplier.Refresh();
            productsuppliers = Products_SuppliersDB.GetProductSuppliers();
            var bindingList = new BindingList<Products_Suppliers>(productsuppliers);
            var source = new BindingSource(bindingList, null);
            gridProductSupplier.DataSource = source;
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }
        private void addProductSupplier_Click(object sender, EventArgs e)
        {
            gridProductSuppliersAddEdit.Refresh();
            productsupplier = new Products_Suppliers();
            PutProductSupplierData(productsupplier);
            try
            {
                productsupplier.ProductSupplierId = Products_SuppliersDB.AddProductSupplier(productsupplier);
                MessageBox.Show("New Product Supplier has been added");
                DisplayAddRemoveTabList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductNamesComboBox();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();

        }
        private void PutProductSupplierData(Products_Suppliers productsupplier)
        {

            if (btnUpdate.Capture == true)
            {
                Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
                int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
                productsupplier.ProductSupplierId = selectedprodSupID;
                Products selectedProd = (Products)cboProducts.SelectedItem;
                int selectedprodID = Convert.ToInt32(selectedProd.ProductId);
                productsupplier.ProductId = selectedprodID;
                Suppliers selectedSup = (Suppliers)cboSuppliers.SelectedItem;
                int selectedSupId = Convert.ToInt32(selectedSup.SupplierId);
                productsupplier.SupplierId = selectedSupId;
            }
            else
            {
                Products selectedProd = (Products)cboProducts.SelectedItem;
                int selectedprodID = Convert.ToInt32(selectedProd.ProductId);
                productsupplier.ProductId = selectedprodID;
                Suppliers selectedSup = (Suppliers)cboSuppliers.SelectedItem;
                int selectedSupId = Convert.ToInt32(selectedSup.SupplierId);
                productsupplier.SupplierId = selectedSupId;
            }


        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            gridProductSupplier.Rows.Clear();
            gridProductSupplier.Refresh();
            lblSelected.Text = "";
            gridProductSupplier.Columns[0].Visible = true;
            gridProductSupplier.Columns[1].Visible = true;
            gridProductSupplier.Columns[2].Visible = true;
            gridProductSupplier.Columns[3].Visible = true;
            gridProductSupplier.Columns[4].Visible = true;
        }


        private void btnRemove_Click(object sender, EventArgs e)
        {
            Products_Suppliers selectedProdsup = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodsupID = Convert.ToInt32(selectedProdsup.ProductSupplierId);
            productsupplier = Products_SuppliersDB.GetProducsuppliersObject(selectedprodsupID);
            DialogResult result = MessageBox.Show("Delete Product Supplier ID "+ selectedprodsupID.ToString() + "?",
                            "Confirm Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                try
                {
                    if (!Products_SuppliersDB.RemoveProductSupplier(productsupplier))
                    {
                        MessageBox.Show("Another User may have Updated or deleted that product supplier or " +
                                    "It is being used in another Table, CANNOT REMOVE", "Database Error");
                    }
                    else
                    {
                        MessageBox.Show("Product Supplier ID: " + selectedprodsupID + " has been deleted");
                        LoadProductSupplierIDRemove();
                        DisplayAddRemoveTabList();


                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductsComboBox();
            LoadProductNamesComboBox();
            LoadProductSupplierIDRemove();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();

        }

        private void rbtnAdd_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            btnRemove.Visible = false;
            cboProductSupplierDelete.Visible = false;
            lblProductSupplier.Visible = false;
            btnUpdate.Visible = false;
            btnaddProductSupplier.Visible = true;
            lblProduct.Visible = true;
            lblSupplier.Visible = true;
            cboProducts.Visible = true;
            cboSuppliers.Visible = true;
            DisplayAddRemoveTabList();
            lblInstruction.Text = "Instruction: Select a product and a supplier "+
                "to Add new Product supplier.\nNew Supplier will be added at the end of the list.";
        }

        private void rbtnRemove_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            btnRemove.Visible = true;
            cboProductSupplierDelete.Visible = true;
            lblProductSupplier.Visible = true;
            btnUpdate.Visible = false;
            btnaddProductSupplier.Visible = false;
            lblProduct.Visible = false;
            lblSupplier.Visible = false;
            cboProducts.Visible = false;
            cboSuppliers.Visible = false;
            Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
            DisplayRemoveupdate(selectedprodSupID);
            lblInstruction.Text = "Instruction: Select a the Product Supplier ID you want to remove.";
        }

        private void rbtnUpdate_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            btnRemove.Visible = false;
            cboProductSupplierDelete.Visible = true;
            lblProductSupplier.Visible = true;
            btnUpdate.Visible = true;
            btnaddProductSupplier.Visible = false;
            lblProduct.Visible = true;
            lblSupplier.Visible = true;
            cboProducts.Visible = true;
            cboSuppliers.Visible = true;
            Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
            DisplayRemoveupdate(selectedprodSupID);
            lblInstruction.Text = "Instruction: Select a the Product Supplier ID you want to Update then choose a product and a supplier for this ID.";
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            gridProductSuppliersAddEdit.Refresh();
            Products_Suppliers newproductsupplier = new Products_Suppliers();
            Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
            Products_Suppliers productsupplier = Products_SuppliersDB.GetProducsuppliersObject(selectedprodSupID);
            PutProductSupplierData(newproductsupplier);
            DialogResult result = MessageBox.Show("Update Product Supplier?\nWarning It will Update all product suppliers in other table",
                            "Confirm Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                try
                {
                    if (!Products_SuppliersDB.UpdateProductSupplier(productsupplier, newproductsupplier))
                    {
                        MessageBox.Show("Another user has updated or " +
                                    "deleted that Product Supplier.", "Database Error");
                    }
                    else
                    {
                        MessageBox.Show("Product supplier ID: " + selectedprodSupID + " has been edited");
                        DisplayAddRemoveTabList();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            LoadProductSupplierIDRemove();
            LoadProductsComboBox();
            LoadSuppliersComboBox();
            LoadSuppliersComboBox();
            LoadProductIDsComboBox();
            LoadProductsComboBox();
            LoadProductNamesComboBox();
            LoadProductSupplierIDRemove();
            LoadSupplierIDsComboBox();
            LoadSupplierNamesComboBox();
            LoadProductSupplierIDsComboBox();
        }

        private void cboProductSupplierDelete_SelectedIndexChanged(object sender, EventArgs e)
        {
            Products_Suppliers selectedProdsupplier = (Products_Suppliers)cboProductSupplierDelete.SelectedItem;
            int selectedprodSupID = Convert.ToInt32(selectedProdsupplier.ProductSupplierId);
            DisplayRemoveupdate(selectedprodSupID);
        }


        private void rbtnProductID_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductIDsComboBox();
            cboProductSupplierID.Visible = false;
            cboProductIDs.Visible = true;
            cboProductNames.Visible = false;
            cboSupplierIDs.Visible = false;
            cboSupplierNames.Visible = false;
            lblProductSupplierID.Visible = false;
            lblProductID.Visible = true;
            lblProductName.Visible = false;
            lblSupplierID.Visible = false;
            lblSupplierName.Visible = false;

        }

        private void rbtnProdName_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductNamesComboBox();
            cboProductSupplierID.Visible = false;
            cboProductIDs.Visible = false;
            cboProductNames.Visible = true;
            cboSupplierIDs.Visible = false;
            cboSupplierNames.Visible = false;
            lblProductSupplierID.Visible = false;
            lblProductID.Visible = false;
            lblProductName.Visible = true;
            lblSupplierID.Visible = false;
            lblSupplierName.Visible = false;
        }

        private void rbtnSupplierID_CheckedChanged(object sender, EventArgs e)
        {
            LoadSupplierIDsComboBox();
            cboProductSupplierID.Visible = false;
            cboProductIDs.Visible = false;
            cboProductNames.Visible = false;
            cboSupplierIDs.Visible = true;
            cboSupplierNames.Visible = false;
            lblProductSupplierID.Visible = false;
            lblProductID.Visible = false;
            lblProductName.Visible = false;
            lblSupplierID.Visible = true;
            lblSupplierName.Visible = false;
        }

        private void rbtnSuName_CheckedChanged(object sender, EventArgs e)
        {
            LoadSupplierNamesComboBox();
            cboProductSupplierID.Visible = false;
            cboProductIDs.Visible = false;
            cboProductNames.Visible = false;
            cboSupplierIDs.Visible = false;
            cboSupplierNames.Visible = true;
            lblProductSupplierID.Visible = false;
            lblProductID.Visible = false;
            lblProductName.Visible = false;
            lblSupplierID.Visible = false;
            lblSupplierName.Visible = true;
        }

        private void rbtnPrductSupplier_CheckedChanged(object sender, EventArgs e)
        {
            LoadProductSupplierIDsComboBox();
            cboProductSupplierID.Visible = true;
            cboProductIDs.Visible = false;
            cboProductNames.Visible = false;
            cboSupplierIDs.Visible = false;
            cboSupplierNames.Visible = false;
            lblProductSupplierID.Visible = true;
            lblProductID.Visible = false;
            lblProductName.Visible = false;
            lblSupplierID.Visible = false;
            lblSupplierName.Visible = false;
        }
    }
}
