﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductSupplier
{
    public static class ProductsDB
    {
        public static List <Products> GetProducts()
        {
            List<Products> products = new List<Products>();
            Products p;
            SqlConnection connection = TravelExpertsDB.GetConnection();
            string selectQuery = "SELECT ProductID, ProdName FROM Products";
            SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    p = new Products();
                    p.ProductId = (int)reader["ProductID"];
                    p.ProdName = reader["ProdName"].ToString();
                    products.Add(p);
                }
            }

            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return products;
        }

    }
}
