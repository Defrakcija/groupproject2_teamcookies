﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsGroupProject
{
    public static class ProductsDB
    {
        public static List<string> GetProducts()
        {
            //list for the IDs
            List<string> prodname = new List<string>();
            string nextProd;
            //connect to the database
            SqlConnection connection = TravelExpertsDB.GetConnection();
            //select query to retrive IDs from orers
            string selectQuery = "SELECT prodName FROM Products";
            SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
            try
            {

                connection.Open();

                SqlDataReader reader = selectCommand.ExecuteReader();
                //read IDs from the sql reader
                while (reader.Read())
                {

                    nextProd = reader["prodName"].ToString();
                    prodname.Add(nextProd);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return prodname;
        }

        
        public static Product GetProduct(string prodName)
        {
            Product prod = null;
            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectQuery = "SELECT ProductID , ProdName " +
                                 "FROM Products " +
                                 "WHERE ProdName = @ProdName";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@ProdName", prodName);
            try
            {
                con.Open(); // open connection
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read()) // read the customer if exists
                {
                    prod = new Product(); // create new customer object
                    prod.ProductId = (int)reader["ProductID"];
                    prod.ProdName = reader["ProdName"].ToString();
                    
                    
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close(); // close connection
            }
            return prod;
        }

        // add a new customer record to the table and return generated ID
        public static int AddProduct(Product prod)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string insertStatement = "INSERT INTO products (ProdName) VALUES (@ProdName) ";


            SqlCommand insertCommand = new SqlCommand(insertStatement, con);
            insertCommand.Parameters.AddWithValue("@ProdName", prod.ProdName);
            try
            {
                con.Open();
                insertCommand.ExecuteNonQuery(); // for DML statements
                string selectQuery = "SELECT IDENT_CURRENT('Customers') FROM CUSTOMERS"; // get the generated ID
                SqlCommand selectCommand = new SqlCommand(selectQuery, con);
                int prodID = Convert.ToInt32(selectCommand.ExecuteScalar()); // retrieves one value
                                                                             // (int) does not work
                return prodID;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        // delete customer, ensuring optimistic concurrency
        public static bool DeleteProduct(Product prod)
        {


            SqlConnection con = TravelExpertsDB.GetConnection();

            string deleteStatement = "DELETE FROM Products " +
                                     " WHERE ProductId = @ProductID " + // to identify record
                                     " AND ProdName = @ProdName " ;
            SqlCommand deleteCommand = new SqlCommand(deleteStatement, con);
            deleteCommand.Parameters.AddWithValue("@ProductId", prod.ProductId);

            deleteCommand.Parameters.AddWithValue("@ProdName", prod.ProdName);
            
            try
            {
                con.Open();
                //fk.ExecuteNonQuery();
                int count = deleteCommand.ExecuteNonQuery(); // returns number of rows deleted
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public static bool UpdateProduct(Product oldProduct, Product newProduct)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string updateStatement = "UPDATE Products SET ProdName = @NewProdName  " +
                                     " WHERE ProductID = @OldProductID ";
            SqlCommand updateCommand = new SqlCommand(updateStatement, con);

            updateCommand.Parameters.AddWithValue("@NewProdName", newProduct.ProdName);
          

            updateCommand.Parameters.AddWithValue("@OldProductID", oldProduct.ProductId);
            updateCommand.Parameters.AddWithValue("@OldProdName", oldProduct.ProdName);
            try
            {
                con.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
