﻿using CustomerMaintenance;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductsGroupProject
{
    public partial class frmproduct : Form
    {
        List<string> prodname;
        Product prod;
        Product newprod;
        public frmproduct()
        {
            InitializeComponent();
        }



        private void frmproduct_Load(object sender, EventArgs e)
        {
            grpUpdate.Visible = false;
            this.DisplayProduct();
        }

        private void cbProductsName_SelectedIndexChanged(object sender, EventArgs e)
        {
            // convert the selected value to in so we can use it to retrieve order information and details
            string productname = cbProductsName.SelectedItem.ToString();
            //get order information for this ID 
            prod = ProductsDB.GetProduct(productname);
            cbProductsName.SelectedIndexChanged -= cbProductsName_SelectedIndexChanged; // unregister handler
            //set the retrieved values in text box
            txtProductID.Text = prod.ProductId.ToString();
            cbProductsName.SelectedIndexChanged += cbProductsName_SelectedIndexChanged; // register handler   
            txtModify.Text = "";       
           // txtModify.Text = cbProductsName.SelectedItem.ToString();
            
        }

        private void GetProduct(string productname)
        {
            try
            {
                prod = ProductsDB.GetProduct(productname.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void ClearControls()
        {
            txtProductID.Text = "";
            txtModify.Text = "";
           // cbProductsName.SelectedIndex = -1;
            //btnModify.Enabled = false;
            //btnDelete.Enabled = false;
            //cbProductsName.Select();
        }

        private void DisplayProduct()
        {
            prodname = ProductsDB.GetProducts();
            cbProductsName.DataSource = prodname;
            btnModify.Enabled = true;
            btnDelete.Enabled = true;
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Delete " + prod.ProdName + "?",
                 "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                try
                {
                    if (!ProductsDB.DeleteProduct(prod))
                    {
                        MessageBox.Show("Another user has updated or deleted " +
                            "that customer.", "Database Error");
                        this.GetProduct(prod.ProdName);
                        if (prod != null)
                            this.DisplayProduct();
                        else
                            this.ClearControls();
                    }
                    else
                    {
                        this.ClearControls();
                        this.DisplayProduct();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("This product is used by a package , Delete Unsuccessful ");
                }
            }
        }

        //private bool btnAddClicked = false;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validator.IsPresent(txtModify))
            {
                newprod = new Product();
                this.PutProductData(newprod);
                try
                {
                    newprod.ProdName = ProductsDB.AddProduct(newprod).ToString();
                    this.DialogResult = DialogResult.OK;
                    this.DisplayProduct();
                    MessageBox.Show("Product " + txtModify.Text + " was added successfully !!");
                    txtModify.Text = "";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }

                this.DisplayProduct();
            }
            else
                MessageBox.Show("Please enter a valid Name");
            
        }

        //private bool btnModifyClicked = false;
        private void btnModify_Click(object sender, EventArgs e)
        {
            grpUpdate.Visible = true;
            txtModify.Select();
            //txtAdd.Visible = false;
            //btnModifyClicked = true;
            ////txtModify.Text = prod.ProdName;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Product newProduct = new Product();
            
            //this.PutProductData(newProduct);
            newProduct.ProductId = prod.ProductId;
            newProduct.ProdName = txtModify.Text.ToString();
            try
            {
                if (!ProductsDB.UpdateProduct(prod, newProduct))
                {
                    MessageBox.Show("Another user has updated or " +
                        "deleted that customer.", "Database Error");
                    this.DialogResult = DialogResult.Retry;
                }
                else
                {
                    prod = newProduct;
                    this.DialogResult = DialogResult.OK;
                    this.DisplayProduct();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        private void PutProductData(Product prodz)
        {
            prodz.ProdName = txtModify.Text.ToString();
        }
    }
}
