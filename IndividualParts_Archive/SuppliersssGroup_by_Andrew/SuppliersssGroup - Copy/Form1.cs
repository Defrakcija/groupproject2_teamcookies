﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuppliersssGroup
{
    public partial class Form1 : Form
    {

        List<string> supname;
        Suppliers sup;
        Suppliers newsup;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.DisplaySupplier();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtSupID_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void GetSupplier(string suppliername)
        {
            try
            {
                sup = SuppliersDB.GetSupplier(suppliername.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        private void amSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            // convert the selected value to in so we can use it to retrieve order information and details
            string suppliername = amSupplierName.SelectedItem.ToString();
            //get order information for this ID 
            sup = SuppliersDB.GetSupplier(suppliername);
            amSupplierName.SelectedIndexChanged -= amSupplierName_SelectedIndexChanged; // unregister handler
            //set the retrieved values in text box
            txtSupID.Text = sup.SupplierId.ToString();
            amSupplierName.SelectedIndexChanged += amSupplierName_SelectedIndexChanged; // register handler   
            txtModify.Text = "";
            // txtModify.Text = amProductsName.SelectedItem.ToString();

        }
        private void DisplaySupplier()
        {
            supname = SuppliersDB.GetSuppliers();
            amSupplierName.DataSource = supname;
            btnModify.Enabled = true;

        }
        private void ClearControls()
        {

            txtModify.Text = "";
            // cbProductsName.SelectedIndex = -1;
            //btnModify.Enabled = false;
            //btnDelete.Enabled = false;
            //cbProductsName.Select();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (RbAdd.Checked)

            {
                if (txtModify != null)
                {
                    newsup = new Suppliers();
                    this.PutSupplierData(newsup);
                    try
                    {
                        newsup.SupName = SuppliersDB.AddSupplier(newsup).ToString();
                        newsup.SupplierId = SuppliersDB.AddSupplier(newsup);

                        this.DialogResult = DialogResult.OK;
                        this.DisplaySupplier();
                        MessageBox.Show("Supplier " + txtModify.Text + " was added successfully !!");
                        txtModify.Text = "";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }

                    this.DisplaySupplier();
                }
                else
                    MessageBox.Show("Please enter a valid Name");

            }
            else if (RbDelete.Checked)
            {
                DialogResult result = MessageBox.Show("Delete " + sup.SupName + "?",
                "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        if (!SuppliersDB.DeleteSupplier(sup))
                        {
                            MessageBox.Show("Another user has updated or deleted " +
                                "that supplier.", "Database Error");

                            this.GetSupplier(sup.SupName);
                            if (sup != null)
                                this.DisplaySupplier();
                            else
                                this.ClearControls();
                        }
                        else
                        {
                            this.ClearControls();
                            this.DisplaySupplier();
                        }
                    }
                    catch (Exception)
                    {
                        //MessageBox.Show(ex.Message, ex.GetType().ToString());
                        MessageBox.Show("It's not possible to delete supplier " + sup.SupName + ". It's being used by a package ");
                    }
                }
            }
            else if (RbUpdate.Checked)
            {
                Suppliers newSupplier = new Suppliers();

                //this.PutProductData(newProduct);
                newSupplier.SupplierId = sup.SupplierId;
                newSupplier.SupName = txtModify.Text.ToString();
                try
                {
                    if (!SuppliersDB.UpdateSupplier(sup, newSupplier))
                    {
                        MessageBox.Show("Another user has updated or " +
                            "deleted that supplie.", "Database Error");
                        this.DialogResult = DialogResult.Retry;
                    }
                    else
                    {
                        sup = newSupplier;
                        this.DialogResult = DialogResult.OK;
                        this.DisplaySupplier();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }
        private void PutSupplierData(Suppliers sups)
        {
            sups.SupName = txtModify.Text.ToString();
        }

        private void RbAdd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtModify_TextChanged(object sender, EventArgs e)
        {

        }
    }
    }

