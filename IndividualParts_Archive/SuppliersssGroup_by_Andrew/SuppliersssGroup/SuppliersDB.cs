﻿// Author - Andrew Murphy
// Project Workshop 2





using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuppliersssGroup

{
    public static class SuppliersDB
    {
        public static List<string> GetSuppliers()
        {
            List<string> supname = new List<string>();
            string nextSup;
            //connecting to the database
            SqlConnection connection = TravelExpertsDB.GetConnection();
            string selectQuery = "SELECT SupName FROM Suppliers order by SupName";
            SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    nextSup = reader["SupName"].ToString();
                    supname.Add(nextSup);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return supname;
        }

        public static Suppliers GetSupplier(string suppName)
        {
            Suppliers sup = null;
            SqlConnection connection = TravelExpertsDB.GetConnection();
            string selectQuery = "SELECT SupplierId, SupName " +
                                 "FROM Suppliers " +
                                 "WHERE SupName = @SupName";

            SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
            selectCommand.Parameters.AddWithValue("@SupName", suppName);
            try
            {
                connection.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    sup = new Suppliers();
                    sup.SupplierId = (int)reader["SupplierID"];
                    sup.SupName = reader["SupName"].ToString();
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return sup;
        }


        // add a new customer record to the table and return generated ID
        public static int AddSupplier(Suppliers sup)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string insertStatement = "INSERT INTO Suppliers (SupName) VALUES (@SupName) ";


            SqlCommand insertCommand = new SqlCommand(insertStatement, con);
            if (sup.SupName == null)
            {
                insertCommand.Parameters.AddWithValue("@SupName", DBNull.Value);
            }
            else
                insertCommand.Parameters.AddWithValue("@SupName", sup.SupName);

            //insertCommand.Parameters.AddWithValue("@SupplierId", sup.SupplierId);

            try
            {
                con.Open();
                insertCommand.ExecuteNonQuery(); // for DML statements
                string selectQuery = "SELECT IDENT_CURRENT('Suppliers') FROM Suppliers"; // get the generated ID
                SqlCommand selectCommand = new SqlCommand(selectQuery, con);
                int supId = Convert.ToInt32(selectCommand.ExecuteScalar()); // retrieves one value
                                                                            // (int) does not work
                return supId;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        // delete customer
        public static bool DeleteSupplier(Suppliers sup)
        {


            SqlConnection con = TravelExpertsDB.GetConnection();

            string deleteStatement = "DELETE FROM Suppliers " +
                                 " WHERE SuppplierId = @SupplierId " + // to identify record
                                 " AND SupName = @SupName ";
            //string deleteStatement = "DELETE FROM Products " +
            //                     " WHERE @ProductID not in ( select ProductId from Products_Suppliers )";
            SqlCommand deleteCommand = new SqlCommand(deleteStatement, con);
            deleteCommand.Parameters.AddWithValue("@SupplierID", sup.SupplierId);

            deleteCommand.Parameters.AddWithValue("@SupName", sup.SupName);

            try
            {
                con.Open();
                //fk.ExecuteNonQuery();
                int count = deleteCommand.ExecuteNonQuery(); // returns number of rows deleted
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public static bool UpdateSupplier(Suppliers oldSupplier, Suppliers newSupplier)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string updateStatement = "UPDATE Suppliers SET SupName = @NewSupName  " +
                                     " WHERE SupplierId = @OldSupplierId ";
            SqlCommand updateCommand = new SqlCommand(updateStatement, con);

            updateCommand.Parameters.AddWithValue("@NewSupName", newSupplier.SupName);


            updateCommand.Parameters.AddWithValue("@OldSupplierId", oldSupplier.SupplierId);
            updateCommand.Parameters.AddWithValue("@OldSupName", oldSupplier.SupName);
            try
            {
                con.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
    }
}




