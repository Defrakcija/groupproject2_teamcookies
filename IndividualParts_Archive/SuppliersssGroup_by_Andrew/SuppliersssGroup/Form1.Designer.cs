﻿namespace SuppliersssGroup
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSupID = new System.Windows.Forms.TextBox();
            this.amSupplierName = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RbUpdate = new System.Windows.Forms.RadioButton();
            this.RbDelete = new System.Windows.Forms.RadioButton();
            this.RbAdd = new System.Windows.Forms.RadioButton();
            this.btnModify = new System.Windows.Forms.Button();
            this.txtModify = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSupID
            // 
            this.txtSupID.Location = new System.Drawing.Point(73, 51);
            this.txtSupID.Name = "txtSupID";
            this.txtSupID.Size = new System.Drawing.Size(100, 20);
            this.txtSupID.TabIndex = 0;
            this.txtSupID.TextChanged += new System.EventHandler(this.txtSupID_TextChanged);
            // 
            // amSupplierName
            // 
            this.amSupplierName.FormattingEnabled = true;
            this.amSupplierName.Location = new System.Drawing.Point(330, 50);
            this.amSupplierName.Name = "amSupplierName";
            this.amSupplierName.Size = new System.Drawing.Size(121, 21);
            this.amSupplierName.TabIndex = 1;
            this.amSupplierName.SelectedIndexChanged += new System.EventHandler(this.amSupplierName_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RbUpdate);
            this.groupBox1.Controls.Add(this.RbDelete);
            this.groupBox1.Controls.Add(this.RbAdd);
            this.groupBox1.Location = new System.Drawing.Point(61, 162);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 100);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Modify Options ";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // RbUpdate
            // 
            this.RbUpdate.AutoSize = true;
            this.RbUpdate.Location = new System.Drawing.Point(234, 42);
            this.RbUpdate.Name = "RbUpdate";
            this.RbUpdate.Size = new System.Drawing.Size(60, 17);
            this.RbUpdate.TabIndex = 2;
            this.RbUpdate.TabStop = true;
            this.RbUpdate.Text = "Update";
            this.RbUpdate.UseVisualStyleBackColor = true;
            // 
            // RbDelete
            // 
            this.RbDelete.AutoSize = true;
            this.RbDelete.Location = new System.Drawing.Point(122, 42);
            this.RbDelete.Name = "RbDelete";
            this.RbDelete.Size = new System.Drawing.Size(56, 17);
            this.RbDelete.TabIndex = 1;
            this.RbDelete.TabStop = true;
            this.RbDelete.Text = "Delete";
            this.RbDelete.UseVisualStyleBackColor = true;
            // 
            // RbAdd
            // 
            this.RbAdd.AutoSize = true;
            this.RbAdd.Location = new System.Drawing.Point(12, 42);
            this.RbAdd.Name = "RbAdd";
            this.RbAdd.Size = new System.Drawing.Size(44, 17);
            this.RbAdd.TabIndex = 0;
            this.RbAdd.TabStop = true;
            this.RbAdd.Text = "Add";
            this.RbAdd.UseVisualStyleBackColor = true;
            this.RbAdd.CheckedChanged += new System.EventHandler(this.RbAdd_CheckedChanged);
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(330, 99);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 3;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtModify
            // 
            this.txtModify.Location = new System.Drawing.Point(72, 102);
            this.txtModify.Name = "txtModify";
            this.txtModify.Size = new System.Drawing.Size(199, 20);
            this.txtModify.TabIndex = 4;
            this.txtModify.TextChanged += new System.EventHandler(this.txtModify_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "SupplierID";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(248, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Supplier Name";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 341);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtModify);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.amSupplierName);
            this.Controls.Add(this.txtSupID);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSupID;
        private System.Windows.Forms.ComboBox amSupplierName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RbDelete;
        private System.Windows.Forms.RadioButton RbAdd;
        private System.Windows.Forms.RadioButton RbUpdate;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.TextBox txtModify;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

