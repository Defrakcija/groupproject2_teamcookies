﻿CREATE TABLE Suppliers_Temp
    (
      SupplierId int NOT NULL IDENTITY(1, 1),
      SupName varchar(50) NULL
    )
ON  [PRIMARY]
go

SET IDENTITY_INSERT Suppliers_Temp ON
go

IF EXISTS ( SELECT  *
            FROM    dbo.Suppliers ) 
    INSERT  INTO dbo.Suppliers_Temp ( SupplierId, SupName )
            SELECT  SupplierId,
                    SupName
            FROM    dbo.Suppliers TABLOCKX
go

SET IDENTITY_INSERT dbo.Suppliers_Temp OFF
go

DROP TABLE dbo.Suppliers
go

Exec sp_rename 'Suppliers_Temp', 'Suppliers'