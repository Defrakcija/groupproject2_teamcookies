﻿CREATE TABLE Products_Images (
ImageID int  IDENTITY(1,1) PRIMARY KEY,
ProductID int FOREIGN KEY REFERENCES Products(ProductId),
ImageArray varbinary(max))