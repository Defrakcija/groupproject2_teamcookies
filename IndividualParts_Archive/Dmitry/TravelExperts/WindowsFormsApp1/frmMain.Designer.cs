﻿namespace TravelExperts
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tlpNav = new System.Windows.Forms.TableLayoutPanel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnNavProductSuppliers = new System.Windows.Forms.Button();
            this.btnPPS = new System.Windows.Forms.Button();
            this.btnNavProducts = new System.Windows.Forms.Button();
            this.lblMain = new System.Windows.Forms.Label();
            this.btnNavPackages = new System.Windows.Forms.Button();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPackages = new System.Windows.Forms.TabPage();
            this.tlpPackages = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPkgDisplayAll = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSort = new System.Windows.Forms.ComboBox();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.gridPackages = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPkgSave = new System.Windows.Forms.Button();
            this.btnPkgDelete = new System.Windows.Forms.Button();
            this.btnPkgEdit = new System.Windows.Forms.Button();
            this.btnPkgAdd = new System.Windows.Forms.Button();
            this.btnPkgCencel = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpPkgEnd = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPkgName = new System.Windows.Forms.TextBox();
            this.dtpPkgStart = new System.Windows.Forms.DateTimePicker();
            this.txtPkgID = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPkgDesc = new System.Windows.Forms.RichTextBox();
            this.txtPkgTotal = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPkgPrice = new System.Windows.Forms.TextBox();
            this.txtPkgCommision = new System.Windows.Forms.TextBox();
            this.flpProducts = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPkgProductSupplier = new System.Windows.Forms.Label();
            this.lblPkgProductName = new System.Windows.Forms.Label();
            this.lblPkgProductID = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnPkgDeleteProduct = new System.Windows.Forms.Button();
            this.btnPkgAddProduct = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabProducts = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.flpDisplayAllProducts = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDeleteProducts = new System.Windows.Forms.Button();
            this.btnEditProducts = new System.Windows.Forms.Button();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.tabPkgProSup = new System.Windows.Forms.TabPage();
            this.gridPPS = new System.Windows.Forms.DataGridView();
            this.ProdSupp = new System.Windows.Forms.TabPage();
            this.tlpNav.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabPackages.SuspendLayout();
            this.tlpPackages.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPackages)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tabProducts.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tabPkgProSup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPPS)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpNav
            // 
            this.tlpNav.BackColor = System.Drawing.Color.Teal;
            this.tlpNav.ColumnCount = 1;
            this.tlpNav.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpNav.Controls.Add(this.btnExit, 0, 6);
            this.tlpNav.Controls.Add(this.btnNavProductSuppliers, 0, 4);
            this.tlpNav.Controls.Add(this.btnPPS, 0, 3);
            this.tlpNav.Controls.Add(this.btnNavProducts, 0, 2);
            this.tlpNav.Controls.Add(this.lblMain, 0, 0);
            this.tlpNav.Controls.Add(this.btnNavPackages, 0, 1);
            this.tlpNav.Dock = System.Windows.Forms.DockStyle.Left;
            this.tlpNav.Location = new System.Drawing.Point(0, 0);
            this.tlpNav.Name = "tlpNav";
            this.tlpNav.RowCount = 7;
            this.tlpNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tlpNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tlpNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tlpNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tlpNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tlpNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tlpNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpNav.Size = new System.Drawing.Size(175, 624);
            this.tlpNav.TabIndex = 0;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Aqua;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Location = new System.Drawing.Point(3, 565);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(169, 56);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnNavProductSuppliers
            // 
            this.btnNavProductSuppliers.BackColor = System.Drawing.Color.Aqua;
            this.btnNavProductSuppliers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNavProductSuppliers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavProductSuppliers.Location = new System.Drawing.Point(3, 374);
            this.btnNavProductSuppliers.Name = "btnNavProductSuppliers";
            this.btnNavProductSuppliers.Size = new System.Drawing.Size(169, 56);
            this.btnNavProductSuppliers.TabIndex = 4;
            this.btnNavProductSuppliers.Text = "Product Suppliers";
            this.btnNavProductSuppliers.UseVisualStyleBackColor = false;
            this.btnNavProductSuppliers.Click += new System.EventHandler(this.btnNavProductSuppliers_Click);
            // 
            // btnPPS
            // 
            this.btnPPS.BackColor = System.Drawing.Color.Aqua;
            this.btnPPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPPS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPPS.Location = new System.Drawing.Point(3, 312);
            this.btnPPS.Name = "btnPPS";
            this.btnPPS.Size = new System.Drawing.Size(169, 56);
            this.btnPPS.TabIndex = 3;
            this.btnPPS.Text = "PPS";
            this.btnPPS.UseVisualStyleBackColor = false;
            this.btnPPS.Click += new System.EventHandler(this.btnPPS_Click);
            // 
            // btnNavProducts
            // 
            this.btnNavProducts.BackColor = System.Drawing.Color.Aqua;
            this.btnNavProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNavProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavProducts.Location = new System.Drawing.Point(3, 250);
            this.btnNavProducts.Name = "btnNavProducts";
            this.btnNavProducts.Size = new System.Drawing.Size(169, 56);
            this.btnNavProducts.TabIndex = 2;
            this.btnNavProducts.Text = "Products";
            this.btnNavProducts.UseVisualStyleBackColor = false;
            this.btnNavProducts.Click += new System.EventHandler(this.btnNavProducts_Click);
            // 
            // lblMain
            // 
            this.lblMain.AutoSize = true;
            this.lblMain.BackColor = System.Drawing.Color.Aqua;
            this.lblMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMain.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMain.Location = new System.Drawing.Point(3, 0);
            this.lblMain.Name = "lblMain";
            this.lblMain.Size = new System.Drawing.Size(169, 185);
            this.lblMain.TabIndex = 0;
            this.lblMain.Text = "Travel Experts";
            this.lblMain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNavPackages
            // 
            this.btnNavPackages.BackColor = System.Drawing.Color.Aqua;
            this.btnNavPackages.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNavPackages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNavPackages.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SlateGray;
            this.btnNavPackages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavPackages.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNavPackages.ImageKey = "(none)";
            this.btnNavPackages.Location = new System.Drawing.Point(3, 188);
            this.btnNavPackages.Name = "btnNavPackages";
            this.btnNavPackages.Size = new System.Drawing.Size(169, 56);
            this.btnNavPackages.TabIndex = 1;
            this.btnNavPackages.Text = "Travel Packages";
            this.btnNavPackages.UseVisualStyleBackColor = false;
            this.btnNavPackages.Click += new System.EventHandler(this.btnNavPackages_Click);
            // 
            // tabMain
            // 
            this.tabMain.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabMain.Controls.Add(this.tabPackages);
            this.tabMain.Controls.Add(this.tabProducts);
            this.tabMain.Controls.Add(this.tabPkgProSup);
            this.tabMain.Controls.Add(this.ProdSupp);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(175, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(876, 624);
            this.tabMain.TabIndex = 1;
            // 
            // tabPackages
            // 
            this.tabPackages.Controls.Add(this.tlpPackages);
            this.tabPackages.Location = new System.Drawing.Point(4, 31);
            this.tabPackages.Name = "tabPackages";
            this.tabPackages.Padding = new System.Windows.Forms.Padding(3);
            this.tabPackages.Size = new System.Drawing.Size(868, 589);
            this.tabPackages.TabIndex = 0;
            this.tabPackages.Text = "pck";
            this.tabPackages.UseVisualStyleBackColor = true;
            // 
            // tlpPackages
            // 
            this.tlpPackages.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tlpPackages.ColumnCount = 1;
            this.tlpPackages.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPackages.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tlpPackages.Controls.Add(this.gridPackages, 0, 1);
            this.tlpPackages.Controls.Add(this.tableLayoutPanel3, 0, 4);
            this.tlpPackages.Controls.Add(this.tableLayoutPanel4, 0, 3);
            this.tlpPackages.Controls.Add(this.label12, 0, 2);
            this.tlpPackages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPackages.Location = new System.Drawing.Point(3, 3);
            this.tlpPackages.Name = "tlpPackages";
            this.tlpPackages.RowCount = 5;
            this.tlpPackages.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.tlpPackages.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29F));
            this.tlpPackages.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tlpPackages.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tlpPackages.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.tlpPackages.Size = new System.Drawing.Size(862, 583);
            this.tlpPackages.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.Controls.Add(this.btnPkgDisplayAll, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmbSort, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtInput, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button4, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(850, 27);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnPkgDisplayAll
            // 
            this.btnPkgDisplayAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPkgDisplayAll.Location = new System.Drawing.Point(640, 3);
            this.btnPkgDisplayAll.Name = "btnPkgDisplayAll";
            this.btnPkgDisplayAll.Size = new System.Drawing.Size(121, 21);
            this.btnPkgDisplayAll.TabIndex = 5;
            this.btnPkgDisplayAll.Text = "Display All";
            this.btnPkgDisplayAll.UseVisualStyleBackColor = true;
            this.btnPkgDisplayAll.Click += new System.EventHandler(this.btnPkgDisplayAll_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(258, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 27);
            this.label2.TabIndex = 2;
            this.label2.Text = "Search";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search By:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbSort
            // 
            this.cmbSort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cmbSort.FormattingEnabled = true;
            this.cmbSort.Items.AddRange(new object[] {
            "Package Name",
            "Country",
            "ID"});
            this.cmbSort.Location = new System.Drawing.Point(88, 3);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(121, 27);
            this.cmbSort.TabIndex = 1;
            // 
            // txtInput
            // 
            this.txtInput.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtInput.Location = new System.Drawing.Point(343, 3);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(125, 27);
            this.txtInput.TabIndex = 3;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(513, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(121, 21);
            this.button4.TabIndex = 4;
            this.button4.Text = "Search";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // gridPackages
            // 
            this.gridPackages.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Maiandra GD", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridPackages.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridPackages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPackages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPackages.Location = new System.Drawing.Point(6, 42);
            this.gridPackages.Name = "gridPackages";
            this.gridPackages.Size = new System.Drawing.Size(850, 157);
            this.gridPackages.TabIndex = 1;
            this.gridPackages.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridPackages_CellClick);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Controls.Add(this.btnPkgSave, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnPkgDelete, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnPkgEdit, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnPkgAdd, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnPkgCencel, 4, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 546);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(850, 31);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // btnPkgSave
            // 
            this.btnPkgSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPkgSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPkgSave.Enabled = false;
            this.btnPkgSave.Location = new System.Drawing.Point(513, 3);
            this.btnPkgSave.Name = "btnPkgSave";
            this.btnPkgSave.Size = new System.Drawing.Size(164, 25);
            this.btnPkgSave.TabIndex = 3;
            this.btnPkgSave.TabStop = false;
            this.btnPkgSave.Text = "Save";
            this.btnPkgSave.UseVisualStyleBackColor = true;
            this.btnPkgSave.Click += new System.EventHandler(this.BtnPkgSave_Click);
            // 
            // btnPkgDelete
            // 
            this.btnPkgDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPkgDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPkgDelete.Location = new System.Drawing.Point(343, 3);
            this.btnPkgDelete.Name = "btnPkgDelete";
            this.btnPkgDelete.Size = new System.Drawing.Size(164, 25);
            this.btnPkgDelete.TabIndex = 2;
            this.btnPkgDelete.TabStop = false;
            this.btnPkgDelete.Text = "Delete";
            this.btnPkgDelete.UseMnemonic = false;
            this.btnPkgDelete.UseVisualStyleBackColor = true;
            // 
            // btnPkgEdit
            // 
            this.btnPkgEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPkgEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPkgEdit.Location = new System.Drawing.Point(173, 3);
            this.btnPkgEdit.Name = "btnPkgEdit";
            this.btnPkgEdit.Size = new System.Drawing.Size(164, 25);
            this.btnPkgEdit.TabIndex = 1;
            this.btnPkgEdit.Text = "Edit";
            this.btnPkgEdit.UseVisualStyleBackColor = true;
            this.btnPkgEdit.Click += new System.EventHandler(this.btnPkgEdit_Click);
            // 
            // btnPkgAdd
            // 
            this.btnPkgAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPkgAdd.BackgroundImage")));
            this.btnPkgAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPkgAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPkgAdd.Location = new System.Drawing.Point(3, 3);
            this.btnPkgAdd.Name = "btnPkgAdd";
            this.btnPkgAdd.Size = new System.Drawing.Size(164, 25);
            this.btnPkgAdd.TabIndex = 0;
            this.btnPkgAdd.UseVisualStyleBackColor = true;
            this.btnPkgAdd.Click += new System.EventHandler(this.btnPkgAdd_Click);
            // 
            // btnPkgCencel
            // 
            this.btnPkgCencel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPkgCencel.Location = new System.Drawing.Point(683, 3);
            this.btnPkgCencel.Name = "btnPkgCencel";
            this.btnPkgCencel.Size = new System.Drawing.Size(164, 25);
            this.btnPkgCencel.TabIndex = 4;
            this.btnPkgCencel.Text = "Cancel";
            this.btnPkgCencel.UseVisualStyleBackColor = true;
            this.btnPkgCencel.Click += new System.EventHandler(this.btnPkgCencel_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.flpProducts, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel10, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 233);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(850, 304);
            this.tableLayoutPanel4.TabIndex = 4;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel5.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.dtpPkgEnd, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtPkgName, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.dtpPkgStart, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.txtPkgID, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(419, 146);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 38);
            this.label6.TabIndex = 8;
            this.label6.Text = "End Date:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 36);
            this.label5.TabIndex = 7;
            this.label5.Text = "Start Date:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpPkgEnd
            // 
            this.dtpPkgEnd.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpPkgEnd.Enabled = false;
            this.dtpPkgEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPkgEnd.Location = new System.Drawing.Point(170, 113);
            this.dtpPkgEnd.Name = "dtpPkgEnd";
            this.dtpPkgEnd.Size = new System.Drawing.Size(188, 27);
            this.dtpPkgEnd.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 36);
            this.label4.TabIndex = 2;
            this.label4.Text = "Package ID: \r";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 36);
            this.label3.TabIndex = 0;
            this.label3.Text = "Package Name:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPkgName
            // 
            this.txtPkgName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPkgName.Location = new System.Drawing.Point(170, 4);
            this.txtPkgName.Name = "txtPkgName";
            this.txtPkgName.ReadOnly = true;
            this.txtPkgName.Size = new System.Drawing.Size(188, 27);
            this.txtPkgName.TabIndex = 1;
            // 
            // dtpPkgStart
            // 
            this.dtpPkgStart.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpPkgStart.Enabled = false;
            this.dtpPkgStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPkgStart.Location = new System.Drawing.Point(170, 76);
            this.dtpPkgStart.Name = "dtpPkgStart";
            this.dtpPkgStart.Size = new System.Drawing.Size(188, 27);
            this.dtpPkgStart.TabIndex = 5;
            // 
            // txtPkgID
            // 
            this.txtPkgID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPkgID.Location = new System.Drawing.Point(170, 40);
            this.txtPkgID.Name = "txtPkgID";
            this.txtPkgID.ReadOnly = true;
            this.txtPkgID.Size = new System.Drawing.Size(188, 27);
            this.txtPkgID.TabIndex = 3;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel6.Controls.Add(this.label10, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.txtPkgDesc, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.txtPkgTotal, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtPkgPrice, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtPkgCommision, 1, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 155);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(419, 146);
            this.tableLayoutPanel6.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(161, 38);
            this.label10.TabIndex = 12;
            this.label10.Text = "Description:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPkgDesc
            // 
            this.txtPkgDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPkgDesc.Location = new System.Drawing.Point(170, 111);
            this.txtPkgDesc.Name = "txtPkgDesc";
            this.txtPkgDesc.ReadOnly = true;
            this.txtPkgDesc.Size = new System.Drawing.Size(246, 32);
            this.txtPkgDesc.TabIndex = 11;
            this.txtPkgDesc.Text = "";
            // 
            // txtPkgTotal
            // 
            this.txtPkgTotal.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPkgTotal.Location = new System.Drawing.Point(170, 76);
            this.txtPkgTotal.Name = "txtPkgTotal";
            this.txtPkgTotal.ReadOnly = true;
            this.txtPkgTotal.Size = new System.Drawing.Size(188, 27);
            this.txtPkgTotal.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(161, 36);
            this.label9.TabIndex = 10;
            this.label9.Text = "Total price here";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 36);
            this.label8.TabIndex = 9;
            this.label8.Text = "Commision:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 36);
            this.label7.TabIndex = 8;
            this.label7.Text = "Price:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPkgPrice
            // 
            this.txtPkgPrice.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPkgPrice.Location = new System.Drawing.Point(170, 4);
            this.txtPkgPrice.Name = "txtPkgPrice";
            this.txtPkgPrice.ReadOnly = true;
            this.txtPkgPrice.Size = new System.Drawing.Size(188, 27);
            this.txtPkgPrice.TabIndex = 6;
            // 
            // txtPkgCommision
            // 
            this.txtPkgCommision.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPkgCommision.Location = new System.Drawing.Point(170, 40);
            this.txtPkgCommision.Name = "txtPkgCommision";
            this.txtPkgCommision.ReadOnly = true;
            this.txtPkgCommision.Size = new System.Drawing.Size(188, 27);
            this.txtPkgCommision.TabIndex = 7;
            // 
            // flpProducts
            // 
            this.flpProducts.AutoScroll = true;
            this.flpProducts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpProducts.Location = new System.Drawing.Point(428, 3);
            this.flpProducts.Name = "flpProducts";
            this.flpProducts.Size = new System.Drawing.Size(419, 146);
            this.flpProducts.TabIndex = 6;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Controls.Add(this.lblPkgProductSupplier, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.lblPkgProductName, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.lblPkgProductID, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.label15, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.btnPkgDeleteProduct, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.btnPkgAddProduct, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(428, 155);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 4;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(419, 146);
            this.tableLayoutPanel10.TabIndex = 7;
            // 
            // lblPkgProductSupplier
            // 
            this.lblPkgProductSupplier.AutoSize = true;
            this.lblPkgProductSupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPkgProductSupplier.Location = new System.Drawing.Point(212, 108);
            this.lblPkgProductSupplier.Name = "lblPkgProductSupplier";
            this.lblPkgProductSupplier.Size = new System.Drawing.Size(204, 38);
            this.lblPkgProductSupplier.TabIndex = 9;
            this.lblPkgProductSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPkgProductName
            // 
            this.lblPkgProductName.AutoSize = true;
            this.lblPkgProductName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPkgProductName.Location = new System.Drawing.Point(212, 72);
            this.lblPkgProductName.Name = "lblPkgProductName";
            this.lblPkgProductName.Size = new System.Drawing.Size(204, 36);
            this.lblPkgProductName.TabIndex = 8;
            this.lblPkgProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPkgProductID
            // 
            this.lblPkgProductID.AutoSize = true;
            this.lblPkgProductID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPkgProductID.Location = new System.Drawing.Point(212, 36);
            this.lblPkgProductID.Name = "lblPkgProductID";
            this.lblPkgProductID.Size = new System.Drawing.Size(204, 36);
            this.lblPkgProductID.TabIndex = 7;
            this.lblPkgProductID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(3, 108);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(203, 38);
            this.label15.TabIndex = 6;
            this.label15.Text = "Product Supplier";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(3, 72);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(203, 36);
            this.label13.TabIndex = 4;
            this.label13.Text = "Product Name: ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnPkgDeleteProduct
            // 
            this.btnPkgDeleteProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPkgDeleteProduct.Location = new System.Drawing.Point(212, 3);
            this.btnPkgDeleteProduct.Name = "btnPkgDeleteProduct";
            this.btnPkgDeleteProduct.Size = new System.Drawing.Size(204, 30);
            this.btnPkgDeleteProduct.TabIndex = 1;
            this.btnPkgDeleteProduct.Text = "Delete Product";
            this.btnPkgDeleteProduct.UseVisualStyleBackColor = true;
            // 
            // btnPkgAddProduct
            // 
            this.btnPkgAddProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPkgAddProduct.Location = new System.Drawing.Point(3, 3);
            this.btnPkgAddProduct.Name = "btnPkgAddProduct";
            this.btnPkgAddProduct.Size = new System.Drawing.Size(203, 30);
            this.btnPkgAddProduct.TabIndex = 0;
            this.btnPkgAddProduct.Text = "Add Product";
            this.btnPkgAddProduct.UseVisualStyleBackColor = true;
            this.btnPkgAddProduct.Click += new System.EventHandler(this.btnPkgAddProduct_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(3, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(203, 36);
            this.label11.TabIndex = 2;
            this.label11.Text = "ProductID: ";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 205);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(850, 22);
            this.label12.TabIndex = 5;
            this.label12.Text = "Display Area";
            this.label12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tabProducts
            // 
            this.tabProducts.BackColor = System.Drawing.SystemColors.Control;
            this.tabProducts.Controls.Add(this.tableLayoutPanel7);
            this.tabProducts.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabProducts.Location = new System.Drawing.Point(4, 31);
            this.tabProducts.Name = "tabProducts";
            this.tabProducts.Padding = new System.Windows.Forms.Padding(3);
            this.tabProducts.Size = new System.Drawing.Size(868, 589);
            this.tabProducts.TabIndex = 1;
            this.tabProducts.Text = "products";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.listView1, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 586F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(862, 583);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.flpDisplayAllProducts, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(568, 577);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // flpDisplayAllProducts
            // 
            this.flpDisplayAllProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpDisplayAllProducts.Location = new System.Drawing.Point(3, 3);
            this.flpDisplayAllProducts.Name = "flpDisplayAllProducts";
            this.flpDisplayAllProducts.Size = new System.Drawing.Size(562, 513);
            this.flpDisplayAllProducts.TabIndex = 0;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.Controls.Add(this.btnDeleteProducts, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.btnEditProducts, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.btnAddProduct, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 522);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(562, 52);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // btnDeleteProducts
            // 
            this.btnDeleteProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDeleteProducts.Location = new System.Drawing.Point(377, 3);
            this.btnDeleteProducts.Name = "btnDeleteProducts";
            this.btnDeleteProducts.Size = new System.Drawing.Size(182, 46);
            this.btnDeleteProducts.TabIndex = 2;
            this.btnDeleteProducts.Text = "Delete";
            this.btnDeleteProducts.UseVisualStyleBackColor = true;
            // 
            // btnEditProducts
            // 
            this.btnEditProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEditProducts.Location = new System.Drawing.Point(190, 3);
            this.btnEditProducts.Name = "btnEditProducts";
            this.btnEditProducts.Size = new System.Drawing.Size(181, 46);
            this.btnEditProducts.TabIndex = 1;
            this.btnEditProducts.Text = "Edit";
            this.btnEditProducts.UseVisualStyleBackColor = true;
            this.btnEditProducts.Click += new System.EventHandler(this.btnEditProducts_Click);
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddProduct.Location = new System.Drawing.Point(3, 3);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(181, 46);
            this.btnAddProduct.TabIndex = 0;
            this.btnAddProduct.Text = "Add";
            this.btnAddProduct.UseVisualStyleBackColor = true;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // listView1
            // 
            this.listView1.BackgroundImageTiled = true;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Location = new System.Drawing.Point(577, 3);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(282, 577);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // tabPkgProSup
            // 
            this.tabPkgProSup.Controls.Add(this.gridPPS);
            this.tabPkgProSup.Location = new System.Drawing.Point(4, 31);
            this.tabPkgProSup.Name = "tabPkgProSup";
            this.tabPkgProSup.Padding = new System.Windows.Forms.Padding(3);
            this.tabPkgProSup.Size = new System.Drawing.Size(868, 589);
            this.tabPkgProSup.TabIndex = 2;
            this.tabPkgProSup.Text = "pkgProSup";
            // 
            // gridPPS
            // 
            this.gridPPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPPS.Location = new System.Drawing.Point(0, 6);
            this.gridPPS.Name = "gridPPS";
            this.gridPPS.Size = new System.Drawing.Size(319, 498);
            this.gridPPS.TabIndex = 1;
            // 
            // ProdSupp
            // 
            this.ProdSupp.Location = new System.Drawing.Point(4, 31);
            this.ProdSupp.Name = "ProdSupp";
            this.ProdSupp.Padding = new System.Windows.Forms.Padding(3);
            this.ProdSupp.Size = new System.Drawing.Size(868, 589);
            this.ProdSupp.TabIndex = 3;
            this.ProdSupp.Text = "ProSup";
            this.ProdSupp.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1051, 624);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.tlpNav);
            this.Font = new System.Drawing.Font("Maiandra GD", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmMain";
            this.Text = "Travel Experts";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tlpNav.ResumeLayout(false);
            this.tlpNav.PerformLayout();
            this.tabMain.ResumeLayout(false);
            this.tabPackages.ResumeLayout(false);
            this.tlpPackages.ResumeLayout(false);
            this.tlpPackages.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPackages)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tabProducts.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tabPkgProSup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPPS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpNav;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnNavProductSuppliers;
        private System.Windows.Forms.Button btnPPS;
        private System.Windows.Forms.Button btnNavProducts;
        private System.Windows.Forms.Label lblMain;
        private System.Windows.Forms.Button btnNavPackages;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPackages;
        private System.Windows.Forms.TabPage tabProducts;
        private System.Windows.Forms.TableLayoutPanel tlpPackages;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbSort;
        private System.Windows.Forms.Button btnPkgDisplayAll;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView gridPackages;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnPkgAdd;
        private System.Windows.Forms.Button btnPkgSave;
        private System.Windows.Forms.Button btnPkgDelete;
        private System.Windows.Forms.Button btnPkgEdit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TabPage tabPkgProSup;
        private System.Windows.Forms.DataGridView gridPPS;
        private System.Windows.Forms.Button btnPkgCencel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button btnDeleteProducts;
        private System.Windows.Forms.Button btnEditProducts;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.TabPage ProdSupp;
        private System.Windows.Forms.FlowLayoutPanel flpDisplayAllProducts;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox txtPkgDesc;
        private System.Windows.Forms.TextBox txtPkgTotal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPkgPrice;
        private System.Windows.Forms.TextBox txtPkgCommision;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpPkgEnd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPkgName;
        private System.Windows.Forms.DateTimePicker dtpPkgStart;
        private System.Windows.Forms.TextBox txtPkgID;
        private System.Windows.Forms.FlowLayoutPanel flpProducts;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label lblPkgProductSupplier;
        private System.Windows.Forms.Label lblPkgProductName;
        private System.Windows.Forms.Label lblPkgProductID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnPkgDeleteProduct;
        private System.Windows.Forms.Button btnPkgAddProduct;
        private System.Windows.Forms.Label label11;
    }
}

