﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using WindowsFormsApp1;
using TravelExperts.UserControls;
using TravelExperts.TableClasses;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts
{
    public partial class frmMain : Form
    {
        private Packages package;
        private Packages currentPackage;
        private Products currentProduct;
        private List<Button> buttonList;
        int currentRow;
        private int currentPackageID;
        bool editMode = false;
        enum Pages { Packages, Products, FuturePage, Suppliers }

        public frmMain()
        {
            InitializeComponent();
            tabMain.ItemSize = new Size(0, 1);
            tabMain.SizeMode = TabSizeMode.Fixed;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }


        //         --- NAVIGATION MENU ---


        private void btnNavProducts_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.Products;
            buttonList = DisplayGui.DisplayAllProducts(flpDisplayAllProducts);
        }
        
        // Close button on navigation bar, closes all program not just current window
        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private void btnNavPackages_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.Packages;
            DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet());
        }
        private void btnPPS_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.FuturePage;
            // DisplayList.FillGridView(gridProducts, PackagesProductsSupplierDB.GetPackageProductSupplierDataSet());
        }

        private void btnNavProductSuppliers_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = (int)Pages.Suppliers;
        }


        //         --- PACKAGES PAGE ---
      

        private void btnPkgDisplayAll_Click(object sender, EventArgs e)
        {
            DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet());
        }

        private void btnPkgAddProduct_Click(object sender, EventArgs e)
        {
            if (currentPackageID > 0)
            {
                frmAddPackageProduct form = new frmAddPackageProduct(currentPackageID);
                DialogResult result = form.ShowDialog();
                
                //if (result == DialogResult.OK)
            }            
        }

        // Displays selected Package in display area from a row in DataGridView
        private void gridPackages_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridPackages.SelectedCells.Count > 0)
            {
                currentRow = gridPackages.SelectedCells[0].RowIndex;
                if (gridPackages.Rows[currentRow] != null)
                {
                    DataGridViewRow selectedRow = gridPackages.Rows[currentRow];
                    if (Int32.TryParse(selectedRow.Cells["PackageID"].Value.ToString(), out currentPackageID))
                    {
                        package = PackagesDB.GetPackage(currentPackageID);
                        txtPkgID.Text = package.PackageID.ToString();
                        txtPkgName.Text = package.PackageName.ToString();
                        txtPkgDesc.Text = package.PackageDescription;
                        dtpPkgStart.Value = package.PackageStartDate;
                        dtpPkgEnd.Value = package.PackageEndDate;
                        txtPkgPrice.Text = package.PackageBasePrice.ToString("C");
                        txtPkgCommision.Text = package.PackageAgencyCommission.ToString("C");
                    }
                    buttonList = DisplayGui.DisplayPackageProducts(currentPackageID, flpProducts);
                    ProductButtonClickEvent(buttonList);
                }
            }
        }

        private void ProductButtonClickEvent(List<Button> buttonList)
        {
            Products pro;
            List<Suppliers> supList;
            string line = null;
            foreach (Button btn in buttonList)
            {
                btn.Click += (sender, e) =>
                    {                        
                        pro = (Products)btn.Tag;
                        supList = PackagesProductsSupplierDB.GetProductSuppliersList(pro.ProductID);
                        lblPkgProductID.Text = pro.ProductID.ToString();
                        lblPkgProductName.Text = pro.ProductName;
                        foreach (Suppliers s in supList)
                        {
                             line += s.SupplierName + ", ";
                        }

                        lblPkgProductSupplier.Text = line;
                        line = "";
                    };
                btn.LostFocus += (sender, e) =>
                {
                    lblPkgProductID.Text = "";
                    lblPkgProductName.Text = "";
                    lblPkgProductSupplier.Text = "";
                };
            }
        }

        // Enables blank display area
        private void btnPkgAdd_Click(object sender, EventArgs e)
        {
            if (editMode == false)
            {
                ClearDisplayArea();
                EnableControls();
            }
        }

        // Clear all controls in display area
        private void ClearDisplayArea()
        {
            txtPkgID.Clear();
            txtPkgName.Clear();
            txtPkgDesc.Clear();
            dtpPkgStart.Value = DateTime.Today;
            dtpPkgEnd.Value = DateTime.Today;
            txtPkgPrice.Clear();
            txtPkgCommision.Clear();
            flpProducts.Controls.Clear();
        }

        // Enables all controls in display area
        private void EnableControls()
        {
            txtPkgName.ReadOnly = false;
            txtPkgDesc.ReadOnly = false;
            dtpPkgStart.Enabled = true;
            dtpPkgEnd.Enabled = true;
            txtPkgPrice.ReadOnly = false;
            txtPkgCommision.ReadOnly = false;
            btnPkgSave.Enabled = true;
        }

        // Makes all constrols read only to prevent editing data 
        private void DisableControls()
        {
            txtPkgName.ReadOnly = true;
            txtPkgDesc.ReadOnly = true;
            dtpPkgStart.Enabled = false;
            dtpPkgEnd.Enabled = false;
            txtPkgPrice.ReadOnly = true;
            txtPkgCommision.ReadOnly = true;
            btnPkgSave.Enabled = false;
        }

        // Grabs data from display area, creates Packages object and saves it to database
        private void BtnPkgSave_Click(object sender, EventArgs e)
        {
            package = new Packages
            {
                PackageName = txtPkgName.Text.ToString(),
                PackageDescription = txtPkgDesc.Text,
                PackageStartDate = dtpPkgStart.Value,
                PackageEndDate = dtpPkgEnd.Value,
                PackageBasePrice = decimal.Parse(txtPkgPrice.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.Number),
                PackageAgencyCommission = decimal.Parse(txtPkgCommision.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.Number)
            };
            if (txtPkgID.Text == "")
            {
                PackagesDB.AddNewPackage(package);
            }
            else
            {

                package.PackageID = Convert.ToInt32(txtPkgID.Text);
                if (!PackagesDB.UpdateCustomer(currentPackage, package))
                {
                    MessageBox.Show("Another user has updated or " +
                        "deleted this package. List updated!.", "Database Error");
                    this.DialogResult = DialogResult.Retry;
                }
            }
            DisplayGui.FillGridView(gridPackages, PackagesDB.GetPackageDataSet());
            ClearDisplayArea();
            DisableControls();
            editMode = false;
        }

        // Enables Display area and populates it with selected Package data
        private void btnPkgEdit_Click(object sender, EventArgs e)
        {
            if (editMode == false)
            {
                if (txtPkgID.Text != "")
                {
                    currentPackage = new Packages()
                    {
                        PackageID = Convert.ToInt32(txtPkgID.Text),
                        PackageName = txtPkgName.Text.ToString(),
                        PackageDescription = txtPkgDesc.Text,
                        PackageStartDate = dtpPkgStart.Value,
                        PackageEndDate = dtpPkgEnd.Value,
                        PackageBasePrice = decimal.Parse(txtPkgPrice.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.Number),
                        PackageAgencyCommission = decimal.Parse(txtPkgCommision.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.Number)
                    };
                    EnableControls();
                    editMode = true;
                }
                else
                {
                    MessageBox.Show("Choose Package from list");
                }
            }
        }

        //Clears and disables display area
        private void btnPkgCencel_Click(object sender, EventArgs e)
        {
            ClearDisplayArea();
            DisableControls();
            editMode = false;
        }

        //
        public void DisplayProductInfo(Products p)
        {
            lblPkgProductID.Text = p.ProductID.ToString();
        }
        

        //         --- PRODUCTS PAGE ---


        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            frmAddProduct form = new frmAddProduct();
            DialogResult result = form.ShowDialog();
        }       

        // Opens form for editing selected Product
        private void btnEditProducts_Click(object sender, EventArgs e)
        {
            if (currentProduct == null)
            {
                MessageBox.Show("Select Product first");
            }
            else
            {
                frmAddProduct form = new frmAddProduct(currentProduct);
                DialogResult result = form.ShowDialog();
                if (result == DialogResult.OK)
                    DisplayGui.DisplayAllProducts(flpDisplayAllProducts);
            }
        }
    }
}

