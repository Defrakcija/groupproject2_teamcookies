﻿using System;
using System.Collections.Generic;
using TravelExperts.TableClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace TravelExperts.ClassesDB
{
    public static class SuppliersDB
    {
        static SqlConnection con = TravelExpertsDB.GetConnection();
        // reads database and return List of Suppliers for specific Product
        public static List<Suppliers> GetProductSuppliers(int id)
        {
            Suppliers sup;
            List<Suppliers> supList = new List<Suppliers>();
            string selectQuery = "SELECT s.SupplierId, s.SupName " +
                                 "FROM Suppliers s " +
                                 "INNER JOIN Products_Suppliers ps " +
                                 "ON s.SupplierId = ps.SupplierId " +
                                 "WHERE ps.ProductId = @ProductId " +
                                 "GROUP BY s.SupplierId, s.SupName";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@ProductId", id);
            try
            {
                con.Open(); // open connection
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read()) // read the customer if exists
                {
                    sup = new Suppliers
                    {
                        SupplierID = (int)reader["SupplierId"],
                        SupplierName = reader["SupName"].ToString()
                    }; // create new products object
                    supList.Add(sup);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close(); // close connection
            }
            return supList;
        }
    }
}
