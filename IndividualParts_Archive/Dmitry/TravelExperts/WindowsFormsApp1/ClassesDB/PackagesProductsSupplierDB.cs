﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TravelExperts.TableClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts
{
    public static class PackagesProductsSupplierDB
    {
        static SqlConnection con = TravelExpertsDB.GetConnection();

        // Gets all data from this table
        public static DataSet GetPackageProductSupplierDataSet()
        {            
            string selectQuery = "SELECT * FROM Packages_Products_Suppliers";
            var dataAdapter = new SqlDataAdapter(selectQuery, con);
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);
            return ds;
        }

        // Get Supplier name by ProductID
        public static List<Suppliers> GetProductSuppliersList(int id)
        {
            List<Suppliers> supList = new List<Suppliers>();
            Suppliers sup;
            string selectQuery = "SELECT s.SupplierId, s.SupName " +
                                 "FROM Suppliers s  " +
                                 "INNER JOIN Products_Suppliers ps " +
                                 "ON s.SupplierId = ps.SupplierId " +
                                 "WHERE ps.ProductId = @ProductId " +
                                 "GROUP BY s.SupplierId, s.SupName ";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@ProductId", id);
            try
            {
                con.Open(); // open connection
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    sup = new Suppliers
                    {
                        SupplierID = (int)reader["SupplierId"],
                        SupplierName = reader["SupName"].ToString()
                    }; 
                    supList.Add(sup);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close(); // close connection
            }
            return supList;
        }

        public static void AddProductsToPackage(Packages package)
        {
            string insertStatement = "INSERT INTO Packages " +
                                     " (PkgName, PkgStartDate, PkgEndDate, PkgDesc, PkgBasePrice, PkgAgencyCommission) " +
                                     "VALUES(@PkgName, @PkgStartDate, @PkgEndDate, @PkgDesc, @PkgBasePrice, @PkgAgencyCommission)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, con);
            insertCommand.Parameters.AddWithValue("@PkgName", package.PackageName);
            insertCommand.Parameters.AddWithValue("@PkgStartDate", package.PackageStartDate);
            insertCommand.Parameters.AddWithValue("@PkgEndDate", package.PackageEndDate);
            insertCommand.Parameters.AddWithValue("@PkgDesc", package.PackageDescription);
            insertCommand.Parameters.AddWithValue("@PkgBasePrice", package.PackageBasePrice);
            insertCommand.Parameters.AddWithValue("@PkgAgencyCommission", package.PackageAgencyCommission);
            try
            {
                con.Open();
                insertCommand.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

    }
}
