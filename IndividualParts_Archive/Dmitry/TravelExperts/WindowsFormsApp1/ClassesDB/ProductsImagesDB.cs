﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts.ClassesDB
{
    public static class ProductsImagesDB
    {
        public static bool AddNewProductImage(byte[] b, Products p)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string insertStatement = "INSERT INTO [Products_Images] " +
                                     " (ProductID, Image) " +
                                     "VALUES(@ProductID, @Array)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, con);
            insertCommand.Parameters.AddWithValue("@ProductID", p.ProductID);
            insertCommand.Parameters.AddWithValue("@Array", b);
            try
            {
                con.Open();
                int count = insertCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public static byte[] GetImage(int id)
        {
            byte[] array = null;
            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectQuery = "SELECT Image " +
                                 "FROM Products_Images " +
                                 "WHERE ProductID = @ProductID";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@ProductID", id);
            try
            {
                con.Open(); // open connection
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read()) // read the customer if exists
                {
                    array = (byte [])reader["Image"];
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close(); // close connection
            }
            return array;
        }
    }
}
