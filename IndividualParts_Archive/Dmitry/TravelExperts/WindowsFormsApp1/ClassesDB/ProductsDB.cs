﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts
{
    public static class ProductsDB
    {
        static SqlConnection con = TravelExpertsDB.GetConnection();
        // Retutns DataSet with all Products from database
        public static DataSet GetProductsDataSet()
        {            
            string selectQuery = "SELECT * FROM Products";
            var dataAdapter = new SqlDataAdapter(selectQuery, con);
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);
            return ds;
        }

        // Read database and return List of Products objects
        public static List<Products> GetProductsList()
        {
            Products pro = null;
            List<Products> proList = new List<Products>();

            string selectQuery = "SELECT ProductId, ProdName " +
                                 "FROM Products";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            try
            {
                con.Open(); // open connection
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read()) // read the products if exists
                {
                    pro = new Products(); // create new product object
                    pro.ProductID = (int)reader["ProductId"];
                    pro.ProductName = reader["ProdName"].ToString();
                    proList.Add(pro);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close(); // close connection
            }
            return proList;
        }

        public static List<Products> GetProductsExeptPackageProducts(int id)
        {
            Products pro;
            List<Products> proList = new List<Products>();
            string selectQuery = "SELECT * FROM Products p WHERE p.ProductId NOT IN (" +
                                 "SELECT p.ProductId " +
                                 "FROM Products p " +
                                 "INNER JOIN Products_Suppliers ps " +
                                 "ON p.ProductId = ps.ProductId " +
                                 "INNER JOIN Packages_Products_Suppliers pps " +
                                 "ON ps.ProductSupplierId = pps.ProductSupplierId " +
                                 "WHERE pps.PackageId = @PackageId " +
                                 "GROUP BY p.ProductId, p.ProdName)";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@PackageId", id);
            try
            {
                con.Open(); // open connection
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read()) // read the customer if exists
                {
                    pro = new Products
                    {
                        ProductID = (int)reader["ProductId"],
                        ProductName = reader["ProdName"].ToString()
                    }; // create new products object
                    proList.Add(pro);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close(); // close connection
            }
            return proList;
        }
    }
}
