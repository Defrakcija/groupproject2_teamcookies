﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TravelExperts.ClassesDB;
using TravelExperts.TableClasses;

namespace TravelExperts
{
    public partial class frmAddPackageProduct : Form
    {
        private int currentPackageID { get;  set; }
        private Products pro;
        private List<Products> proList = new List<Products>();
        private List<Suppliers> supList = new List<Suppliers>();
        List<Button> newButList = new List<Button>();
        FlowLayoutPanel flpSupSelect = new FlowLayoutPanel()
        {
            Dock = DockStyle.Fill,
        };
        Button btnFinishAdding = new Button
        {
            Dock = DockStyle.Fill,
            Text = "Finish",
            DialogResult = DialogResult.OK,
        };
        TableLayoutPanel tblFooter = new TableLayoutPanel()
        {
            Dock = DockStyle.Bottom,
            ColumnCount = 3,
            Height = 50,
            CellBorderStyle = TableLayoutPanelCellBorderStyle.Inset,
        };        
        
        Button btnCancel = new Button
        {
            Dock = DockStyle.Fill,
            Text = "Cancel",
            DialogResult = DialogResult.Cancel,
        };
        Button btnBack = new Button
        {
            Dock = DockStyle.Fill,
            Text = "Back",
            Enabled = false,
        };
        Button btnNext = new Button
        {
            Dock = DockStyle.Fill,
            Text = "Next",
        };


        public frmAddPackageProduct(int i)
        {
            InitializeComponent();

            tblFooter.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0F));
            tblFooter.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25.0F));
            tblFooter.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25.0F));
            
            btnNext.Click += (sender, e) =>
            {
                ChangePanel();
            };
            tblFooter.Controls.Add(btnNext, 0, 1);
            tblFooter.Controls.Add(btnBack, 1, 1);
            tblFooter.Controls.Add(btnCancel, 2, 1);
            this.Controls.Add(tblFooter);
            
            this.currentPackageID = i;
            List<Button> butList = DisplayGui.GetProductsNotIncludedInPackage(currentPackageID);       
            foreach (Button btn in butList)            {
                flpAvailableProducts.Controls.Add(btn);
                btn.Click += (sender, e) =>
                {                   
                    Button b = (Button)sender;
                    if (b.Parent.Name.Equals(flpAvailableProducts.Name))
                    {
                        flpSelectedProducts.Controls.Add(b);
                        flpAvailableProducts.Controls.Remove(b);
                        if(!newButList.Contains(b))
                        newButList.Add(b);
                    }
                    else
                    {
                        flpAvailableProducts.Controls.Add(b);
                        flpSelectedProducts.Controls.Remove(b);
                        if (newButList.Contains(b))
                            newButList.Remove(b);
                    }                    
                };
            }
        }

        private void ChangePanel()
        {
            tblMain.Hide();
            btnBack.Enabled = true;
            tblFooter.Controls.Clear();
            proList.Clear();
            supList.Clear();
            tblFooter.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0F));
            tblFooter.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25.0F));
            tblFooter.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25.0F));
            
            tblFooter.Controls.Add(btnFinishAdding, 0, 1);
            tblFooter.Controls.Add(btnBack, 1, 1);
            tblFooter.Controls.Add(btnCancel, 2, 1);
            this.Controls.Add(tblFooter);     
            this.Controls.Add(flpSupSelect);
            btnBack.Click += (sender, e) =>
            {
                BackPanel();
            };
            foreach (Button b in newButList)
            {
                pro = (Products)b.Tag;
                proList.Add(pro);
                supList = SuppliersDB.GetProductSuppliers(pro.ProductID);
                Panel pp = new Panel()
                {
                    AutoScroll = true,
                    Height = 121,
                    Width = flpSupSelect.Width - 8,
                    BorderStyle = BorderStyle.FixedSingle,
                };
                Label l = new Label()
                {
                    Text = "Select supplier for this product: ",
                    Dock = DockStyle.Fill,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Font = new Font("Modern No. 20", 14.0F),
                };
                ComboBox cb = new ComboBox()
                {
                    Anchor = AnchorStyles.Left,
                    Width = 250,                    
                };
                foreach(Suppliers s in supList)
                {
                    cb.Items.Add(s.SupplierName);
                }
                flpSupSelect.Controls.Add(pp);
                TableLayoutPanel tlp = new TableLayoutPanel()
                {
                    Dock = DockStyle.Fill,
                    ColumnCount = 3,
                };
                tlp.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120));
                tlp.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0F));
                tlp.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0F));



                tlp.Controls.Add(b, 0, 1);
                tlp.Controls.Add(l, 1, 1);
                tlp.Controls.Add(cb, 2, 1);
                pp.Controls.Add(tlp);
            }
        }

        public void BackPanel()
        {
            flpSupSelect.Controls.Clear();
            this.Controls.Remove(flpSupSelect);
            tblMain.Show();
            btnBack.Enabled = false;
            tblFooter.Controls.Remove(btnFinishAdding);
            tblFooter.Controls.Add(btnNext, 0, 1);
            foreach(Button b in newButList)
            {
                flpSelectedProducts.Controls.Add(b);
            }
            btnNext.Click += (sender, e) =>
            {
                ChangePanel();
            };
        }
    }
}
