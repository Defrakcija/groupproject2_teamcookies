﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using TravelExperts.ClassesDB;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts
{
    public partial class frmAddProduct : Form
    {
        private Products currentProduct;
        public frmAddProduct()
        {
            InitializeComponent();
            this.Text = "New Product";
        }

        public frmAddProduct(Products p)
        {
            InitializeComponent();
            currentProduct = p;
            txtEnterProductName.Text = p.ProductName;
            this.Text = "Edit Product";
        }

        private void btnOpenProductFileDialog_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                picAddProductBox.Image = new Bitmap(ofd.FileName);
            }
        }

        private void ConvertImage()
        {
            //converting photo to binary data and savig it to database
            if (picAddProductBox.Image != null)
            {
                MemoryStream ms = new MemoryStream();
                picAddProductBox.Image.Save(ms, ImageFormat.Jpeg);
                byte[] photoAray = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(photoAray, 0, photoAray.Length);
                ProductsImagesDB.AddNewProductImage(photoAray, currentProduct);
            }
        }

        private void btnSaveProduct_Click(object sender, EventArgs e)
        {
            ConvertImage();
        }
    }
}
