﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts.UserControls
{
    public partial class DisplayProducts : UserControl
    {
        public string productName { get; set; }
        public int productID { get; set; }
        public int picID { get; set; }

        public DisplayProducts(string name)
        {
            InitializeComponent();
            this.lblProName.Text = name;
        }
       


        private void DisplayProducts_Load(object sender, EventArgs e)
        {
        }


    }
}
