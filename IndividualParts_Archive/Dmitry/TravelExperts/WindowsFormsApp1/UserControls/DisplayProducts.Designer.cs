﻿namespace TravelExperts.UserControls
{
    partial class DisplayProducts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblProName = new System.Windows.Forms.Label();
            this.picUCDisplay = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUCDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.lblProName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.picUCDisplay, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(100, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblProName
            // 
            this.lblProName.AutoSize = true;
            this.lblProName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProName.Location = new System.Drawing.Point(4, 69);
            this.lblProName.Name = "lblProName";
            this.lblProName.Size = new System.Drawing.Size(92, 30);
            this.lblProName.TabIndex = 0;
            this.lblProName.Text = "`";
            this.lblProName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picUCDisplay
            // 
            this.picUCDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picUCDisplay.Location = new System.Drawing.Point(4, 4);
            this.picUCDisplay.Name = "picUCDisplay";
            this.picUCDisplay.Size = new System.Drawing.Size(92, 61);
            this.picUCDisplay.TabIndex = 1;
            this.picUCDisplay.TabStop = false;
            // 
            // DisplayProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "DisplayProducts";
            this.Size = new System.Drawing.Size(100, 100);
            this.Load += new System.EventHandler(this.DisplayProducts_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUCDisplay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblProName;
        private System.Windows.Forms.PictureBox picUCDisplay;
    }
}
