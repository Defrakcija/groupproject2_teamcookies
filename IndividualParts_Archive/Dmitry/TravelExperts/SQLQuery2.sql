﻿SELECT * FROM Products p WHERE p.ProductId NOT IN (
SELECT p.ProductId
FROM Products p 
INNER JOIN Products_Suppliers ps 
ON p.ProductId = ps.ProductId
INNER JOIN Packages_Products_Suppliers pps 
ON ps.ProductSupplierId = pps.ProductSupplierId 
WHERE pps.PackageId = 4
GROUP BY p.ProductId, p.ProdName )

SELECT p.ProductId, p.ProdName
FROM Products p 
INNER JOIN Products_Suppliers ps 
ON p.ProductId = ps.ProductId
INNER JOIN Packages_Products_Suppliers pps 
ON ps.ProductSupplierId = pps.ProductSupplierId 
WHERE pps.PackageId = 4
GROUP BY p.ProductId, p.ProdName